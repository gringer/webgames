// Drone battle game -- drone battle simulation, with SVG and javascript
// author: David Eccles (gringer) <bioinformatics@gringene.org>

var backgroundDocument;
var started = false;
var leftEdge = 0;
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second

var finished = false;

// game / screen settings
var svgWidth = 500;
var svgHeight = 500;
var gameWidth = 400;
var gameHeight = 400;

// point / particle model parameters
var numPoints = 100;
var pointSize = 10;
var pointLocs = new Array();
var pointVels = new Array();
var pointAngs = new Array();

// drone model parameters
var droneTeam = new Array();
var cooling = new Array();
var dead = new Array();
var availableSlots = new Array();
var deadDroneCount = 0;
var strength = new Array();
var droneSpeed = new Array();
var defectChance = 0;
var maxDroneSpeed = 3;
var seekMul = 0.01;

// tracer parameters
var tracerPX = 200;
var tracerPY = 200;
var tracerX = 200;
var tracerY = 200;
var tracerVX = 0;
var tracerVY = 0;
var traceLimit = 3;
var pointerVisible = true;

var tracedDrone = -1;

// reward parameters
var totalMoney = 0;
var totalDays = 0;
var framesPerDay = 20;
var firstInfectionID = -1;
var firstInfection = true;
var alertedWon = false;

var randSeed = new Date().getTime();
var gameSeed = randSeed;
var lastLog = 0;

var moveId = setInterval(processFrame, (1000/fps));

// the game only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*541*9301+49297) % 233279;
    return randSeed/(233279.0);
}

function startup(evt){
    backgroundDocument = evt.target.ownerDocument;
    makeGame(Math.floor(Math.random() * 10000));
}

function createDrone(i){
    var paramForm = backgroundDocument.getElementById("paramform");
    var newStrength = Number(paramForm.strengthInput.value);
    var newSpeed = Number(paramForm.speedInput.value);
    dead[i] = false;
    cooling[i] = 0;
    strength[i] = (droneTeam[i] == 0) ? newStrength : 1;
    droneSpeed[i] = (droneTeam[i] == 0) ? newSpeed : 1;
    moveAng = nextRandom() * Math.PI * 2;
    pointAngs[i] = moveAng;
    pointVels[i] = [Math.cos(moveAng) * droneSpeed[i],
		    Math.sin(moveAng) * droneSpeed[i]];
}

function makeGame(seed){
    randSeed = seed;
    gameSeed = seed;
    setForm();
    var seedtext = backgroundDocument.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var game = backgroundDocument.getElementById("gamesetup");
    game.setAttribute("width", svgWidth + "px");
    game.setAttribute("height", svgHeight + "px");
    randSeed = seed;
    gameSeed = seed;
    // reset game variables
    dead = new Array();
    availableSlots = new Array();
    deadDroneCount = 0;
    strength = new Array();
    droneSpeed = new Array();
    blueIDs = new Array();
    redIDs = new Array();
    // reset tracer variables
    tracerPX = 200;
    tracerPY = 200;
    tracerX = 200;
    tracerY = 200;
    tracerVX = 0;
    tracerVY = 0;
    totalDays = 0;
    totalMoney = 0;
    pointerVisible = true;
    firstInfection = true;
    // reset win state
    alertedWon = false;
    // set up drones
    var blueIDs = new Array();
    var redIDs = new Array();
    var blueCount = 0;
    var redCount = 0;
    for(var i=0; i < 13; i++){
	nextRandom(); // feed the random generator a few times
    }
    for(var i=0; i < numPoints; i++){
	pointLocs[i] = [nextRandom() * (gameWidth - pointSize) + pointSize/2,
			nextRandom() * (gameHeight - pointSize) + pointSize/2];
	droneTeam[i] = pointLocs[i][1] > pointLocs[0][1] ? 0 : 1;
	createDrone(i);
	if(droneTeam[i] == 0){
	    blueCount++;
	    blueIDs.push(i);
	} else {
	    redCount++;
	    redIDs.push(i);
	}
    }
    if(blueCount == 0){
	droneTeam[0] = 0;
    }
    if(redCount == 0){
	droneTeam[0] = 1;
    }
    // draw points
    drawPoints();
    clearInterval(moveId);
    moveId = setInterval(processFrame, (1000/fps));
}

function drawPoints(){
    var redPath = backgroundDocument.getElementById("teamRed");
    var bluePath = backgroundDocument.getElementById("teamBlue");
    var redCoolingPath = backgroundDocument.getElementById("coolingRed");
    var blueCoolingPath = backgroundDocument.getElementById("coolingBlue");
    var newStrength = Number(paramForm.strengthInput.value);
    var tracedDroneText = "";
    var redText = "";
    var redCoolingText = "";
    var blueText = "";
    var blueCoolingText = "";
    var boxesHoriz = (gameWidth / (pointSize * 2)) + 2;
    var boxesVert = (gameHeight / (pointSize * 2)) + 2;
    var foundPoints = [];
    var collided = [];
    var addedCollision = [];
    var pointLoc = [];
    var blueIDs = new Array();
    var redIDs = new Array();
    for(var xi = 0; xi < boxesHoriz; xi++){
	foundPoints[xi] = [];
	for(var yi = 0; yi < boxesHoriz; yi++){
	    foundPoints[xi][yi] = [];
	}
    }
    for(var i=0; i < numPoints; i++){
	pointLoc = pointLocs[i];
	if(dead[i]){
	    continue;
	}
	if(Math.sqrt((tracerX - pointLoc[0]) ** 2 +
		     (tracerY - pointLoc[1]) ** 2) <= (pointSize * 1.5)){
	    tracedDrone = i;
	}
	if(droneTeam[i] == 0){
	    blueIDs.push(i);
	} else {
	    redIDs.push(i);
	}
	// Calculate collision bounding boxes
	bbX = Math.floor(pointLoc[0] / 20); bbX1 = bbX+1;
	bbY = Math.floor(pointLoc[1] / 20); bbY1 = bbY+1;
	var bbHashes = [[bbX, bbY],  [bbX1, bbY],
			[bbX, bbY1], [bbX1, bbY1]];
	for(var bbi = 0; bbi < 4; bbi++){
	    bbh = bbHashes[bbi];
	    fp = foundPoints[bbh[0]][bbh[1]];
	    if(fp.length == 0){
		foundPoints[bbh[0]][bbh[1]][fp.length] = i;
	    } else {
		var addedCollision = false;
		for(var fi = 0; fi < fp.length; fi++){
		    var cp = pointLocs[fp[fi]]; // potentiallyCollidedPoint
		    // sqrt((x0-x1)^2 + (y0-y1)^2)
		    var dist = Math.sqrt((cp[0] - pointLoc[0]) ** 2 +
					 (cp[1] - pointLoc[1]) ** 2);
		    // Note: this is slightly inaccurate, as it's an
		    // instantaneous collision calculation. A more accurate
		    // calculation would require comparing trajectories.
		    if(dist < (pointSize)){ // collided
			s1 = strength[i] * (cooling[i] > 1 ? 0.375 : 1);
			s2 = strength[fp[fi]] * (cooling[fp[fi]] > 1 ? 0.375 : 1);
			if(droneTeam[i] != droneTeam[fp[fi]]){
			    // battle
			    if(nextRandom() > strength[i]/(strength[i] + strength[fp[fi]])){
				dead[i] = true;
				availableSlots.push(i);
				deadDroneCount++;
				strength[fp[fi]] += 1;
				cooling[fp[fi]] = cooling[fp[fi]] + 100;
			    } else {
				dead[fp[fi]] = true;
				availableSlots.push(fp[fi]);
				deadDroneCount++;
				strength[i] += 1;
				cooling[i] = cooling[i] + 100;
			    }
			} else {
			    // create new
			    if((cooling[i] == 0) && (cooling[fp[fi]] == 0) &&
			       (strength[i] > 1) && (strength[fp[fi]] > 1)){
				cooling[i] = 30;
				cooling[fp[fi]] = 30;
				// randomly wear out one of the drones
				if(nextRandom() < 0.5){
				    strength[i] -= 1;
				    cooling[i] = 60;
				} else {
				    strength[fp[fi]] -= 1;
				    cooling[fp[fi]] = 60;
				}
				if(availableSlots.length > 0){
				    // create a new drone
				    newID = availableSlots.pop();
				    deadDroneCount -= 1;
				    dead[newID] = false;
				    droneTeam[newID] = droneTeam[i];
				    pointLocs[newID] =
					[(pointLoc[0] + cp[0]) / 2,
					 (pointLoc[1] + cp[1]) / 2];
				    createDrone(newID);
				    cooling[newID] = 100;
				}
			    }
			}
			if(!addedCollision){
			    collided[collided.length] = i;
			    addedCollision = true;
			}
			collided[collided.length] = fp[fi];
			// Another simplification - assuming the deflection point
			// is right in the middle of the two points
			// calculate collision normal vector
			colMid = [(cp[0] + pointLoc[0])/2,
				  (cp[1] + pointLoc[1])/2];
			// shift points away from collision in an attempt to reduce
			// dancing points
			cp[0] = cp[0] -
			    (colMid[0] - cp[0]) * (pointSize - dist) / 2;
			cp[1] = cp[1] -
			    (colMid[1] - cp[1]) * (pointSize - dist) / 2;
			pointLoc[0] = pointLoc[0] +
			    (pointLoc[0] - colMid[0]) * (pointSize - dist) / 2;
			pointLoc[1] = pointLoc[1] +
			    (pointLoc[1] - colMid[1]) * (pointSize - dist) / 2;
			// elastic collision of equal masses - just swap velocities!
			vi1 = pointVels[i]; vi2 = pointVels[fp[fi]];
			pointVels[i] = vi2;
			pointVels[fp[fi]] = vi1;
		    }
		}
		foundPoints[bbh[0]][bbh[1]][fp.length] = i;
	    }
	}
    }
    var redCount = 0;
    var blueCount = 0;
    var redCentre = [0,0];
    var blueCentre = [0,0];
    for(var i=0; i < numPoints; i++){
	if(dead[i]){
	    continue;
	}
	pointLoc = pointLocs[i];
	if(droneTeam[i] == 0){
	    blueCount++;
	    blueCentre[0] += pointLoc[0];
	    blueCentre[1] += pointLoc[1];
	} else {
	    redCount++;
	    redCentre[0] += pointLoc[0];
	    redCentre[1] += pointLoc[1];
	}
    }
    if(redCount > 0){
	redCentre[0] = redCentre[0] / redCount;
	redCentre[1] = redCentre[1] / redCount;
    }
    if(blueCount > 0){
	blueCentre[0] = blueCentre[0] / blueCount;
	blueCentre[1] = blueCentre[1] / blueCount;
    }
    var qc = 0;
    var hc = 0;
    for(var i=0; i < numPoints; i++){
	if(dead[i]){
	    continue;
	}
	if(cooling[i] > 0){
	    cooling[i] -= 1;
	}
	pointLoc = pointLocs[i];
	// Head towards the centre point of the other team
	if(true){
	    var ps = [0,0];
	    if(droneTeam[i] == 0){
		ps = redCentre;
	    } else {
		ps = blueCentre;
	    }
	    deltaX = ps[0] - pointLoc[0];
	    deltaY = ps[1] - pointLoc[1];
	    dist = Math.sqrt(deltaX ** 2 + deltaY ** 2);
	    oldVel = pointVels[i];
	    newVel = [deltaX / dist,
		      deltaY / dist];
	    newVel[0] = oldVel[0] + newVel[0] * seekMul;
	    newVel[1] = oldVel[1] + newVel[1] * seekMul;
	    newVelDist = Math.sqrt(newVel[0] ** 2 + newVel[1] ** 2);
	    if(newVelDist > maxDroneSpeed){
		newVel[0] = newVel[0] * maxDroneSpeed / newVelDist;
		newVel[1] = newVel[1] * maxDroneSpeed / newVelDist;
	    }
	    pointVels[i] = newVel;
	}
	if(droneTeam[i] == 0){
	    if(cooling[i]){
		blueCoolingText += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    } else {
		blueText += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    }
	} else {
	    if(cooling[i]){
		redCoolingText += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    } else {
		redText += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    }
	}
    }
    var infecttext = backgroundDocument.getElementById("infectlabel");
    infecttext.firstChild.nodeValue = "Red: " + Math.floor(redCount) + "; Blue: " + Math.floor(blueCount);
    var newScoreText = "";
    //newScoreText += "Money: $" + Math.floor(totalMoney) +"; Days: " + Math.floor(totalDays);
    var scoretext = backgroundDocument.getElementById("scorelabel");
    if(tracedDrone >= 0){
	newScoreText += "Drone #" + tracedDrone;
	if(dead[tracedDrone]){
	    newScoreText += ": dead";
	} else {
	    newScoreText += ": " + strength[tracedDrone];
	    if(cooling[tracedDrone] > 1){
		newScoreText += " (" + strength[tracedDrone] *
		    (cooling[tracedDrone] > 1 ? 0.375 : 1) + ")";
	    }
	}
	newScoreText += "";
    }
    scoretext.firstChild.nodeValue = newScoreText;
    redPath.setAttribute("d", redText);
    bluePath.setAttribute("d", blueText);
    redCoolingPath.setAttribute("d", redCoolingText);
    blueCoolingPath.setAttribute("d", blueCoolingText);
    if((redCount == 0) || (blueCount == 0)){
	winName = "Team Red";
	if(redCount == 0){
	    winName = "Team Blue";
	}
	window.alert(winName + " wins after " +
		     Math.floor(totalDays) + " days\n\n" +
		     "Final score: " + Math.ceil(Math.max(0, totalMoney) * 100 / ((totalDays/10) ** 2)) / 100);
	clearInterval(moveId);
	alertedWon = true;
    }
}

function setForm() {
    var inputForm = backgroundDocument.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = gameSeed;
        var speedValue = -1;
        for(var i = 0; i < inputForm.gamespeed.length; i++){
            if(inputForm.gamespeed[i].checked){
                speedValue = i;
            }
        }
        if(speedValue == -1){
            inputForm.gamespeed[2].checked = true;
            speedValue = 2;
        }
        if(speedValue == 0){
            frameDelay = 45;
        }
        if(speedValue == 1){
            frameDelay = 20;
        }
        if(speedValue == 2){
            frameDelay = 12;
        }
        if(speedValue == 3){
            frameDelay = 8;
        }
    }
}

function processFrame(){
    totalDays += (8/frameDelay) / framesPerDay;
    var minX = pointSize/2;
    var minY = pointSize/2;
    var maxX = gameWidth - pointSize/2;
    var maxY = gameWidth - pointSize/2;
    for(var i=0; i < numPoints; i++){
	// update point location
    	pointLoc = [pointLocs[i][0] + pointVels[i][0] * 8/frameDelay,
    		    pointLocs[i][1] + pointVels[i][1] * 8/frameDelay];
	// wall bounce collision check
	if(pointLoc[0] < minX){
	    pointLoc[0] = minX + (minX - pointLoc[0]);
	    pointVels[i][0] = -pointVels[i][0];
	}
	if(pointLoc[1] < minY){
	    pointLoc[1] = minY + (minY - pointLoc[1]);
	    pointVels[i][1] = -pointVels[i][1];
	}
	if(pointLoc[0] > maxX){
	    pointLoc[0] = maxX - (pointLoc[0] - maxX);
	    pointVels[i][0] = -pointVels[i][0];
	}
	if(pointLoc[1] > maxY){
	    pointLoc[1] = maxY - (pointLoc[1] - maxY);
	    pointVels[i][1] = -pointVels[i][1];
	}
	pointLocs[i] = pointLoc;
    }
    drawPoints();
    // update tracer location
    tracerX = tracerX + tracerVX * 8/frameDelay * 3;
    tracerY = tracerY + tracerVY * 8/frameDelay * 3;
    // stop if it's close to the mouse location
    if(Math.sqrt((tracerPX - tracerX) ** 2 + (tracerPY - tracerY) ** 2) <= 2){
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
    }
    // stop if it's close to the edge
    if((tracerX <= minX) || (tracerX >= maxX) ||
       (tracerY <= minY) || (tracerY >= maxY)){
	tracerVX = 0;
	tracerVY = 0;
    }
    updatePointer();
}

function updatePointer(){
    pointerDot = backgroundDocument.getElementById("pointerdot");
    if(pointerVisible){
	pointerDot.setAttribute("cx", tracerX);
	pointerDot.setAttribute("cy", tracerY);
    } else {
	pointerDot.setAttribute("cx", -100);
	pointerDot.setAttribute("cy", -100);
    }
}

function movePointer(e){
    pointerVisible = true;
    tracerPX = e.offsetX;
    tracerPY = e.offsetY;
    if(Math.sqrt((tracerPX - tracerX) ** 2 + (tracerPY - tracerY) ** 2) <= 2){
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
    } else {
	tracerVAng = Math.atan2(tracerPY - tracerY, tracerPX - tracerX);
	tracerVX = Math.cos(tracerVAng);
	tracerVY = Math.sin(tracerVAng);
    }
}

function startAnimation(){
    moveId = setInterval(processFrame, (1000/fps));
}

function clearPointer(e){
    //pointerVisible = false;
    updatePointer();
}
