// Space Battle game -- Space battle simulation game, with SVG and javascript
// author: David Eccles (gringer) <bioinformatics@gringene.org>

var backgroundDocument;
var started = false;
var leftEdge = 0;
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second

var finished = false;

// game / screen settings
var svgWidth = 500;
var svgHeight = 500;
var gameWidth = 500;
var gameHeight = 500;

var randSeed = new Date().getTime();
var gameSeed = randSeed;
var lastLog = 0;
var maxShips = 1; // maximum number of ships
var ns = 1; // number of ships
var nai = 0; // number of AIs

var moveId = setInterval(processFrame, (1000/fps));

var shipMass = 1;
var planetMass = 100;
var planetX = 250;
var planetY = 250;
var planetZ = 250;
var healthX = 50;
var healthY = 50;
var healthZ = 250;
var healthPct = 50;

var motionRotation = [0];
var pointerX = 250;
var pointerY = 250;

var shipX = [150];
var shipY = [150];
var shipZ = [250];
var shipAngleLR = [0];
var shipAngleUD = [0];
var shipDX = [0];
var shipDY = [0];
var shipDZ = [0];
var shipEnergy = [100];
var shipLife = [100];
var shipDeaths = [0];

var turnLeft = [false];
var turnRight = [false];
var turnUp = [false];
var turnDown = [false];
var accel = [false];
var shipShooting = [false];
var shipCanShoot = [false];
var shipRecharging = [false];

document.onkeydown = processKeyDown;
document.onkeyup = processKeyUp;

// https://stackoverflow.com/a/24392281/3389895
function intersects(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) {
  var det, gamma, lambda;
  det = (ax2 - ax1) * (by2 - by1) - (bx2 - bx1) * (ay2 - ay1);
  if (det === 0) {
    return false;
  } else {
    lambda = ((by2 - by1) * (bx2 - ax1) + (bx1 - bx2) * (by2 - ay1)) / det;
    gamma = ((ay1 - ay2) * (bx2 - ax1) + (ax2 - ax1) * (by2 - ay1)) / det;
    return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
  }
};

// https://stackoverflow.com/a/18473154/3389895
function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
}

function arcDef(cx, cy, r, sAng, eAng){
    var start = polarToCartesian(cx, cy, r, eAng);
    var end = polarToCartesian(cx, cy, r, sAng);
    var largeArcFlag = eAng - sAng <= 180 ? "0" : "1";
    var d = [
        "M", start.x, start.y, 
        "A", r, r, 0, largeArcFlag, 0, end.x, end.y
    ].join(" ");
    return(d);
}

function to2Ds(px, py, pz){
    sx = Math.round(10 * ((px-250) * (pz+500)/1000 + 250)) / 10;
    sy = Math.round(10 * ((py-250) * (pz+500)/1000 + 250)) / 10;
    return("" + sx + "," + sy);
}

function laserHasHit(fromShip, toShip){
    fromShipAngle = shipAngleLR[fromShip];
    fromShipX = shipX[fromShip];
    fromShipY = shipY[fromShip];
    toShipAngle = shipAngleLR[toShip];
    toShipX = shipX[toShip];
    toShipY = shipY[toShip];
    if(Math.abs(fromShipX - toShipX) > 250){
	if(fromShipX < 250){
	    fromShipX += 500;
	} else {
	    toShipX += 500;
	}
    }
    if(Math.abs(fromShipY - toShipY) > 250){
	if(fromShipY < 250){
	    fromShipY += 500;
	} else {
	    toShipY += 500;
	}
    }
    fromRad = (fromShipAngle + 90) * (Math.PI / 180);
    laserLineX1 = fromShipX + 20 * Math.cos(fromRad);
    laserLineY1 = fromShipY + 20 * Math.sin(fromRad);
    laserLineX2 = fromShipX + 120 * Math.cos(fromRad);
    laserLineY2 = fromShipY + 120 * Math.sin(fromRad);
    // Check laser against all triangle edges
    toRad = (toShipAngle + 90) * (Math.PI / 180);
    toRad90 = (toShipAngle) * (Math.PI / 180);
    res = false;
    toLineX1 = toShipX + 5 * Math.cos(toRad90);
    toLineY1 = toShipY + 5 * Math.sin(toRad90);
    toLineX2 = toShipX + 20 * Math.cos(toRad);
    toLineY2 = toShipY + 20 * Math.sin(toRad);
    res = res || intersects(laserLineX1, laserLineY1, laserLineX2, laserLineY2,
			    toLineX1, toLineY1, toLineX2, toLineY2);
    toLineX1 = toShipX - 5 * Math.cos(toRad90);
    toLineY1 = toShipY - 5 * Math.sin(toRad90);
    toLineX2 = toShipX + 20 * Math.cos(toRad);
    toLineY2 = toShipY + 20 * Math.sin(toRad);
    res = res || intersects(laserLineX1, laserLineY1, laserLineX2, laserLineY2,
			    toLineX1, toLineY1, toLineX2, toLineY2);
    toLineX1 = toShipX - 5 * Math.cos(toRad90);
    toLineY1 = toShipY - 5 * Math.sin(toRad90);
    toLineX2 = toShipX + 5 * Math.cos(toRad90);
    toLineY2 = toShipY + 5 * Math.sin(toRad90);
    res = res || intersects(laserLineX1, laserLineY1, laserLineX2, laserLineY2,
			    toLineX1, toLineY1, toLineX2, toLineY2);
    return(res);
}

// the game only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*9301+49297) % 233280;
    return randSeed/(233280.0);
}

function startup(evt) {
    backgroundDocument = evt.target.ownerDocument;
    makeGame(Math.floor(Math.random() * 10000), ns, nai);
    // motion tracking from https://sensor-js.xyz/demo.html
    if (DeviceMotionEvent &&
	typeof(DeviceMotionEvent.requestPermission) === "function") {
	DeviceMotionEvent.requestPermission();
    }
}

function updateScores(){
    var scoretext = backgroundDocument.getElementById("seedlabel");
    nodeText = "Seed: " + gameSeed;
    for(si = 0; si < ns; si++){
	nodeText += "; Ship " + (si+1) + ": " + shipDeaths[si];
    }
    scoretext.firstChild.nodeValue = nodeText;
}

function resetLifeAndEnergy(fromShip){
    if(shipEnergy.length <= fromShip){
	shipEnergy.push(100);
	shipLife.push(100);
	shipDeaths.push(0);
	turnLeft.push(false);
	turnRight.push(false);
	accel.push(false);
	shipShooting.push(false);
	shipCanShoot.push(false);
	shipRecharging.push(false);
    }
    shipEnergy[fromShip] = 100;
    shipLife[fromShip] = 100;
}

function resetShipPos(fromShip, newAng = Math.random() * 360){
    if(shipX.length <= fromShip){
	shipX.push(150);
	shipY.push(150);
	shipAngleLR.push(0);
	shipDX.push(0);
	shipDY.push(0);
	shipDZ.push(0);
    }
    shipX[fromShip] = 250 + 150 * Math.cos(newAng  * (Math.PI / 180));
    shipY[fromShip] = 250 + 150 * Math.sin(newAng  * (Math.PI / 180));
    shipZ[fromShip] = 250;
    shipDX[fromShip] = 0;
    shipDY[fromShip] = 0;
    shipDZ[fromShip] = 0;
    turnLeft[fromShip] = false;
    turnRight[fromShip] = false;
    shipShooting[fromShip] = false;
    shipCanShoot[fromShip] = false;
    shipRecharging[fromShip] = false;
    if((nai > 0) & (fromShip >= (ns-nai))){
	lrpA = shipAngleLR[fromShip];
	udpA = shipAngleUD[fromShip];
	lrFromRad = (lrpA + 90) * (Math.PI / 180);
	udFromRad = (udpA) * (Math.PI / 180);
	accel[fromShip] = true;
	shipDX[fromShip] = 1 * Math.cos(udFromRad) * Math.cos(lrFromRad);
	shipDY[fromShip] = 1 * Math.cos(udFromRad) * Math.sin(lrFromRad);
	shipDZ[fromShip] = 1 * Math.sin(udFromRad);
    } else {
	accel[fromShip] = false;
    }
}

function makeGame(seed, numShips, numAIs){
    randSeed = seed;
    gameSeed = seed;
    ns = numShips;
    nai = numAIs;
    setForm();
    var seedtext = backgroundDocument.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var game = backgroundDocument.getElementById("gamesetup");
    game.setAttribute("width", svgWidth + "px");
    game.setAttribute("height", svgHeight + "px");
    randSeed = seed;
    gameSeed = seed;
    // draw points
    startAng = Math.random() * 360;
    for(si = 0; si < maxShips; si++){
	var shipNode = backgroundDocument.getElementById("3dShip"+si);
	shipNode.setAttribute("visibility", "hidden");
    }
    for(si = 0; si < ns; si++){
	var shipNode = backgroundDocument.getElementById("3dShip"+si);
	shipNode.setAttribute("visibility", "visible");
	resetShipPos(fromShip = si, newAng = si * (360 / ns) + startAng);
	resetLifeAndEnergy(fromShip = si);
    }
    // reset health spot
    healthAng = Math.random() * 360;
    healthX = 250 + 200 * Math.cos(healthAng * (Math.PI / 180));
    healthY = 250 + 200 * Math.sin(healthAng * (Math.PI / 180));
    updateScores();
    drawPoints();
    stopAnimation();
    startAnimation();
}


function shipPath(pX, pY, pZ, lrAng, udAng){
    lrRad = (lrAng + 90) * (Math.PI / 180);
    lrRad90 = (lrAng) * (Math.PI / 180);
    udRad = (udAng) * (Math.PI / 180);
    noseX = pX + 40 * Math.cos(lrRad) * Math.cos(udRad);
    noseY = pY + 40 * Math.sin(lrRad) * Math.cos(udRad);
    noseZ = pZ + 40 * Math.sin(udRad);
    bX1 = pX - 10 * Math.cos(lrRad90);
    bY1 = pY - 10 * Math.sin(lrRad90);
    bZ1 = pZ;
    bX2 = pX + 10 * Math.cos(lrRad90);
    bY2 = pY + 10 * Math.sin(lrRad90);
    bZ2 = pZ;
    bX3 = pX + Math.cos(lrRad) * Math.sin(udRad) * 10;
    bY3 = pY + Math.sin(lrRad) * Math.sin(udRad) * 10;
    bZ3 = pZ + Math.cos(udRad) * 10;
    pts = [[bX1, bY1, bZ1],
	   [noseX, noseY, noseZ],
	   [bX2, bY2, bZ2],
	   ["Z"],
	   [bX1, bY1, bZ1],
	   [bX3, bY3, bZ3],
	   [noseX, noseY, noseZ],
	   ["Z"],
	   [bX2, bY2, bZ2],
	   [bX3, bY3, bZ3],
	   [noseX, noseY, noseZ],
	   ["Z"]
	  ];
    lastEnd = true;
    outPath = "";
    for(pi = 0; pi < pts.length; pi++){
	if(pts[pi].length < 3){
	    outPath += "Z";
	    lastEnd = true;
	} else if(lastEnd){
	    outPath += " M" + to2Ds(pts[pi][0], pts[pi][1], pts[pi][2]);
	    lastEnd = false;
	} else {
	    outPath += " L" + to2Ds(pts[pi][0], pts[pi][1], pts[pi][2]);
	}
    }
    return(outPath);
}

function drawPoints(){
    box3DNode = backgroundDocument.getElementById("pBox3D");
    box3DNode.setAttribute("d",
			   "M" + to2Ds(0,0,0) +
			   " L" + to2Ds(500,0,0) +
			   " L" + to2Ds(500,500,0) +
			   " L" + to2Ds(0,500,0) + "Z " +
			   "M" + to2Ds(0,0,250) +
			   " L" + to2Ds(500,0,250) +
			   " L" + to2Ds(500,500,250) +
			   " L" + to2Ds(0,500,250) + "Z " +
			   "M" + to2Ds(0,0,500) +
			   " L" + to2Ds(500,0,500) +
			   " L" + to2Ds(500,500,500) +
			   " L" + to2Ds(0,500,500) + "Z " +
			   " M" + to2Ds(0,0,0) +
			   " L" + to2Ds(0,0,500) +
			   " M" + to2Ds(0,500,0) +
			   " L" + to2Ds(0,500,500) +
			   " M" + to2Ds(500,500,0) +
			   " L" + to2Ds(500,500,500) +
			   " M" + to2Ds(500,0,0) +
			   " L" + to2Ds(500,0,500));
    for(si = 0; si < ns; si++){
	guideNode = backgroundDocument.getElementById("pShip"+si+"guide");
	tShipX = shipX[si];
	tShipY = shipY[si];
	tShipZ = shipZ[si];
	guideNode.setAttribute("d", "M" + to2Ds(0,0,tShipZ) +
			       " L" + to2Ds(500,0,tShipZ) +
			       " L" + to2Ds(500,500,tShipZ) +
			       " L" + to2Ds(0,500,tShipZ) + "Z");
	ship3DNode = backgroundDocument.getElementById("pShip"+si+"3D");
	ship3DNode.setAttribute("d",
				shipPath(shipX[si], shipY[si], shipZ[si],
					 shipAngleLR[si], shipAngleUD[si]));
	udMul = Math.cos(shipAngleUD[si] * (Math.PI / 180));
	htMul = (shipZ[si] + 500) / 1000;
	shipLaserG = backgroundDocument.getElementById("gShip"+si+"Laser");
	shipLaserP = backgroundDocument.getElementById("pShip"+si+"Laser");
	if(shipShooting[si] && shipCanShoot[si]){
	    pX=shipX[si]; pY=shipY[si]; pZ=shipZ[si];
	    lasPath = "";
	    shipLaserG.setAttribute("stroke","red");
	    lrRad = (shipAngleLR[si] + 90) * (Math.PI / 180);
	    lrRad90 = (shipAngleLR[si]) * (Math.PI / 180);
	    udRad = (shipAngleUD[si]) * (Math.PI / 180);
	    udRad90 = (shipAngleUD[si] + 90) * (Math.PI / 180);
	    noseX = pX + 40 * Math.cos(lrRad) * Math.cos(udRad);
	    noseY = pY + 40 * Math.sin(lrRad) * Math.cos(udRad);
	    noseZ = pZ + 40 * Math.sin(udRad);
	    endMidX = noseX + 200 * Math.cos(lrRad) * Math.cos(udRad);
	    endMidY = noseY + 200 * Math.sin(lrRad) * Math.cos(udRad);
	    endMidZ = noseZ + 200 * Math.sin(udRad);
	    //endMidX = 250; noseX = 250;
	    //endMidY = 250; noseY = 250;
	    //endMidZ = 250; noseZ = 250;
	    for(udtA = 0; udtA < 360; udtA += 20){
		udtrA = udtA * (Math.PI / 180);
		udtP = udtA / 180 - 1;
		lraD = Math.cos(udtrA);
		udaD = Math.sin(udtrA);
		bX2 = (lraD * 50);
		bY2 = (udaD * 50) * Math.sin(udRad);
		bZ2 = (udaD * 50) * Math.cos(udRad);
		// rotate around XY angle
		bXYr = Math.sqrt(bX2 ** 2 + bY2 ** 2);
		bXYa = Math.atan2(bY2, bX2) + lrRad90;
		bX2 = bXYr * Math.cos(bXYa);
		bY2 = bXYr * Math.sin(bXYa);
		endX = endMidX + bX2;
		endY = endMidY + bY2;
		endZ = endMidZ + bZ2;
		//console.log("hello " + [noseX, noseY, noseZ] + "; " + [endX, endY, endZ] + "; ");
		lasPath += "M"+ to2Ds(noseX, noseY, noseZ) + " L" + to2Ds(endX, endY, endZ) + " ";
	    }
	    shipLaserP.setAttribute("d", lasPath);
	} else {
	    oldVB = shipLaserG.setAttribute("stroke","none");
	}
	lifeNode = backgroundDocument.getElementById("pHealth" + si);
	lifeNode.setAttribute("d", "M"+to2Ds(tShipX,tShipY-5,tShipZ)+" h" +
			      (shipLife[si]/100) * 20);
	energyNode = backgroundDocument.getElementById("pEnergy" + si);
	energyNode.setAttribute("d", "M"+to2Ds(tShipX,tShipY-10,tShipZ)+" h" +
				(shipEnergy[si]/100) * 20);
    }
    // draw health point
    healthNode = backgroundDocument.getElementById("gHealPlace");
    oldVB = healthNode.setAttribute("transform","translate(" +
				    to2Ds(healthX, healthY, healthZ) + ")");
    lifePctNode = backgroundDocument.getElementById("healProgress");
    lifePctNode.setAttribute("d", arcDef(0,0,40,0,360*(healthPct/100)));
    // draw orientation marker (if device has orientation support)
    // nodeText = "[" + motionRotation[0] + "," +
    // 	motionRotation[1] + "," + motionRotation[2] + "]";
    // debugNode = backgroundDocument.getElementById("debuglabel");
    // debugNode.firstChild.nodeValue = nodeText;
    if(motionRotation.length >= 3){
	angX = motionRotation[2] / 30;
	angY = motionRotation[1] / 30;
	pointerX = (angX * 250 + 250) % 500;
	pointerY = (angY * 250 + 250) % 500;
	orientationNode = backgroundDocument.getElementById("orientationMarker");
	orientationNode.setAttribute("visibility", "visible");
	orientationNode.setAttribute("transform", "translate(" + pointerX + "," + pointerY + ")");
    }
}

function setForm() {
    var inputForm = backgroundDocument.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = gameSeed;
        inputForm.numships.value = ns;
        var speedValue = -1;
        for(var i = 0; i < inputForm.gamespeed.length; i++){
            if(inputForm.gamespeed[i].checked){
                speedValue = i;
            }
        }
        if(speedValue == -1){
            inputForm.gamespeed[2].checked = true;
            speedValue = 2;
        }
        if(speedValue == 0){
            frameDelay = 45;
        }
        if(speedValue == 1){
            frameDelay = 20;
        }
        if(speedValue == 2){
            frameDelay = 12;
        }
        if(speedValue == 3){
            frameDelay = 8;
        }
    }
}

function processFrame(){
    processAIs();
    processKeyPresses();
    // Planet gravity
    for(si = 0; si < ns; si++){
	planetDist = Math.sqrt((shipX[si] - planetX) ** 2 + (shipY[si] - planetY) ** 2 + (shipZ[si] - planetZ) ** 2);
	if(planetDist < 10){
	    if(shipLife[si] > 1){
		shipLife[si] = shipLife[si] / 2;
	    } else {
		shipLife[si] = 0;
	    }
	    resetShipPos(fromShip = si);
	} else if(planetDist < 100){
	    shipEnergy[si] += (1 * (100 - planetDist) / 100);
	    if(shipEnergy[si] >= 100){
		shipEnergy[si] = 100;
	    }
	    planetForce = shipMass * planetMass / (planetDist ** 2);
	    if(planetForce > 5){
		planetForce = 5;
	    }
	    shipDX[si] = shipDX[si] + ((planetX - shipX[si]) / planetDist) * planetForce;
	    shipDY[si] = shipDY[si] + ((planetY - shipY[si]) / planetDist) * planetForce;
	    shipDZ[si] = shipDZ[si] + ((planetZ - shipZ[si]) / planetDist) * planetForce;
	    Dlen = Math.sqrt(shipDX[si] * shipDX[si] + shipDY[si] * shipDY[si] + shipDZ[si] * shipDZ[si]);
	    if(Dlen > 5){
		shipDX[si] = shipDX[si] * (5 / Dlen);
		shipDY[si] = shipDY[si] * (5 / Dlen);
		shipDZ[si] = shipDZ[si] * (5 / Dlen);
	    }
	}
	// check health point
	healthDist = Math.sqrt((shipX[si] - healthX) ** 2 + (shipY[si] - healthY) ** 2 + (shipZ[si] - healthZ) ** 2);
	if((healthDist < 40) && (shipLife[si] < 90)){
	    if(healthDist < 5){
		healthPct += 2;
	    } else {
		healthPct += 2 * (35 - (healthDist - 5)) / 35;
	    }
	    if(healthPct >= 100){
		// heal 25% of lost life
		shipLife[si] += (100 - shipLife[si]) / 4;
		if(shipLife[si] >= 100){
		    shipLife = 100;
		}
	    }
	}
	// Process velocities
	shipX[si] = ((shipX[si] + shipDX[si] / (frameDelay/20)) % gameWidth + gameWidth) % gameWidth;
	shipY[si] = ((shipY[si] + shipDY[si] / (frameDelay/20)) % gameHeight + gameHeight) % gameHeight;
	shipZ[si] = ((shipZ[si] + shipDZ[si] / (frameDelay/20)) % gameHeight + gameHeight) % gameHeight;
    }
    // reset health location if anyone is inside when it triggers
    if(healthPct >= 100){
	healthPct = 0;
	healthAng = Math.random() * 360;
	healthX = 250 + 200 * Math.cos(healthAng * (Math.PI / 180));
	healthY = 250 + 200 * Math.sin(healthAng * (Math.PI / 180));
	healthZ = 250;
    }
    drawPoints();
    updatePointer();
}

function accelerate(){
    updatePointer();
}

function updatePointer(){
}

function movePointer(e){
}

function handleOrientation(event) {
    if(motionRotation.length < 3){
	motionRotation = [0,0,0];
    }
    if(event.alpha != null){
	motionRotation[0] = event.alpha.toFixed(2);
    }
    if(event.beta != null){
	motionRotation[1] = event.beta.toFixed(2);
    }
    if(event.gamma != null){
	motionRotation[2] = event.gamma.toFixed(2);
    }
}

function startAnimation(){
    moveId = setInterval(processFrame, (1000/fps));
    window.addEventListener("deviceorientation", handleOrientation);
}

function stopAnimation(){
    clearInterval(moveId);
    window.removeEventListener("deviceorientation", handleOrientation);
}

function clearPointer(e){
    //pointerVisible = false;
    updatePointer();
}

function processAIs(){
    for(si = ns - nai; si < ns; si++){
	pX = shipX[si];
	pY = shipY[si];
	pA = shipAngleLR[si];
	fromRad = (pA + 90) * (Math.PI / 180);
	pX = (pX + 20 * Math.cos(fromRad)) % 500;
	pY = (pY + 20 * Math.sin(fromRad)) % 500;
	nearestX = shipX[(si + 1) % ns];
	nearestY = shipY[(si + 1) % ns];
	nearestDist = Math.sqrt((nearestX - pX) ** 2 + (nearestY - pY) ** 2);
	// find closest other ship
	for(soi = 0; soi < ns; soi++){
	    if(soi != si){
		npX = pX;
		npY = pY;
		tX = shipX[soi];
		tY = shipY[soi];
		if(Math.abs(npX - tX) > 250){
		    if(npX < 250){
			npX += 500;
		    } else {
			tX += 500;
		    }
		}
		if(Math.abs(npY - tY) > 250){
		    if(npY < 250){
			npY += 500;
		    } else {
			tY += 500;
		    }
		}
		newDist = Math.sqrt((tX - npX) ** 2 + (tY - npY) ** 2);
		if(newDist < nearestDist){
		    nearestX = tX;
		    nearestY = tY;
		    nearestDist = newDist;
		}
	    }
	}
	// rotate towards it
	idealAngle = (Math.atan2(nearestY-pY, nearestX - pX) / Math.PI * 180 - 90 + 360) % 360;
	if(Math.abs(idealAngle - pA) > 180){
	    if(idealAngle < 180){
		idealAngle += 360;
	    } else {
		pA += 360;
	    }
	}
	changeAngle = idealAngle - pA;
	if(changeAngle > 180){
	    changeAngle = 360 - changeAngle;
	}
	if(changeAngle > 0){
	    turnRight[si] = true;
	    turnLeft[si] = false;
	} else if(changeAngle < 0){
	    turnLeft[si] = true;
	    turnRight[si] = false;
	} else {
	    turnLeft[si] = false;
	    turnRight[si] = false;
	}
	// accelerate towards it if it's too far away
	speed = Math.sqrt(shipDX[si] ** 2 + shipDY[si] ** 2);
	if(nearestDist > 100){
	    shipShooting[si] = false;
	    if((speed < 5) || (Math.abs(changeAngle) > 5)){
		accel[si] = true;
	    } else {
		accel[si] = false;
		if(Math.abs(changeAngle <= 3)){
		    turnLeft[si] = false;
		    turnRight[si] = false;
		}
	    }
	} else {
	    if(speed < 1){
		// move a little if not moving [stops a freeze bug]
		accel[si] = true;
	    }
	    // regenerate energy if not close enough
	    if(shipEnergy[si] <= 20){
		accel[si] = false;
		turnLeft[si] = false;
		turnRight[si] = false;
	    } else {
		// fire if pointing roughly in the right direction
		// and there's enough energy
		if(speed >= 1){
		    accel[si] = false;
		}
		if(Math.abs(changeAngle) < 10){
		    shipShooting[si] = true;
		} else {
		    shipShooting[si] = false;
		}
	    }
	}
    }
    if((nai < ns) && (motionRotation.length == 3)){
	// move the first ship to the pointer
	pX = shipX[0];
	pY = shipY[0];
	pA = shipAngleLR[0];
	fromRad = (pA + 90) * (Math.PI / 180);
	pX = (pX + 20 * Math.cos(fromRad)) % 500;
	pY = (pY + 20 * Math.sin(fromRad)) % 500;
	tX = pointerX;
	tY = pointerY;
	pointerDist = Math.sqrt((tX - pX) ** 2 + (tY - pY) ** 2);
	idealAngle = (Math.atan2(tY - pY, tX - pX) / Math.PI * 180 - 90 + 360) % 360;
	if(Math.abs(idealAngle - pA) > 180){
	    if(idealAngle < 180){
		idealAngle += 360;
	    } else {
		pA += 360;
	    }
	}
	changeAngle = idealAngle - pA;
	if(changeAngle > 180){
	    changeAngle = 360 - changeAngle;
	}
	if(changeAngle > 0){
	    turnRight[0] = true;
	    turnLeft[0] = false;
	} else if(changeAngle < 0){
	    turnLeft[0] = true;
	    turnRight[0] = false;
	} else {
	    turnLeft[0] = false;
	    turnRight[0] = false;
	}
	speed = Math.sqrt(shipDX[0] ** 2 + shipDY[0] ** 2);
	if(pointerDist > 100){
	    accel[0] = true;
	} else if((pointerDist > 20)  && (speed < 1)){
	    accel[0] = true;
	} else {
	    accel[0] = false;
	}
	nearestX = shipX[1 % ns];
	nearestY = shipY[1 % ns];
	nearestDist = Math.sqrt((nearestX - pX) ** 2 + (nearestY - pY) ** 2);
	// find closest other ship
	for(soi = 1; soi < ns; soi++){
	    npX = pX;
	    npY = pY;
	    tX = shipX[soi];
	    tY = shipY[soi];
	    if(Math.abs(npX - tX) > 250){
		if(npX < 250){
		    npX += 500;
		} else {
		    tX += 500;
		}
	    }
	    if(Math.abs(npY - tY) > 250){
		if(npY < 250){
		    npY += 500;
		} else {
		    tY += 500;
		}
	    }
	    newDist = Math.sqrt((tX - npX) ** 2 + (tY - npY) ** 2);
	    if(newDist < nearestDist){
		nearestX = tX;
		nearestY = tY;
		nearestDist = newDist;
	    }
	}
	if(nearestDist < 100){
	    // work out if pointing roughly at the nearest ship
	    idealAngle = (Math.atan2(nearestY-pY, nearestX - pX) / Math.PI * 180 - 90 + 360) % 360;
	    if(Math.abs(idealAngle - pA) > 180){
		if(idealAngle < 180){
		    idealAngle += 360;
		} else {
		    pA += 360;
		}
	    }
	    changeAngle = idealAngle - pA;
	    if(changeAngle > 180){
		changeAngle = 360 - changeAngle;
	    }
	    // regenerate energy if needed
	    if((shipEnergy[0] <= 20) && (pointerDist < 50)){
		accel[0] = false;
		turnLeft[0] = false;
		turnRight[0] = false;
	    } else {
		// fire if pointing roughly in the right direction
		// and there's enough energy
		if(pointerDist < 50){
		    accel[0] = false;
		}
		if(Math.abs(changeAngle) < 10){
		    shipShooting[0] = true;
		} else {
		    shipShooting[0] = false;
		}
	    }
	}
    }
}

function processKeyPresses(){
    for(si = 0; si < ns; si++){
	if(turnLeft[si]){
	    shipAngleLR[si] = (shipAngleLR[si] - 5 + 360) % 360;
	}
	if(turnRight[si]){
	    shipAngleLR[si] = (shipAngleLR[si] + 5 + 360) % 360;
	}
	if(turnUp[si]){
	    shipAngleUD[si] = (shipAngleUD[si] - 5 + 360) % 360;
	}
	if(turnDown[si]){
	    shipAngleUD[si] = (shipAngleUD[si] + 5 + 360) % 360;
	}
	if(accel[si]){
	    udA = (shipAngleUD[si]) * (Math.PI / 180);
	    lrA = (shipAngleLR[si] + 90) * (Math.PI / 180);
	    shipDX[si] = shipDX[si] + Math.cos(udA) * Math.cos(lrA) * 0.2;
	    shipDY[si] = shipDY[si] + Math.cos(udA) * Math.sin(lrA) * 0.2;
	    shipDZ[si] = shipDZ[si] + Math.sin(udA) * 0.2;
	    Dlen = Math.sqrt(shipDX[si] ** 2 + shipDY[si] ** 2 + shipDZ[si] ** 2);
	    if(Dlen > 5){
		shipDX[si] = shipDX[si] * (5 / Dlen);
		shipDY[si] = shipDY[si] * (5 / Dlen);
		shipDZ[si] = shipDZ[si] * (5 / Dlen);
	    }
	}
	if(shipShooting[si]){
	    if((shipEnergy[si] > 20)){
		shipRecharging[si] = false;
	    }
	    if(shipEnergy[si] >= 5){
		if(!shipRecharging[si]){
		    shipEnergy[si] = shipEnergy[si] - 1;
		    shipCanShoot[si] = true;
		    for(oi = 0; oi < ns; oi++){
			if(oi != si){
			    if(laserHasHit(fromShip = si, toShip = oi)){
				shipLife[oi] -= 1;
			    }
			}
		    }
		}
	    } else {
		shipCanShoot[si] = false;
		shipRecharging[si] = true;
		shipNode = backgroundDocument.getElementById("pEnergy"+si);
		shipNode.setAttribute("stroke", "red");
	    }
	}
	if(!shipShooting[si] || !shipCanShoot[si]) {
	    if(!(accel[si] || turnLeft[si] || turnRight[si])){
		shipEnergy[si] += (1 * shipLife[si] / 100);
		if(shipEnergy[si] > 20){
		    shipNode = backgroundDocument.getElementById("pEnergy"+si);
		    shipNode.setAttribute("stroke", "orange");
		}
	    }
	    if(shipEnergy[si] >= 100){
		shipEnergy[si] = 100;
	    }
	}
    }
    // process deaths (done after shots have been processed)
    for(si = 0; si < ns; si++){
	if(shipLife[si] <= 0){
	    shipDeaths[si] += 1;
	    resetShipPos(fromShip = si);
	    shipLife[si] = 100;
	    updateScores();
	}
    }
}

function processKeyDown(e){
    if((e.code == "ArrowLeft") || (e.code == 'KeyA')){
	turnLeft[0] = true;
    } else if((e.code == "ArrowRight") || (e.code == 'KeyD')){
	turnRight[0] = true;
    } else if((e.code == "ArrowUp") || (e.code == 'KeyW')){
	turnUp[0] = true;
    } else if((e.code == "ArrowDown") || (e.code == 'KeyS')){
	turnDown[0] = true;
    } else if((e.code == "BracketLeft") || (e.code == 'KeyO')){
	accel[0] = true;
    } else if((e.code == "BracketRight") || (e.code == 'KeyP')){
	shipShooting[0] = true;
    }
}

function processKeyUp(e){
    if((e.code == "ArrowLeft") || (e.code == 'KeyA')){
	turnLeft[0] = false;
    } else if((e.code == "ArrowRight") || (e.code == 'KeyD')){
	turnRight[0] = false;
    } else if((e.code == "ArrowUp") || (e.code == 'KeyW')){
	turnUp[0] = false;
    } else if((e.code == "ArrowDown") || (e.code == 'KeyS')){
	turnDown[0] = false;
    } else if((e.code == "BracketLeft") || (e.code == 'KeyO')){
	accel[0] = false;
    } else if((e.code == "BracketRight") || (e.code == 'KeyP')){
	shipShooting[0] = false;
    }
}
