// Going Viral game -- viral isolation game, with SVG and javascript
// author: David Eccles (gringer) <bioinformatics@gringene.org>

var backgroundDocument;
var started = false;
var leftEdge = 0;
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second

var finished = false;

// game / screen settings
var svgWidth = 500;
var svgHeight = 500;
var gameWidth = 400;
var gameHeight = 400;

// point / particle model parameters
var numPoints = 150;
var pointSize = 10;
var pointLocs = new Array();
var pointVels = new Array();

// infection model parameters
var infectChance = 0.15;
var presympMin = 50;
var presympMax = 300;
var recoveredMin = 300;
var recoveredMax = 1200;
var isolated = new Array();
var infected = new Array();
var symptomatic = new Array();
var recovered = new Array();
var quarantined = new Array();
var hospitalised = new Array();
var contacts = new Array();
var framesToRecovered = new Array();
var framesToSymptomatic = new Array();

// tracer parameters
var tracerPX = 200;
var tracerPY = 200;
var tracerX = 200;
var tracerY = 200;
var tracerVX = 0;
var tracerVY = 0;
var traceLimit = 3;
var activeIsolateFrames = 0;
var pointerVisible = true;

// reward parameters
var totalMoney = 0;
var totalDays = 0;
var framesPerDay = 20;
// https://www.stats.govt.nz/topics/income
// 1. income per week: $1016
// https://www.mytax.co.nz/tax-resources/tax-rates-for-individuals-companies-and-trusts/
// 2. yearly tax at weekly median income: ((1016 * 52) - 48000) * 0.3 + (1470 + 5950)
// note: this ignores GST and other taxes
// 3. proportion of taxes devoted to infection: 30%
var rewardPerAsymp = (((1016 * 52) - 48000) * 0.3 + (1470 + 5950)) / 365 * 0.3;
// Assumes 30% of recovered people have trouble working
var rewardPerRecov = rewardPerAsymp * 0.7;
// not sure about this calculation... how much loss is there when a symptomatic person is in the community
var rewardPerSymp = -100;
// https://www.newstalkzb.co.nz/news/national/government-dumps-thousands-of-official-covid-19-papers/
var rewardPerIsolation = -3700;
var firstInfectionID = -1;
var firstInfection = true;
var alertedWon = false;

var randSeed = new Date().getTime();
var gameSeed = randSeed;
var lastLog = 0;

var moveId = setInterval(processFrame, (1000/fps));

// the game only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*9301+49297) % 233280;
    return randSeed/(233280.0);
}

function startup(evt) {
    backgroundDocument = evt.target.ownerDocument;
    makeGame(Math.floor(Math.random() * 10000));
}

function makeGame(seed){
    randSeed = seed;
    gameSeed = seed;
    setForm();
    var seedtext = backgroundDocument.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var game = backgroundDocument.getElementById("gamesetup");
    game.setAttribute("width", svgWidth + "px");
    game.setAttribute("height", svgHeight + "px");
    randSeed = seed;
    gameSeed = seed;
    // reset game variables
    isolated = new Array();
    infected = new Array();
    symptomatic = new Array();
    recovered = new Array();
    quarantined = new Array();
    hospitalised = new Array();
    framesToRecovered = new Array();
    framesToSymptomatic = new Array();
    contacts = new Array();
    // reset tracer variables
    tracerPX = 200;
    tracerPY = 200;
    tracerX = 200;
    tracerY = 200;
    tracerVX = 0;
    tracerVY = 0;
    activeIsolateFrames = 0;
    totalDays = 0;
    totalMoney = 0;
    pointerVisible = true;
    firstInfection = true;
    // reset win state
    alertedWon = false;
    // set up people
    for(var i=0; i < numPoints; i++){
	iFac = (i / numPoints);
 	moveAng = nextRandom() * Math.PI * 2;
	//moveAng = iFac * Math.PI * 2;
	pointLocs[i] = [nextRandom() * (gameWidth - pointSize) + pointSize/2,
			nextRandom() * (gameHeight - pointSize) + pointSize/2];
	//pointLocs[i] = [200 + Math.cos(moveAng)*150, 200 + Math.sin(moveAng)*150];
	pointVels[i] = [Math.cos(moveAng),
			Math.sin(moveAng)];
	contacts[i] = [];
    }
    // add one infected person
    for(var i = 0; i < 1; i++){
	var infectedID = Math.floor(Math.random() * numPoints);
	firstInfectionID = infectedID;
	infected[infectedID] = true;
    }
    // draw points
    drawPoints();
    clearInterval(moveId);
    moveId = setInterval(processFrame, (1000/fps));
}

function drawPoints(){
    var pointpath = backgroundDocument.getElementById("pointpath");
    var symptomaticpath = backgroundDocument.getElementById("symptomaticpath");
    var recoveredpath = backgroundDocument.getElementById("recoveredpath");
    var tracepath = backgroundDocument.getElementById("tracepath");
    var pointpathtext = "";
    var tracetext = "";
    var symptomatictext = "";
    var recoveredtext = "";
    var boxesHoriz = (gameWidth / (pointSize * 2)) + 2;
    var boxesVert = (gameHeight / (pointSize * 2)) + 2;
    var foundPoints = [];
    var collided = [];
    var addedCollision = [];
    var pointLoc = [];
    for(var xi = 0; xi < boxesHoriz; xi++){
	foundPoints[xi] = [];
	for(var yi = 0; yi < boxesHoriz; yi++){
	    foundPoints[xi][yi] = [];
	}
    }
    var tracedPerson = false;
    for(var i=0; i < numPoints; i++){
	pointLoc = pointLocs[i];
	// check to see if the point should be isolated
	if(Math.sqrt((tracerX - pointLoc[0]) ** 2 +
		     (tracerY - pointLoc[1]) ** 2) <= (pointSize * 1.5)){
	    if(!tracedPerson){
		ci = contacts[i];
		for(cii = 0; cii < ci.length; cii++){
		    if(!isolated[ci[cii]]){
			tracetext +=
			    "M" + pointLoc[0] + "," + pointLoc[1] +
			    "L" + pointLocs[ci[cii]][0] +
			    "," + pointLocs[ci[cii]][1] + " ";
		    }
		}
		tracedPerson = true;
	    }
	    if((activeIsolateFrames > 0) && (totalMoney > -rewardPerIsolation) && !recovered[i]){
		isolated[i] = true;
		totalMoney += rewardPerIsolation;
		pointLocs[i] = [400 + pointSize, pointSize];
		pointVels[i] = [0, 0];
		quarantined[quarantined.length] = i;
	    }
	}
	if(isolated[i]){
	    continue;
	}
	if(i > -1){
	    // Calculate collision bounding boxes
	    bbX = Math.floor(pointLoc[0] / 20); bbX1 = bbX+1;
	    bbY = Math.floor(pointLoc[1] / 20); bbY1 = bbY+1;
	    var bbHashes = [[bbX, bbY],  [bbX1, bbY],
			    [bbX, bbY1], [bbX1, bbY1]];
	    for(var bbi = 0; bbi < 4; bbi++){
		bbh = bbHashes[bbi];
		fp = foundPoints[bbh[0]][bbh[1]];
		if(fp.length == 0){
		    foundPoints[bbh[0]][bbh[1]][fp.length] = i;
		} else {
		    var addedCollision = false;
		    for(var fi = 0; fi < fp.length; fi++){
			var cp = pointLocs[fp[fi]]; // potentiallyCollidedPoint
			// sqrt((x0-x1)^2 + (y0-y1)^2)
			var dist = Math.sqrt((cp[0] - pointLoc[0]) ** 2 +
					     (cp[1] - pointLoc[1]) ** 2);
			// Note: this is slightly inaccurate, as it's an
			// instantaneous collision calculation. A more accurate
			// calculation would require comparing trajectories.
			if(dist < (pointSize)){ // collided
			    contacts[i][(contacts[i].length)] =
				fp[fi];
			    if(contacts[i].length > traceLimit){
				contacts[i].shift();
			    }
			    contacts[fp[fi]][(contacts[fp[fi]].length)] =
				fp[fi];
			    if(contacts[fp[fi]].length > traceLimit){
				contacts[fp[fi]].shift();
			    }
			    if((infected[i] && !recovered[i]) ||
			       (infected[fp[fi]] && !recovered[fp[fi]])){
				// infectChance should be based on time until symptomatic
				if(nextRandom() < infectChance){
				    if(!infected[i] || firstInfection){
					infected[i] = true;
					framesToRecovered[i] =
					    (Math.floor(nextRandom() *
							(recoveredMax - recoveredMin)) + recoveredMin) *
					    (frameDelay / 8);
					framesToSymptomatic[i] =
					    (Math.floor(nextRandom() *
							(presympMax - presympMin)) + presympMin) *
					    (frameDelay / 8);
				    }
				    if(!infected[fp[fi]] || firstInfection){
					infected[fp[fi]] = true;
					framesToRecovered[fp[fi]] =
					    (Math.floor(nextRandom() *
							(recoveredMax - recoveredMin)) + recoveredMin) *
					    (frameDelay / 8);
					framesToSymptomatic[i] =
					    (Math.floor(nextRandom() *
							(presympMax - presympMin)) + presympMin) *
					    (frameDelay / 8);
				    }
				    firstInfection = false;
				}
			    }
			    if(!addedCollision){
				collided[collided.length] = i;
				addedCollision = true;
			    }
			    collided[collided.length] = fp[fi];
			    // Another simplification - assuming the deflection point
			    // is right in the middle of the two points
			    // calculate collision normal vector
			    colMid = [(cp[0] + pointLoc[0])/2,
				      (cp[1] + pointLoc[1])/2];
			    // shift points away from collision in an attempt to reduce
			    // dancing points
			    cp[0] = cp[0] -
			    	(colMid[0] - cp[0]) * (pointSize - dist) / 2;
			    cp[1] = cp[1] -
			    	(colMid[1] - cp[1]) * (pointSize - dist) / 2;
			    pointLoc[0] = pointLoc[0] +
			    	(pointLoc[0] - colMid[0]) * (pointSize - dist) / 2;
			    pointLoc[1] = pointLoc[1] +
			    	(pointLoc[1] - colMid[1]) * (pointSize - dist) / 2;
			    // elastic collision of equal masses - just swap velocities!
			    vi1 = pointVels[i]; vi2 = pointVels[fp[fi]];
			    pointVels[i] = vi2;
			    pointVels[fp[fi]] = vi1;
			}
		    }
		    foundPoints[bbh[0]][bbh[1]][fp.length] = i;
		}
	    }
	}
    }
    var infectedCount = 0;
    var qc = 0;
    var hc = 0;
    for(var i=0; i < numPoints; i++){
	pointLoc = pointLocs[i];
	if(infected[i] && !recovered[i] && !isolated[i]){
	    infectedCount++;
	}
	if(isolated[i]){
	    if(!symptomatic[i]){
		pointLoc = [400 + (qc % (50 / pointSize) + 0.5) * (pointSize * 2),
			    pointSize + Math.floor(qc / (50 / pointSize)) * (pointSize * 2)];
		qc++;
	    } else if (symptomatic[i] || recovered[i]){
		pointLoc = [400 + (hc % (50 / pointSize) + 0.5) * (pointSize * 2),
			    200 + pointSize + Math.floor(hc / (50 / pointSize)) * (pointSize * 2)];
		hc++;
	    }
	}
	if(recovered[i]){
	    recoveredtext += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    if(!isolated[i]){
		totalMoney += (rewardPerRecov * 8/frameDelay) / framesPerDay;
	    }
	} else if(infected[i] && symptomatic[i]){
	    symptomatictext += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    totalMoney += (rewardPerSymp * 8/frameDelay) / framesPerDay;
	} else {
	    pointpathtext += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	    if(!isolated[i]){
		totalMoney += (rewardPerAsymp * 8/frameDelay) / framesPerDay;
	    }
	}
    }
    var infecttext = backgroundDocument.getElementById("infectlabel");
    infecttext.firstChild.nodeValue = "Infected: " + Math.floor(infectedCount);
    var scoretext = backgroundDocument.getElementById("scorelabel");
    scoretext.firstChild.nodeValue = "Money: $" + Math.floor(totalMoney) +
	"; Days: " + Math.floor(totalDays);;
    pointpath.setAttribute("d", pointpathtext);
    recoveredpath.setAttribute("d", recoveredtext);
    symptomaticpath.setAttribute("d", symptomatictext);
    tracepath.setAttribute("d", tracetext);
    if((infectedCount == 0) && !alertedWon){
	window.alert("Finished with $" + Math.ceil(totalMoney) +
		     " after " + Math.floor(totalDays) + " days\n\n" +
		     "Final score: " + Math.ceil(Math.max(0, totalMoney) * 100 / ((totalDays/10) ** 2)) / 100);
	clearInterval(moveId);
	alertedWon = true;
    }
}

function setForm() {
    var inputForm = backgroundDocument.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = gameSeed;
        var speedValue = -1;
        for(var i = 0; i < inputForm.gamespeed.length; i++){
            if(inputForm.gamespeed[i].checked){
                speedValue = i;
            }
        }
        if(speedValue == -1){
            inputForm.gamespeed[2].checked = true;
            speedValue = 2;
        }
        if(speedValue == 0){
            frameDelay = 45;
        }
        if(speedValue == 1){
            frameDelay = 20;
        }
        if(speedValue == 2){
            frameDelay = 12;
        }
        if(speedValue == 3){
            frameDelay = 8;
        }
    }
}

function processFrame(){
    totalDays += (8/frameDelay) / framesPerDay;
    var minX = pointSize/2;
    var minY = pointSize/2;
    var maxX = gameWidth - pointSize/2;
    var maxY = gameWidth - pointSize/2;
    for(var i=0; i < numPoints; i++){
	// recovery model
	if(framesToRecovered[i]){
	    if((--framesToRecovered[i]) <= 0){
		framesToRecovered[i] = 0;
		recovered[i] = true;
	    }
	}
	if(framesToSymptomatic[i]){
	    if((--framesToSymptomatic[i]) <= 0){
		framesToSymptomatic[i] = 0;
		symptomatic[i] = true;
	    }
	}
	if(isolated[i]){
	    continue;
	}
	// update point location
    	pointLoc = [pointLocs[i][0] + pointVels[i][0] * 8/frameDelay,
    		    pointLocs[i][1] + pointVels[i][1] * 8/frameDelay];
	// wall bounce collision check
	if(pointLoc[0] < minX){
	    pointLoc[0] = minX + (minX - pointLoc[0]);
	    pointVels[i][0] = -pointVels[i][0];
	}
	if(pointLoc[1] < minY){
	    pointLoc[1] = minY + (minY - pointLoc[1]);
	    pointVels[i][1] = -pointVels[i][1];
	}
	if(pointLoc[0] > maxX){
	    pointLoc[0] = maxX - (pointLoc[0] - maxX);
	    pointVels[i][0] = -pointVels[i][0];
	}
	if(pointLoc[1] > maxY){
	    pointLoc[1] = maxY - (pointLoc[1] - maxY);
	    pointVels[i][1] = -pointVels[i][1];
	}
	pointLocs[i] = pointLoc;
    }
    drawPoints();
    // update tracer location
    tracerX = tracerX + tracerVX * 8/frameDelay * 3;
    tracerY = tracerY + tracerVY * 8/frameDelay * 3;
    if(activeIsolateFrames > 0){
	activeIsolateFrames--;
    }
    // stop if it's close to the mouse location
    if(Math.sqrt((tracerPX - tracerX) ** 2 + (tracerPY - tracerY) ** 2) <= 2){
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
    }
    // stop if it's close to the edge
    if((tracerX <= minX) || (tracerX >= maxX) ||
       (tracerY <= minY) || (tracerY >= maxY)){
	tracerVX = 0;
	tracerVY = 0;
    }
    updatePointer();
}

function isolatePoint(){
    pointerDot = backgroundDocument.getElementById("pointerdot");
    activeIsolateFrames = 4 * (frameDelay / 8);
    updatePointer();
}


function updatePointer(){
    pointerDot = backgroundDocument.getElementById("pointerdot");
    if(pointerVisible){
	pointerDot.setAttribute("cx", tracerX);
	pointerDot.setAttribute("cy", tracerY);
    } else {
	pointerDot.setAttribute("cx", -100);
	pointerDot.setAttribute("cy", -100);
    }
    if(activeIsolateFrames > 0){
	pointerDot.setAttribute("fill", "#4682B480");
	pointerDot.setAttribute("stroke", "steelblue");
	pointerDot.setAttribute("stroke-width", "3");
    } else {
	pointerDot.setAttribute("fill", "none");
	pointerDot.setAttribute("stroke", "darkgreen");
	pointerDot.setAttribute("stroke-width", "1");
    }
}

function movePointer(e){
    pointerVisible = true;
    tracerPX = e.offsetX;
    tracerPY = e.offsetY;
    if(Math.sqrt((tracerPX - tracerX) ** 2 + (tracerPY - tracerY) ** 2) <= 2){
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
    } else {
	tracerVAng = Math.atan2(tracerPY - tracerY, tracerPX - tracerX);
	tracerVX = Math.cos(tracerVAng);
	tracerVY = Math.sin(tracerVAng);
    }
}

function startAnimation(){
    var infectedCount = 0;
    for(var i=0; i < numPoints; i++){
	if(infected[i] && !recovered[i] && !isolated[i]){
	    infectedCount++;
	}
    }
    if(infectedCount > 0){
	moveId = setInterval(processFrame, (1000/fps));
    }
}

function clearPointer(e){
    //pointerVisible = false;
    updatePointer();
}
