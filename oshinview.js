// OshinView - A sea-quence editor with a spirally twist
// author: David Eccles (gringer) <oshinview@gringene.org>

// License: GPLv3
// [https://gitlab.com/gringer/webgames/-/blob/master/LICENSE]

// For the most recent version of this code, see here:
// https://gitlab.com/gringer/webgames/-/blob/master/oshinview.html
// https://gitlab.com/gringer/webgames/-/blob/master/oshinview.js

// Ideas for later:
// * Consider ... <sequence> ... in spiral/any representation
//   to allow subsequence display (and reduce processor load).
//   A default to 10,000 bases shown seems reasonable.
// * Load view parameters on startup (if they exist in FASTA header)
// * Multi-sequence view (with automatic kmer-based alignment)
// * Add ruler
// * Bug: distance per base calculation for constant-distance is not
//        correct (it's based only on the number of rows, not columns)

var backgroundDocument = document;
var viewX;
var viewY;
var viewZoom;
var viewRotate;
var dragStartX;
var dragStartY;
var dragViewX;
var dragViewY;
var dragViewZoom;
var newViewX;
var newViewY;
var lastBasePosX;
var lastBasePosY;
var currentMouseX;
var currentMouseY;
var currentBasePosition;
var currentHeader;
const currentSequence = new Array();
const editOperations = new Array();
var resetViewRequested;
var seqCols;
var seqRows;
var seqLength;
var seqColWidth;
var seqRowHeight;
var seqOutputType;
var newViewZoom;
var appFocused;
var shiftPressed;
var controlPressed;
var altPressed;
var mouseLeftPressed;
var mouseDragging;
var lastMoveEvent;

function startup(evt) {
    backgroundDocument = evt.target.ownerDocument;
    // motion tracking from https://sensor-js.xyz/demo.html
    if (DeviceMotionEvent &&
	typeof(DeviceMotionEvent.requestPermission) === "function") {
	DeviceMotionEvent.requestPermission();
    }
    viewX = 25;
    viewY = 25;
    viewZoom = 0;
    viewRotate = 0;
    dragStartX = 0;
    dragStartY = 0;
    currentMouseX = 0;
    currentMouseY = 0;
    currentSequence.length = 0;
    currentHeader = "[No Sequence Loaded]";
    lastBasePosX = -1;
    lastBasePosY = -1;
    currentBasePosition = -1;
    appFocused = false;
    shiftPressed = false;
    controlPressed = false;
    altPressed = false;
    mouseLeftPressed = false;
    mouseDragging = false;
    resetViewRequested = true;
    seqCols = 0;
    seqRows = 0;
    seqOutputType = "";
    backgroundDocument.onkeydown = processKeyDown;
    backgroundDocument.onkeyup = processKeyUp;
    console.log("Started!");
}

// Setup file reading
// [see https://stackoverflow.com/a/49092712/3389895 ]
var reader = new FileReader();
reader.onload = handleFileRead;
// variable for the namespace 
const svgns = "http://www.w3.org/2000/svg";


function handleFileUpload(){
    var file = event.target.files[0];
    reader.readAsText(file); // fires onload when done.
}

function handleFileRead(event) {
    var saveText = event.target.result;
    // https://stackoverflow.com/a/21712066/3389895
    var lines = saveText.split(/\r?\n|\r|\n/g);
    currentHeader = lines.shift();
    var clearFromHere = false;
    for(var li = 0; li < lines.length; li++){
	if(clearFromHere || (lines[li][0] == ">")){
	    clearFromHere = true;
	    lines[li] = "";
	}
    }
    var tmpSequence = lines.join("");
    editOperations.length = 0;
    editOperations.push("sequence:" + tmpSequence);
    currentSequence.length = 0;
    currentSequence.push(...tmpSequence.split(""));
    window.localStorage.setItem("names_0", currentHeader);
    window.localStorage.setItem("sequences_0", tmpSequence);
    resetViewRequested = true;
    requestAnimationFrame(drawSequence);
}

function saveSequence(fileType){
    if(currentSequence.length == 0){
	return;
    } else if(fileType == "svg"){
	// https://stackoverflow.com/a/38019175/3389895
	var svgNode = backgroundDocument.getElementById("oshinDisplay");
	// make pointer dot transparent
	var dotNode = backgroundDocument.getElementById("pointerdot");
	dotNode.setAttribute("opacity", 0);
	var serializer = new XMLSerializer();
	// restore image parameters
	dotNode.setAttribute("opacity", 1);
	var svgData = serializer.serializeToString(svgNode);
	var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"});
	var svgUrl = URL.createObjectURL(svgBlob);
	var downloadLink = backgroundDocument.createElement("a");
	downloadLink.href = svgUrl;
	downloadLink.download = "oshinview.svg";
	backgroundDocument.body.appendChild(downloadLink);
	downloadLink.click();
	backgroundDocument.body.removeChild(downloadLink);
    } else if(fileType == "png"){
	// inspired by https://stackoverflow.com/a/45334496/3389895
	var svgNode = backgroundDocument.getElementById("oshinDisplay");
	// make image temporarily larger
	svgNode.setAttribute("width", 1600);
	svgNode.setAttribute("height", 800);
	// make pointer dot transparent
	var dotNode = backgroundDocument.getElementById("pointerdot");
	dotNode.setAttribute("opacity", 0);
	var serializer = new XMLSerializer();
	var svgData = 'data:image/svg+xml;base64,' + btoa(serializer.serializeToString(svgNode));
	// restore image parameters
	svgNode.setAttribute("width", 800);
	svgNode.setAttribute("height", 400);
	dotNode.setAttribute("opacity", 1);
	const canvas = backgroundDocument.createElement('canvas');
	canvas.id = "tmpCanvas";
	canvas.width = 1600;
	canvas.height = 800;
	var svgImage = backgroundDocument.createElement('img');
	svgImage.id = "tmpImage";
	svgImage.width = 1600;
	svgImage.height = 800;
	svgImage.src = svgData;
	var seaquenceNode = backgroundDocument.getElementById("seaquence");
	svgImage.onload = function(){
	    canvas.getContext('2d').drawImage(svgImage, 0, 0);
	    // https://stackoverflow.com/a/11112370/3389895
	    var dataURL = canvas.toDataURL("image/png");
	    var downloadLink = backgroundDocument.createElement("a");
	    downloadLink.href = dataURL;
	    downloadLink.download = "oshinview.png";
	    backgroundDocument.body.appendChild(downloadLink);
	    downloadLink.click();
	    backgroundDocument.body.removeChild(downloadLink);
	}
    } else if(fileType == "fasta"){
	var sequenceData = currentHeader +
	    " [OshinView{type:" + seqOutputType + ";seqCols:" + seqCols +
	    ";x:" + viewX + ";y:" + viewY +
	    ";zoom:" + viewZoom + ";rotate:" + viewRotate + "}]" +
	    "\n" + currentSequence.join("").replace(/(.{80})/g, "$1\n");
	var sequenceBlob = new Blob([sequenceData], {type:"text/plain;charset=utf-8"});
	var sequenceUrl = URL.createObjectURL(sequenceBlob);
	var downloadLink = backgroundDocument.createElement("a");
	downloadLink.href = sequenceUrl;
	downloadLink.download = "oshinview.fasta";
	backgroundDocument.body.appendChild(downloadLink);
	downloadLink.click();
	backgroundDocument.body.removeChild(downloadLink);
    }
}

function updatePosition() {
    let seqGroup = backgroundDocument.getElementById("sequence_0");
    let absScale = 2 ** (viewZoom / 8);
    //console.log("Transforming: scale(" + absScale + ") translate(" +
    // viewX + " " + viewY +")");
    seqGroup.setAttribute("transform",
			  "translate(" + (viewX) +
			  " " + (viewY) +") " +
			  "rotate(" + viewRotate + ") " +
			  "scale(" + absScale + ")");
}

function drawSequence(){
    if(currentHeader == "[No Sequence Loaded]"){
	console.log("Nothing to do!");
	return;
    }
    if(resetViewRequested){
	var setupForm = backgroundDocument.getElementById("setupForm");
	seqOutputType = setupForm.outputType.value;
	seqCols = Number(setupForm.firstLineLength.value);
    }
    seqLength = currentSequence.length;
    seqRows = Math.ceil(seqLength / seqCols);
    seqColWidth = 750 / seqCols;
    seqRowHeight = seqColWidth;
    // console.log(currentSequence);
    // console.log("Number of columns: " + seqCols + "; Number of rows: " + seqRows);
    // console.log("Column width: " + seqColWidth + "; Row height: " + seqRowHeight);
    // https://www.motiontricks.com/creating-dynamic-svg-elements-with-javascript/
    let outerGroup = backgroundDocument.getElementById("all_sequences");
    let seqGroup = backgroundDocument.getElementById("sequence_0");
    seqGroup.innerHTML = ""; // get rid of existing visualisations
    let newText = backgroundDocument.getElementById("sequence_header_0");
    if(newText == null){
	newText = backgroundDocument.createElementNS(svgns, "text");
	newText.setAttribute("id", "sequence_header_0");
	newText.setAttribute("x", "400");
	newText.setAttribute("y", 25/2 + 15/2);
	newText.setAttribute("text-anchor", "middle");
	newText.setAttribute("font-size", "15px");
    }
    newText.innerHTML = "";
    var textNode = backgroundDocument.createTextNode(currentHeader);
    newText.appendChild(textNode);
    outerGroup.appendChild(newText);
    newText = backgroundDocument.getElementById("sequence_metadata_0");
    if(newText == null){
	newText = backgroundDocument.createElementNS(svgns, "text");
	newText.setAttribute("id", "sequence_metadata_0");
	newText.setAttribute("x", "775");
	newText.setAttribute("y", 400 - 25/2+3);
	newText.setAttribute("text-anchor", "end");
	newText.setAttribute("font-size", "12px");
    }
    newText.innerHTML = "";
    var textNode = backgroundDocument.
	createTextNode("Sequence length: " + seqLength);
    newText.appendChild(textNode);
    outerGroup.appendChild(newText);
    newText = backgroundDocument.getElementById("sequence_baseinfo_0");
    if(newText == null){
	newText = backgroundDocument.createElementNS(svgns, "text");
	newText.setAttribute("id", "sequence_baseinfo_0");
	newText.setAttribute("x", "25");
	newText.setAttribute("y", 400 - 25/2+3);
	newText.setAttribute("text-anchor", "start");
	newText.setAttribute("font-size", "12px");
    }
    newText.innerHTML = "";
    var textNode = backgroundDocument.createTextNode("Base ?: ");
    newText.appendChild(textNode);
    outerGroup.appendChild(newText);
    let pathDefs = {A: "", C: "", G: "", T: "", X: ""};
    let pathColours = {A:"#006400", C:"#0000FF",
		       G:"#FFD700", T:"#FF6347",
		       X:"#00000000"};
    // console.log("Output type: " + seqOutputType);
    if(seqOutputType == "rectMatrix"){
	for(var i = 0; i < seqLength; i++){
	    var posY = (Math.floor(i / seqCols)) * seqRowHeight;
	    var posX = (i % seqCols) * seqColWidth;
	    pathDefs[currentSequence[i]] +=
		" M"+posX+","+posY+
		" h"+seqColWidth+" v"+(seqRowHeight * 0.75)+" h-"+seqColWidth + "z";
	}
    } else if(seqOutputType == "preserveAng") {
	var maxRadius = 350 / 2;
	var minRadius = 50;
	var angPerPos = (2 * Math.PI) / (seqCols);
	var radPerPos = (maxRadius - minRadius) / (seqRows * seqCols);
	var thetaOuterStart = 0 * angPerPos;
	var thetaOuterEnd = (0 + 1) * angPerPos;
	var thetaInnerStart = (0 + seqCols) * angPerPos;
	var thetaInnerEnd = (0 + seqCols + 1) * angPerPos;
	var rOuterStart = maxRadius - 0 * radPerPos;
	var rOuterEnd = maxRadius - (0 + 1) * radPerPos;
	var rInnerStart = maxRadius - (0 + (seqCols * 0.75)) * radPerPos;
	var rInnerEnd = maxRadius - (0 + (seqCols * 0.75) + 1) * radPerPos;
	for(var i = 0; i < seqLength; i++){
	    var pX1 = rOuterStart * Math.cos(-thetaOuterStart);
	    var pY1 = rOuterStart * Math.sin(-thetaOuterStart);
	    var pX2 = rOuterEnd * Math.cos(-thetaOuterEnd);
	    var pY2 = rOuterEnd * Math.sin(-thetaOuterEnd);
	    var pX3 = rInnerEnd * Math.cos(-thetaInnerEnd);
	    var pY3 = rInnerEnd * Math.sin(-thetaInnerEnd);
	    var pX4 = rInnerStart * Math.cos(-thetaInnerStart);
	    var pY4 = rInnerStart * Math.sin(-thetaInnerStart);
	    pathDefs[currentSequence[i]] +=
		" M"+pX1+","+pY1+
		" L"+pX2+","+pY2+
		" L"+pX3+","+pY3+
		" L"+pX4+","+pY4+"z";
	    thetaOuterStart += angPerPos; thetaOuterEnd += angPerPos;
	    thetaInnerStart += angPerPos; thetaInnerEnd += angPerPos;
	    rOuterStart -= radPerPos; rOuterEnd -= radPerPos;
	    rInnerStart -= radPerPos; rInnerEnd -= radPerPos;
	}
    } else if(seqOutputType == "preserveDist") {
	var maxRadius = 350 / 2;
	var minRadius = 50;
	totalDist = Math.PI * (maxRadius ** 2 - minRadius ** 2);
	// Note: This is an approximation, assuming infinite rows;
	//       outer distance will be slightly less than a full circle
	// distPerPos = Math.PI * (2 * maxRadius) / (seqCols);
	distPerPos = totalDist / (seqLength);
	// https://en.wikipedia.org/wiki/Archimedean_spiral#Arc_length_and_curvature
	distNeeded = distPerPos * seqLength;
	radPerRow = (maxRadius - minRadius) / seqRows;
	distTravelled = 0;
	for(var i = 0; i < seqLength; i++){
	    currentRad = Math.sqrt(maxRadius**2 - distTravelled / Math.PI);
	    currentTheta = 2 * Math.PI * seqRows *
		(currentRad - minRadius) / (maxRadius - minRadius);
	    distTravelled += distPerPos;
	    nextRad = Math.sqrt(maxRadius**2 - distTravelled / Math.PI);
	    nextTheta = 2 * Math.PI * seqRows *
		(nextRad - minRadius) / (maxRadius - minRadius);
	    var pX1 = currentRad * Math.cos(currentTheta);
	    var pY1 = currentRad * Math.sin(currentTheta);
	    var pX2 = nextRad * Math.cos(nextTheta);
	    var pY2 = nextRad * Math.sin(nextTheta);
	    var pX3 = (nextRad - radPerRow*0.75) * Math.cos(nextTheta);
	    var pY3 = (nextRad - radPerRow*0.75) * Math.sin(nextTheta);
	    var pX4 = (currentRad - radPerRow*0.75) * Math.cos(currentTheta);
	    var pY4 = (currentRad - radPerRow*0.75) * Math.sin(currentTheta);
	    pathDefs[currentSequence[i]] +=
		" M"+pX1+","+pY1+
		" L"+pX2+","+pY2+
		" L"+pX3+","+pY3+
		" L"+pX4+","+pY4+"z";
	}
    } else {
	return;
    }
    for(var key in pathColours){
	let newPath = backgroundDocument.createElementNS(svgns, "path");
	newPath.setAttribute("id", "path_0_" + key);
	newPath.setAttribute("fill", pathColours[key]);
	newPath.setAttribute("stroke", "none");
	newPath.setAttribute("d", pathDefs[key]);
	seqGroup.appendChild(newPath);
    }
    if(resetViewRequested){
	requestAnimationFrame(resetView);
	resetViewRequested = false;
    } else {
	lastBasePosX = -1;
	lastBasePosY = -1;
	requestAnimationFrame(findBase);
    }
    requestAnimationFrame(updatePosition);
}

function getEmail(){
  var count = 2; /* number of email addresses */
  var b = "";
  var a = Math.floor(nextRandom() * count);
  if (a==0) {b= "oshinview"+"%40gringer%2E"+"org"};
  if (a==1) {b= "hacking"+"%40gringer%2E"+"org"};
  b = "mailto:" + b;
  return b;
}

function processRelativeMove(dX, dY, dZoom, dRotate){
    newViewX = viewX + dX;
    newViewY = viewY + dY;
    newViewZoom = viewZoom + dZoom;
    newViewRotate = viewRotate + dRotate;
    requestAnimationFrame(processAbsoluteMove);
}

function processAbsoluteMove(){
    viewX = newViewX;
    viewY = newViewY;
    viewZoom = newViewZoom;
    // The modulus and addition is needed to deal with excessively negative values
    viewRotate = ((newViewRotate % 360) + 360) % 360;
    requestAnimationFrame(updatePosition);
}

function noteFocus(hasFocus){
    let outerRect = backgroundDocument.getElementById("frame_rect_1");
    outerRect.setAttribute("stroke-width", (hasFocus) ? 4 : 2);
    outerRect.setAttribute("stroke", (hasFocus) ? "darkblue" : "grey");
    appFocused = hasFocus;
    if(!appFocused){
	backgroundDocument.body.style.overflow = "auto";
    } else {
	backgroundDocument.body.style.overflow = "hidden";
	backgroundDocument.body.style.userSelect = "none";
    }
}

function processMouseClick(e){
    https://javascriptsource.com/drag-to-scroll/
    if(!appFocused){
	return true;
    }
    dragStartX = e.offsetX;
    dragStartY = e.offsetY;
    dragViewX = viewX;
    dragViewY = viewY;
    dragViewZoom = viewZoom;
    mouseLeftPressed = ((e.buttons & 1) == 1);
    if(mouseLeftPressed){
	mouseDragging = true;
    } else {
	mouseDragging = false;
	viewX = newViewX;
	viewY = newViewY;
	viewZoom = newViewZoom;
    }
}

function findBase(){
    // find offset into SVG region
    var posX; var posY;
    viewportX = currentMouseX;
    viewportY = currentMouseY;
    absScale = 2 ** (viewZoom / 8);
    // work out where the pointer is in the original image
    unscaledX = (viewportX - viewX) / absScale;
    unscaledY = (viewportY - viewY) / absScale;
    if(seqRows > 0){
	if(seqOutputType == "rectMatrix"){
	    posX = Math.floor(unscaledX / seqColWidth);
	    posY = Math.floor(unscaledY / seqRowHeight);
	} else if(seqOutputType == "preserveAng"){
	    var maxRadius = 350 / 2;
	    var minRadius = 50;
	    var totalDist = Math.PI * (maxRadius - minRadius);
	    theta = -Math.atan2(unscaledY, unscaledX);
	    // viewRotate is in degrees, so need to convert to radians
	    theta += viewRotate * Math.PI / 180;
	    theta = ((theta % (2 * Math.PI)) + 2 * Math.PI) % (2 * Math.PI);
	    thetaFrac = (theta / (2 * Math.PI));
	    radius = Math.sqrt(unscaledX ** 2 + unscaledY ** 2);
	    posY = Math.floor(seqRows *
			      (1 - (radius - minRadius) /
			       (maxRadius - minRadius)) - thetaFrac);
	    posX = Math.floor(seqCols * theta / (2 * Math.PI));
	    //console.log("X: " +unscaledX+ "; Y: " +unscaledY+
	    //	"; radius: " + radius + "; theta: " + theta);
	} else if(seqOutputType == "preserveDist") {
	    var maxRadius = 350 / 2;
	    var minRadius = 50;
	    theta = -Math.atan2(unscaledY, unscaledX);
	    // viewRotate is in degrees, so need to convert to radians
	    theta += viewRotate * Math.PI / 180;
	    theta = ((theta % (2 * Math.PI)) + 2 * Math.PI) % (2 * Math.PI);
	    totalDist = Math.PI * (maxRadius ** 2 - minRadius ** 2);
	    distPerPos = totalDist / (seqLength);
	    if(theta < 0){
		theta += 2 * Math.PI;
	    }
	    thetaFrac = (theta / (2 * Math.PI));
	    radius = Math.sqrt(unscaledX ** 2 + unscaledY ** 2);
	    ring = Math.floor(seqRows *
			      (1 - (radius - minRadius) /
			       (maxRadius - minRadius)) - thetaFrac);
	    posX = Math.floor(seqCols * theta / (2 * Math.PI));
	    // adjust theta to account for ring
	    currentTheta = ring * 2 * Math.PI + theta;
	    currentRad = maxRadius - currentTheta * (maxRadius - minRadius) /
		(2 * Math.PI * seqRows);
	    // work out base location
	    distTravelled = Math.PI * (maxRadius ** 2 - currentRad ** 2);
	    baseLoc = Math.floor(distTravelled / distPerPos);
	    posY = Math.floor(baseLoc / seqCols);
	    posX = baseLoc % seqCols;
	    //console.log("posX: " +posX+ "; posY: " +posY);
	}
	if((posX == lastBasePosX) && (posY == lastBasePosY)){
	    return;
	}
	lastBasePosX = posX;
	lastBasePosY = posY;
	if((posX < 0) || (posX >= seqCols) ||
	   (posY < 0) || (posY >= seqRows)){
	    //console.log("FAIL mouseX: " + currentMouseX + "; " +
	    //            "mouseY: " + currentMouseY);
	    currentBasePosition = -1;
	    selectPath = backgroundDocument.getElementById("path_0_X");
	    if(selectPath == null){
		return;
	    }
	    selectPath.setAttribute("d", "");
	    return;
	}
	//console.log("PASS mouseX: " + currentMouseX + "; " +
	//            "mouseY: " + currentMouseY);
	newBasePosition = posY * seqCols + posX;
	if((newBasePosition >= seqLength) || (newBasePosition < 0)){
	    return;
	}
	currentBasePosition = newBasePosition;
	basePosText = backgroundDocument.getElementById("sequence_baseinfo_0");
	basePosText.firstChild.nodeValue =
	    "Base " + (currentBasePosition + 1) + ": " +
	    currentSequence[currentBasePosition];
	selectPath = backgroundDocument.getElementById("path_0_X");
	if(selectPath == null){
	    return;
	}
	if(seqOutputType == "rectMatrix"){
	    var posY = (Math.floor(currentBasePosition / seqCols)) * seqRowHeight;
	    var posX = (currentBasePosition % seqCols) * seqColWidth;
	    pathDef = "M"+posX+","+posY+
		" h"+seqColWidth+" v"+(seqRowHeight * 0.75)+" h-"+seqColWidth;
	} else if(seqOutputType == "preserveAng"){
	    var angPerPos = (2 * Math.PI) / (seqCols);
	    var radPerPos = (maxRadius - minRadius) / (seqRows * seqCols);
	    var thetaOuterStart = currentBasePosition * angPerPos;
	    var thetaOuterEnd = (currentBasePosition + 1) * angPerPos;
	    var thetaInnerStart = (currentBasePosition + seqCols) * angPerPos;
	    var thetaInnerEnd = (currentBasePosition + seqCols + 1) * angPerPos;
	    var rOuterStart = maxRadius - currentBasePosition * radPerPos;
	    var rOuterEnd = maxRadius - (currentBasePosition + 1) * radPerPos;
	    var rInnerStart = maxRadius - (currentBasePosition + (seqCols * 0.75)) * radPerPos;
	    var rInnerEnd = maxRadius - (currentBasePosition + (seqCols * 0.75) + 1) * radPerPos;
	    var pX1 = rOuterStart * Math.cos(-thetaOuterStart);
	    var pY1 = rOuterStart * Math.sin(-thetaOuterStart);
	    var pX2 = rOuterEnd * Math.cos(-thetaOuterEnd);
	    var pY2 = rOuterEnd * Math.sin(-thetaOuterEnd);
	    var pX3 = rInnerEnd * Math.cos(-thetaInnerEnd);
	    var pY3 = rInnerEnd * Math.sin(-thetaInnerEnd);
	    var pX4 = rInnerStart * Math.cos(-thetaInnerStart);
	    var pY4 = rInnerStart * Math.sin(-thetaInnerStart);
	    pathDef =  "M" + pX1 + "," + pY1 + " L" + pX2 + "," + pY2 +
		      " L" + pX3 + "," + pY3 + " L" + pX4 + "," + pY4 + "z";
	} else if(seqOutputType == "preserveDist"){
	    var maxRadius = 350 / 2;
	    var minRadius = 50;
	    totalDist = Math.PI * (maxRadius ** 2 - minRadius ** 2);
	    distPerPos = totalDist / (seqLength);
	    baseLoc = posY * seqCols + posX;
	    distTravelled = baseLoc * distPerPos;
	    currentRad = Math.sqrt(maxRadius**2 - distTravelled / Math.PI);
	    currentTheta = 2 * Math.PI * seqRows *
		(currentRad - minRadius) / (maxRadius - minRadius);
	    distTravelled += distPerPos;
	    nextRad = Math.sqrt(maxRadius**2 - distTravelled / Math.PI);
	    nextTheta = 2 * Math.PI * seqRows *
		(nextRad - minRadius) / (maxRadius - minRadius);
	    var pX1 = currentRad * Math.cos(currentTheta);
	    var pY1 = currentRad * Math.sin(currentTheta);
	    var pX2 = nextRad * Math.cos(nextTheta);
	    var pY2 = nextRad * Math.sin(nextTheta);
	    var pX3 = (nextRad - radPerRow*0.75) * Math.cos(nextTheta);
	    var pY3 = (nextRad - radPerRow*0.75) * Math.sin(nextTheta);
	    var pX4 = (currentRad - radPerRow*0.75) * Math.cos(currentTheta);
	    var pY4 = (currentRad - radPerRow*0.75) * Math.sin(currentTheta);
	    pathDef =
		" M" + pX1 + "," + pY1 +
		" L" + pX2 + "," + pY2 +
		" L" + pX3 + "," + pY3 +
		" L" + pX4 + "," + pY4 +"z";
	}
	selectPath.setAttribute("fill", "#00000080");
	selectPath.setAttribute("stroke", "none");
	selectPath.setAttribute("d", pathDef);
    }
}

function updatePointer(){
    pointerDot = backgroundDocument.getElementById("pointerdot");
    if(currentMouseX > 0){
	pointerDot.setAttribute("transform",
				"translate(" + currentMouseX +
				" " + currentMouseY + ")");
	requestAnimationFrame(findBase);
    }
}

function processMouseMove(e){
    lastMoveEvent = e;
    if(!appFocused){
	return true;
    }
    currentMouseX = e.offsetX;
    currentMouseY = e.offsetY;
    requestAnimationFrame(updatePointer);
    if(!mouseLeftPressed){
	return true;
    }
    dragEndX = e.offsetX;
    dragEndY = e.offsetY;
    newViewX = dragViewX + (dragEndX - dragStartX);
    newViewY = dragViewY + (dragEndY - dragStartY);
    newViewZoom = dragViewZoom;
    requestAnimationFrame(processAbsoluteMove);
}

function resetView(){
    if(seqOutputType == "rectMatrix"){
	newViewX = 25;
	newViewY = 25;
    } else if((seqOutputType == "preserveAng") || (seqOutputType == "preserveDist")){
	maxRadius = 350 / 2;
	newViewX = 400 + 25;
	newViewY = maxRadius + 25;
    }
    newViewZoom = 0;
    newViewRotate = 0;
    processAbsoluteMove();
}

function changeBase(baseToAdd){
    if((currentBasePosition < 0) ||
       (currentBasePosition > currentSequence.length)){
	return;
    }
    oldBase = currentSequence[currentBasePosition];
    if(oldBase == baseToAdd){
	return;
    }
    editOperations.push("change:" + currentBasePosition + "," + oldBase +
			"," + baseToAdd);
    currentSequence[currentBasePosition] = baseToAdd;
    requestAnimationFrame(drawSequence);
}

function insertBase(){
    if((currentBasePosition < 0) ||
       (currentBasePosition > currentSequence.length)){
	return;
    }
    editOperations.push("insert:" + currentBasePosition);
    currentSequence.splice(currentBasePosition, 0, "-");
    requestAnimationFrame(drawSequence);
}

function deleteBase(){
    if((currentBasePosition < 0) ||
       (currentBasePosition > currentSequence.length)){
	return;
    }
    oldBase = currentSequence[currentBasePosition];
    editOperations.push("delete:" + currentBasePosition + "," + oldBase);
    currentSequence.splice(currentBasePosition, 1);
    requestAnimationFrame(drawSequence);
}

function undoLastChange(){
    if((editOperations.length == 0) ||
       (editOperations[editOperations.length - 1].startsWith("sequence:"))){
	// nothing to do
	return;
    }
    lastChange = editOperations.pop().split(":");
    lastOp = lastChange[0];
    lastMods = lastChange[1].split(",");
    if(lastOp == "change"){
	currentSequence[Number(lastMods[0])] = lastMods[1];
    } else if(lastOp == "delete"){
	currentSequence.splice(Number(lastMods[0]), 0, lastMods[1]);
    } else if(lastOp == "insert"){
	currentSequence.splice(Number(lastMods[0]), 1);
    } else {
	// don't understand operation; put back on the stack
	editOperations.push(lastChange.join(":"));
	return;
    }
    requestAnimationFrame(drawSequence);
}

function processKeyDown(e){
    if(!appFocused){
	return true;
    }
    if(e.code == "ArrowLeft"){ // Left
	processRelativeMove(-1, 0, 0, 0);
    } else if(e.code == "ArrowRight"){ // Right
	processRelativeMove(1, 0, 0, 0);
    } else if(e.code == "ArrowUp"){ // Up
	processRelativeMove(0, -1, 0, 0);
    } else if(e.code == "ArrowDown"){ // Down
	processRelativeMove(0, 1, 0, 0);
    } else if((e.code == "PageUp") || (e.code == "Equal")){ // Up
	processRelativeMove(0, 0, 1, 0);
    } else if((e.code == "PageDown") || (e.code == "Minus")){ // Down
	processRelativeMove(0, 0, -1, 0);
    } else if(e.code == "Period"){ // Rotate Right
	processRelativeMove(0, 0, 0, 1);
    } else if(e.code == "Comma"){ // Rotate Left
	processRelativeMove(0, 0, 0, -1);
    } else if(e.code == "Home"){
	requestAnimationFrame(resetView);
    } else if(e.code == "KeyA"){
	changeBase("A");
    } else if(e.code == "KeyC"){
	changeBase("C");
    } else if(e.code == "KeyG"){
	changeBase("G");
    } else if(e.code == "KeyT"){
	changeBase("T");
    } else if((e.code == "Space") || (e.code == "Insert")){
	insertBase();
    } else if((e.code == "Backspace") || (e.code == "Delete")){
	deleteBase();
    } else if(controlPressed && (e.code == "KeyZ")){
	undoLastChange();
    } else if((e.code == "ShiftLeft") ||
	      (e.code == "ShiftRight")){
	shiftPressed = true;
    } else if((e.code == "ControlLeft") ||
	      (e.code == "ControlRight")){
	controlPressed = true;
    } else if((e.code == "AltLeft") ||
	      (e.code == "AltRight")){
	altPressed = true;
    }
    return false;
}

function processKeyUp(e){
    if(!appFocused){
	return true;
    }
    if((e.code == "ShiftLeft") ||
       (e.code == "ShiftRight")){
	shiftPressed = false;
    } else if((e.code == "ControlLeft") ||
	      (e.code == "ControlRight")){
	controlPressed = false;
    } else if((e.code == "AltLeft") ||
	      (e.code == "AltRight")){
	altPressed = false;
    }
    return false;
}

function processWheel(e){
    if(!appFocused){
	return true;
    }
    // https://www.w3schools.com/jsref/event_wheel_deltamode.asp
    // console.log(e);
    // console.log("Mouse X: " + currentMouseX + "; Mouse Y: " + currentMouseY);
    newDelta = 0;
    if(e.deltaMode == 0){ // pixels
	newDelta = (-e.deltaY / (96 * 8));
    } else if(e.deltaMode == 1){
	newDelta = (-e.deltaY);
    } else if(e.deltaMode == 2){
	newDelta = (-e.deltaY * 4);
    }
    if(shiftPressed){
	if ((seqOutputType == "preserveAng") || (seqOutputType == "preserveDist")){
	    processRelativeMove(0, 0, 0, -newDelta);
	}
	updatePointer();
	return;
    }
    // find offset into SVG region
    viewportX = e.offsetX;
    viewportY = e.offsetY;
    absScaleOld = 2 ** (viewZoom / 8);
    absScaleNew = 2 ** ((viewZoom + newDelta) / 8);
    // work out where the pointer is in the original image
    unscaledX = (viewportX - viewX) / absScaleOld;
    unscaledY = (viewportY - viewY) / absScaleOld;
    // work out where the pointer would be shifted if nothing changed
    newScaledX = unscaledX * absScaleNew + viewX;
    newScaledY = unscaledY * absScaleNew + viewY;
    // work out new offset
    dX = newScaledX - viewportX;
    dY = newScaledY - viewportY;
    processRelativeMove(-dX, -dY, newDelta, 0);
}
