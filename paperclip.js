let paperclipCount = 0;
let paperclipPrice = 0.25;
let clipBuyBuffer = 0;
let wirePrice = 10;
let wireQuantity = 33;
let paperclipLength = 0.033;
let publicDemand = 10;
let availableMoney = 0;

function updateCounts(){
    if(wireQuantity < 0){
	wireQuantity = 0;
    }
    document.getElementById("paperclipCount").textContent = paperclipCount;
    document.getElementById("availableMoney").textContent = availableMoney.toFixed(2);
    document.getElementById("paperclipPrice").textContent = paperclipPrice.toFixed(2);
    document.getElementById("publicDemand").textContent = publicDemand.toFixed(0);
    document.getElementById("wirePrice").textContent = wirePrice;
    document.getElementById("wireQuantity").textContent = wireQuantity.toFixed(3);
}

// Buy wire to make more paperclips
function buyWire() {
    if(availableMoney >= wirePrice){
	wireQuantity += 33;
	availableMoney -= wirePrice;
	updateCounts();
    }
}

// Make a paperclip
function makePaperclip() {
    if(wireQuantity.toFixed(3) >= paperclipLength){
	paperclipCount += 1;
	wireQuantity -= paperclipLength;
	updateCounts();
    }
}

// Update clip price
function alterPrice(amount){
    oldPrice = paperclipPrice;
    paperclipPrice = Math.max(0.01, paperclipPrice + amount);
    priceRatio = paperclipPrice / oldPrice;
    publicDemand = publicDemand / priceRatio;
    updateCounts();
}

// Update counters every second
setInterval(() => {
    if(paperclipCount > 0){
	clipBuyBuffer += (publicDemand / 100) * Math.random();
    } else {
	clipBuyBuffer = 0;
    }
    paperclipsBought = Math.floor(Math.min(paperclipCount, clipBuyBuffer));
    if(paperclipsBought > 0){
	paperclipCount -= paperclipsBought;
	clipBuyBuffer -= paperclipsBought;
	availableMoney = (availableMoney + (paperclipsBought * paperclipPrice));
    }
    updateCounts();
}, 100);
