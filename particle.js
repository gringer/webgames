// Particle battle game -- particle battle simulation, with SVG and javascript
// author: David Eccles (gringer) <bioinformatics@gringene.org>

var backgroundDocument = null;
var started = false;
var leftEdge = 0;
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second

var finished = false;

// game / screen settings
var svgWidth = 500;
var svgHeight = 500;
var gameWidth = 400;
var gameHeight = 400;

// point / particle model parameters
var numPoints = 100;
var numPointsRed = 100;
var numPointsBlue = 100;
var pointSize = 5;
var pointLocs = new Array();
var pointVels = new Array();
var pointAngs = new Array();

// particle model parameters
var particleTeam = new Array();
var dead = new Array();
var availableSlots = new Array();
var deadParticleCount = 0;
var strength = new Array();
var particleSpeed = new Array();
var maxParticleSpeed = 3;
var seekMulBase = -0.25;
var seekMulSame = seekMulBase;
var seekMulDiff = seekMulBase;

// rotation parameters
var rotatePX = 200;
var rotatePY = 200;
var rotateSpeed = 0.01;

// tracer parameters
var tracerPX = 200;
var tracerPY = 200;
var tracerX = 200;
var tracerY = 200;
var tracerVX = 0;
var tracerVY = 0;
var traceLimit = 3;
var nextPointToRemove = 0;
var pointerVisible = true;
var buttonPressed = false;

var tracedParticle = -1;

// reward parameters
var totalMoney = 0;
var totalDays = 0;
var framesPerDay = 20;
var firstInfectionID = -1;
var firstInfection = true;
var alertedWon = false;

var randSeed = new Date().getTime();
var gameSeed = randSeed;
var lastLog = 0;

var moveId = setInterval(recalculatePoints, (1000/fps));

// the game only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*541*9301+49297) % 233279;
    return randSeed/(233279.0);
}

function stopPresses(){
    buttonPressed = false;
}

function startup(evt){
    backgroundDocument = evt.target.ownerDocument;
    makeGame(Math.floor(Math.random() * 10000));
}

function createParticle(i){
    if(backgroundDocument == null){
	return;
    }
    var paramForm = backgroundDocument.getElementById("paramform");
    var newStrength = 1;
    var newSpeed = 1;
    dead[i] = false;
    strength[i] = (particleTeam[i] == 0) ? newStrength : 1;
    particleSpeed[i] = (particleTeam[i] == 0) ? newSpeed : 1;
    moveAng = nextRandom() * Math.PI * 2;
    pointAngs[i] = moveAng;
    pointVels[i] = [Math.cos(moveAng) * particleSpeed[i],
		    Math.sin(moveAng) * particleSpeed[i]];
}

function makeGame(seed){
    if(backgroundDocument == null){
	return;
    }
    randSeed = seed;
    gameSeed = seed;
    setForm();
    var seedtext = backgroundDocument.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var game = backgroundDocument.getElementById("gamesetup");
    game.setAttribute("width", svgWidth + "px");
    game.setAttribute("height", svgHeight + "px");
    randSeed = seed;
    gameSeed = seed;
    // reset game variables
    dead = new Array();
    availableSlots = new Array();
    deadParticleCount = 0;
    strength = new Array();
    particleSpeed = new Array();
    blueIDs = new Array();
    redIDs = new Array();
    // reset tracer variables
    tracerPX = 200;
    tracerPY = 200;
    tracerX = 200;
    tracerY = 200;
    tracerVX = 0;
    tracerVY = 0;
    totalDays = 0;
    totalMoney = 0;
    nextPointToRemove = 0;
    pointerVisible = true;
    firstInfection = true;
    // update global game parameters
    var paramForm = backgroundDocument.getElementById("paramform");
    seekMulSame = seekMulBase * paramForm.avoidanceSameInput.value;
    seekMulDiff = seekMulBase * paramForm.avoidanceDiffInput.value;
    numPointsRed = paramForm.redParticleCountInput.value;
    numPointsBlue = paramForm.blueParticleCountInput.value;
    if(numPointsRed > 300){
	paramForm.redParticleCountInput.value = 300;
	numPointsRed = 300;
    }
    if(numPointsBlue > 500){
	paramForm.blueParticleCountInput.value = 500;
	numPointsBlue = 500;
    }
    if(numPointsRed < 1){
	paramForm.redParticleCountInput.value = 1;
	numPointsRed = 1;
    }
    if(numPointsBlue < 10){
	paramForm.blueParticleCountInput.value = 10;
	numPointsBlue = 10;
    }
    numPoints = Math.floor(numPointsRed) + Math.floor(numPointsBlue);
    if(numPoints > 800){
	numPoints = 800;
    }
    // reset win state
    alertedWon = false;
    // set up particles
    var blueIDs = new Array();
    var redIDs = new Array();
    var blueCount = 0;
    var redCount = 0;
    for(var i=0; i < 13; i++){
	nextRandom(); // feed the random generator a few times
    }
    for(var i=0; i < numPoints; i++){
	pointLocs[i] = [nextRandom() * (gameWidth - pointSize) + pointSize/2,
			nextRandom() * (gameHeight - pointSize) + pointSize/2];
	particleTeam[i] = (i < numPointsBlue) ? 0 : 1; //pointLocs[i][1] > pointLocs[0][1] ? 0 : 1;
	createParticle(i);
	if(particleTeam[i] == 0){
	    blueCount++;
	    blueIDs.push(i);
	} else {
	    redCount++;
	    redIDs.push(i);
	}
    }
    if(blueCount == 0){
	particleTeam[0] = 0;
    }
    if(redCount == 0){
	particleTeam[0] = 1;
    }
    // draw points
    drawPoints();
    clearInterval(moveId);
    moveId = setInterval(recalculatePoints, (1000/fps));
}

function recalculatePoints(){
    if(backgroundDocument == null){
	return;
    }
    var newStrength = 1;
    var boxesHoriz = (gameWidth / (pointSize * 2)) + 2;
    var boxesVert = (gameHeight / (pointSize * 2)) + 2;
    var foundPoints = [];
    var collided = [];
    var addedCollision = [];
    var pointLoc = [];
    var blueIDs = new Array();
    var redIDs = new Array();
    var paramForm = backgroundDocument.getElementById("paramform");
    var fixedRed = paramForm.fixRedInput.checked;
    var flowParticles = paramForm.flowParticlesInput.checked;
    var rotateRed = paramForm.rotateRedInput.checked;
    seekMulSame = seekMulBase * paramForm.avoidanceSameInput.value;
    seekMulDiff = seekMulBase * paramForm.avoidanceDiffInput.value;
    for(var xi = 0; xi < boxesHoriz; xi++){
	foundPoints[xi] = [];
	for(var yi = 0; yi < boxesHoriz; yi++){
	    foundPoints[xi][yi] = [];
	}
    }
    for(var i=0; i < numPoints; i++){
	pointLoc = pointLocs[i];
	if(dead[i]){
	    continue;
	}
	if(Math.sqrt((tracerX - pointLoc[0]) ** 2 +
		     (tracerY - pointLoc[1]) ** 2) <= (pointSize * 1.5)){
	    tracedParticle = i;
	}
	if(particleTeam[i] == 0){
	    blueIDs.push(i);
	} else {
	    redIDs.push(i);
	}
	// Calculate collision bounding boxes
	bbX = Math.floor((pointLoc[0]+pointSize) / (pointSize * 3)); bbX1 = bbX+1;
	bbY = Math.floor((pointLoc[1]+pointSize) / (pointSize * 3)); bbY1 = bbY+1;
	var bbHashes = [[bbX, bbY],  [bbX1, bbY],
			[bbX, bbY1], [bbX1, bbY1]];
	for(var bbi = 0; bbi < 4; bbi++){
	    bbh = bbHashes[bbi];
	    fp = foundPoints[bbh[0]][bbh[1]];
	    if(fixedRed && (particleTeam[i] == particleTeam[fp[fi]]) && (particleTeam[i] == 1)){
		// don't collide non-moving red particles
		continue;
	    }
	    if(fp.length == 0){ // first particle in this hash bucket; nothing to collide with
		foundPoints[bbh[0]][bbh[1]][fp.length] = i;
	    } else { // other particles; calculate collisions
		var addedCollision = false;
		for(var fi = 0; fi < fp.length; fi++){
		    var cp = pointLocs[fp[fi]]; // potentiallyCollidedPoint
		    // sqrt((x0-x1)^2 + (y0-y1)^2)
		    var dx = (cp[0] - pointLoc[0]);
		    var dy = (cp[1] - pointLoc[1]);
		    var dist = Math.sqrt(dx ** 2 + dy ** 2);
		    var ang = Math.atan2(dy, dx);
		    var massMul0 = 1;
		    var massMul1 = 1;
		    var sameTeam = (particleTeam[i] == particleTeam[fp[fi]]);
		    var seekMul = (sameTeam) ? seekMulSame : seekMulDiff;
		    if(fixedRed){
			if(particleTeam[fp[fi]] == 1){
			    massMul1 = 2;
			}
			if(particleTeam[i] == 1){
			    massMul0 = 2;
			}
		    }
		    // Note: this is slightly inaccurate, as it's an
		    // instantaneous collision calculation. A more accurate
		    // calculation would require comparing trajectories to determine
		    // the exact time of collision (if any)
		    if(dist >= pointSize){
			// not colliding, but too near, so avoid it
			adj0 = [seekMul * massMul1 * (100 / numPoints) * (1/(dist**2)) * Math.cos(ang),
				seekMul * massMul1 * (100 / numPoints) * (1/(dist**2)) * Math.sin(ang)];
			adj1 = [seekMul * massMul0 * (100 / numPoints) * (1/(dist**2)) * Math.cos(ang + Math.PI),
				seekMul * massMul0 * (100 / numPoints) * (1/(dist**2)) * Math.sin(ang + Math.PI)];
			pointVels[i][0] += adj0[0] * 0.95 + adj1[0] * 0.05;
			pointVels[i][1] += adj0[1] * 0.95 + adj1[1] * 0.05;
			pointVels[fp[fi]][0] += adj0[0] * 0.05 + adj1[0] * 0.95;
			pointVels[fp[fi]][1] += adj0[1] * 0.05 + adj1[1] * 0.95;
		    } else { // collided
			if(!addedCollision){
			    collided[collided.length] = i;
			    addedCollision = true;
			}
			collided[collided.length] = fp[fi];
			// Another simplification - assuming the deflection point
			// is right in the middle of the two points
			// calculate collision normal vector
			colMid = [(cp[0] + pointLoc[0])/2,
				  (cp[1] + pointLoc[1])/2];
			// shift points away from collision in an attempt to reduce
			// dancing points
			if(!fixedRed || (particleTeam[fp[fi]] == 0)){
			    cp[0] = cp[0] -
				(colMid[0] - cp[0]) * (pointSize - dist) / 2;
			    cp[1] = cp[1] -
				(colMid[1] - cp[1]) * (pointSize - dist) / 2;
			}
			if(!fixedRed || (particleTeam[i] == 0)){
			    pointLoc[0] = pointLoc[0] +
				(pointLoc[0] - colMid[0]) * (pointSize - dist) / 2;
			    pointLoc[1] = pointLoc[1] +
				(pointLoc[1] - colMid[1]) * (pointSize - dist) / 2;
			}
			vi1 = pointVels[i]; vi2 = pointVels[fp[fi]];
			if(fixedRed && (!sameTeam)){ // elastic collision, but red won't budge
			    if(particleTeam[i] == 1){
				movingVel = pointVels[fp[fi]];
				collideAng = ang;
			    } else {
				movingVel = pointVels[i];
				collideAng = ang + Math.PI;
			    }
			    // angle of incidence == angle of reflection
			    movingVelAng = Math.atan2(movingVel[1], movingVel[0]);
			    movingVelMag = Math.sqrt(movingVel[1] ** 2 + movingVel[0] ** 2);
			    reflectAng = collideAng - (movingVelAng - collideAng);
			    if(particleTeam[i] == 1){
				pointVels[fp[fi]][0] = -Math.cos(reflectAng) * movingVelMag;
				pointVels[fp[fi]][1] = -Math.sin(reflectAng) * movingVelMag;
			    } else {
				pointVels[i][0] = -Math.cos(reflectAng) * movingVelMag;
				pointVels[i][1] = -Math.sin(reflectAng) * movingVelMag;
			    }
			} else {
			    // elastic collision of equal masses - just swap velocities!
			    //pointVels[i] = vi2;
			    //pointVels[fp[fi]] = vi1;
			    // try it with a slight inelastic collision
			    pointVels[i][0] = vi1[0] * 0.05 + vi2[0] * 0.95;
			    pointVels[i][1] = vi1[1] * 0.05 + vi2[1] * 0.95;
			    pointVels[fp[fi]][0] = vi1[0] * 0.95 + vi2[0] * 0.05;
			    pointVels[fp[fi]][1] = vi1[1] * 0.95 + vi2[1] * 0.05;
			}
		    }
		}
		foundPoints[bbh[0]][bbh[1]][fp.length] = i;
	    }
	}
    }
    var particleCount = 0;
    var particleCentre = [0,0];
    // work out red inertia
    for(var i=0; i < numPoints; i++){
	if(dead[i]){
	    continue;
	}
	pointLoc = pointLocs[i];
	particleCount++;
	particleCentre[0] += pointLoc[0];
	particleCentre[1] += pointLoc[1];
    }
    particleCentre[0] = particleCentre[0] / particleCount;
    particleCentre[1] = particleCentre[1] / particleCount;
    var qc = 0;
    var hc = 0;
    for(var i=0; i < numPoints; i++){
	if(dead[i]){
	    continue;
	}
	pointLoc = pointLocs[i];
	// Head towards the centre point of all particles
	if(false){ // [Note: disabled for now]
	    var ps = particleCentre;
	    deltaX = ps[0] - pointLoc[0];
	    deltaY = ps[1] - pointLoc[1];
	    dist = Math.sqrt(deltaX ** 2 + deltaY ** 2);
	    oldVel = pointVels[i];
	    newVel = [deltaX / dist,
		      deltaY / dist];
	    newVel[0] = oldVel[0] + newVel[0] * (seekMulSame + seekMulDiff) / 2;
	    newVel[1] = oldVel[1] + newVel[1] * (seekMulSame + seekMulDiff) / 2;
	    newVelDist = Math.sqrt(newVel[0] ** 2 + newVel[1] ** 2);
	    if(newVelDist > maxParticleSpeed){
		newVel[0] = newVel[0] * maxParticleSpeed / newVelDist;
		newVel[1] = newVel[1] * maxParticleSpeed / newVelDist;
	    }
	    pointVels[i] = newVel;
	}
	if(particleTeam[i] == 1){
	    if(fixedRed){
		pointVels[i][0] = pointVels[i][1] = 0;
	    } if(rotateRed){
		dx = (rotatePX - pointLoc[0]);
		dy = (rotatePY - pointLoc[1]);
		pointDist = Math.sqrt(dy ** 2 + dx ** 2);
		pointAngle = Math.atan2(dy, dx);
		pointVels[i][0] = pointDist * rotateSpeed * Math.cos(pointAngle + Math.PI/2);
		pointVels[i][1] = pointDist * rotateSpeed * Math.sin(pointAngle + Math.PI/2);
	    }
	}
    }
    processFrame();
}

function drawPoints(){
    if(backgroundDocument == null){
	return;
    }
    var paramForm = backgroundDocument.getElementById("paramform");
    var fixedRed = paramForm.fixRedInput.checked;
    var flowParticles = paramForm.flowParticlesInput.checked;
    var rotateRed = paramForm.rotateRedInput.checked;
    var redPath = backgroundDocument.getElementById("teamRed");
    var bluePath = backgroundDocument.getElementById("teamBlue");
    var tracedParticleText = "";
    var redText = "";
    var blueText = "";
    for(var i=0; i < numPoints; i++){
	pointLoc = pointLocs[i];
	if(particleTeam[i] == 0){

	    blueText += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	} else {
	    redText += "M " + pointLoc[0] + "," + pointLoc[1] + " v 0 ";
	}
    }
    redPath.setAttribute("d", redText);
    bluePath.setAttribute("d", blueText);
    var rotatePoint = backgroundDocument.getElementById("centrepoint");
    rotatePoint.style.opacity = (rotateRed) ? 1 : 0.25;
    var infecttext = backgroundDocument.getElementById("infectlabel");
    infecttext.firstChild.nodeValue = "Particles: " + numPoints;
    var newScoreText = "";
    var scoretext = backgroundDocument.getElementById("scorelabel");
    if(tracedParticle >= 0){
	newScoreText += "Particle #" + tracedParticle;
	if(dead[tracedParticle]){
	    newScoreText += ": dead";
	} else {
	    newScoreText += ": " + strength[tracedParticle];
	}
	newScoreText += "";
    }
    scoretext.firstChild.nodeValue = newScoreText;
}

function setForm() {
    if(backgroundDocument == null){
	return;
    }
    var inputForm = backgroundDocument.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = gameSeed;
        var speedValue = -1;
        for(var i = 0; i < inputForm.gamespeed.length; i++){
            if(inputForm.gamespeed[i].checked){
                speedValue = i;
            }
        }
        if(speedValue == -1){
            inputForm.gamespeed[2].checked = true;
            speedValue = 2;
        }
        if(speedValue == 0){
            frameDelay = 45;
        }
        if(speedValue == 1){
            frameDelay = 20;
        }
        if(speedValue == 2){
            frameDelay = 12;
        }
        if(speedValue == 3){
            frameDelay = 8;
        }
	console.log("Setting frame delay to " + frameDelay);
    }
}

function processFrame(){
    if(backgroundDocument == null){
	return;
    }
    totalDays += (8/frameDelay) / framesPerDay;
    var minX = -pointSize/2;
    var minY = -pointSize/2;
    var maxX = gameWidth + pointSize/2;
    var maxY = gameWidth + pointSize/2;
    var paramForm = backgroundDocument.getElementById("paramform");
    var fixedRed = paramForm.fixRedInput.checked;
    var flowParticles = paramForm.flowParticlesInput.checked;
    var flowEnergy = paramForm.flowEnergyInput.value;
    for(var i=0; i < numPoints; i++){
	// update point location
    	pointLoc = [pointLocs[i][0] + pointVels[i][0] * 8/frameDelay,
    		    pointLocs[i][1] + pointVels[i][1] * 8/frameDelay];
	// wall bounce collision check
	if(pointLoc[0] < minX){
	    if(flowParticles){ // wrap around X
		pointLoc[0] = maxX - (minX - pointLoc[0]);
		pointLoc[1] = nextRandom() * (maxY - minY) + minY;
	    } else {
		pointLoc[0] = minX + (minX - pointLoc[0]);
		pointVels[i][0] = -pointVels[i][0];
	    }
	}
	if(pointLoc[1] < minY){
	    if(flowParticles){ // wrap around Y
		pointLoc[0] = nextRandom() * (maxX - minX) + minX;
		pointLoc[1] = maxY - (minY - pointLoc[1]);
	    } else {
		pointLoc[1] = minY + (minY - pointLoc[1]);
		pointVels[i][1] = -pointVels[i][1];
	    }
	}
	if(pointLoc[0] > maxX){
	    if(flowParticles){
		pointLoc[0] = minX + (pointLoc[0] - maxX);
		pointLoc[1] = nextRandom() * (maxY - minY) + minY;
		pointVels[i][0] += flowEnergy * 0.1;
		newVelDist = Math.sqrt(pointVels[i][0] ** 2 + pointVels[i][1] ** 2);
		if(newVelDist > (maxParticleSpeed * Math.sqrt(flowEnergy))){
		    pointVels[i][0] = pointVels[i][0] * Math.sqrt(flowEnergy) * maxParticleSpeed / newVelDist;
		    pointVels[i][1] = pointVels[i][1] * Math.sqrt(flowEnergy) * maxParticleSpeed / newVelDist;
		    if(pointVels[i][0] < 0) { pointVels[i][0] = -pointVels[i][0]; }
		    if(Math.abs(pointVels[i][1]) < 0.1){
			pointVels[i][1] = nextRandom() * 0.2 - 0.1;
		    }
		}
	    } else {
		pointLoc[0] = maxX - (pointLoc[0] - maxX);
		pointVels[i][0] = -pointVels[i][0];
	    }
	}
	if(pointLoc[1] > maxY){
	    if(flowParticles){ // wrap around Y
		pointLoc[0] = nextRandom() * (maxX - minX) + minX;
		pointLoc[1] = minY + (pointLoc[1] - maxY);
	    } else {
		pointLoc[1] = maxY - (pointLoc[1] - maxY);
		pointVels[i][1] = -pointVels[i][1];
	    }
	}
	pointLocs[i] = pointLoc;
    }
    // update tracer location
    tracerX = tracerX + tracerVX * 8/frameDelay * 3;
    tracerY = tracerY + tracerVY * 8/frameDelay * 3;
    // stop if it's close to the mouse location
    if(Math.sqrt((tracerPX - tracerX) ** 2 + (tracerPY - tracerY) ** 2) <= 2){
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
    }
    // stop if it's close to the edge
    if((tracerX <= minX) || (tracerX >= maxX) ||
       (tracerY <= minY) || (tracerY >= maxY)){
	tracerVX = 0;
	tracerVY = 0;
    }
    window.requestAnimationFrame(drawPoints);
    window.requestAnimationFrame(updatePointer);
}

function makeRedDot(){
    buttonPressed = true;
    firstRed = -1;
    chosenPos = -1;
    for(var i=0; i < numPoints; i++){
	if(particleTeam[i] == 1){
	    if(firstRed == -1){
		firstRed = i;
	    }
	    if((chosenPos == -1) && (i >= nextPointToRemove)){
		chosenPos = i;
	    }
	}
    }
    if(chosenPos == -1){
	chosenPos = firstRed;
    }
    pointLocs[chosenPos] = [tracerPX, tracerPY];
    lastPlacedDot = [tracerPX, tracerPY];
    nextPointToRemove = (chosenPos + 1) % numPoints;
}

function updatePointer(){
    if(backgroundDocument == null){
	return;
    }
    pointerDot = backgroundDocument.getElementById("pointerdot");
    if(pointerVisible){
	pointerDot.setAttribute("cx", tracerX);
	pointerDot.setAttribute("cy", tracerY);
    } else {
	pointerDot.setAttribute("cx", -100);
	pointerDot.setAttribute("cy", -100);
    }
}

function movePointer(e){
    pointerVisible = true;
    tracerPX = e.offsetX;
    tracerPY = e.offsetY;
    if(Math.sqrt((tracerPX - tracerX) ** 2 + (tracerPY - tracerY) ** 2) <= 2){
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
    } else {
	// move right there
	tracerVAng = Math.atan2(tracerPY - tracerY, tracerPX - tracerX);
	tracerX = tracerPX;
	tracerY = tracerPY;
	tracerVX = 0;
	tracerVY = 0;
	//tracerVX = Math.cos(tracerVAng);
	//tracerVY = Math.sin(tracerVAng);
    }
    if(buttonPressed){
	tracerDist = Math.sqrt((lastPlacedDot[0] - tracerX) ** 2 + (lastPlacedDot[1] - tracerY) ** 2);
	if(tracerDist > 2){
	    makeRedDot();
	}
    }
}

function startAnimation(){
    recalculatePoints();
    moveId = setInterval(recalculatePoints, (1000/fps));
}

function clearPointer(e){
    //pointerVisible = false;
    window.requestAnimationFrame(updatePointer);
}
