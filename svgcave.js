// SVGCave game -- like SFcave, but with SVG and javascript
// author: David Eccles (gringer) <programming@gringer.org>

// Not optimised much -- please take care on hot computers

var backgroundDocument;
var started = false;
var leftEdge = 0;
var shipX = 0;
var shipY = 200;
var motionRotation = [0];
var oldPX = 200;
var oldPY = 200;
var shipPX = 200;
var shipPY = 200;
var shipVX = 1;
var shipVY = 0;
var bulX = -100;
var bulY = -100;
var bulVX = -1;
var bulVY = 0;
var usingMouse = false;
var timesActivated = 0;
var shipAccelFactor = 0.08;
var shipAccel = shipAccelFactor;
var oldTime = new Date().getTime();
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second
var leadSize = 50; // traversed path to show at beginning of screen

var initialBeta = 0;

var finished = false;
var explodeCount = 0;

var cW = 5000; // Cave Width
var cH = 400; // Cave Height
var sH = 6; // ship Height
var bH = 20; // block height min
var bM = 120; // block height max
var bW = 10; // block width

var svgWidth = 400;
var svgHeight = 500;

var numBlocks = 0;
var numPoints = 0;
var caveXPoints = new Array();
var caveYTop = new Array();
var caveYBot = new Array();
var blockXPos = new Array();
var blockYPos = new Array();
var blockHeight = new Array();
var blockText = new Array();
var nextPoint = 1; // next change point that the ship will arrive at
var nextBlock = 0;

var edgeCurvePos = 0;
var shipCurvePos = 0;
var shipCurveXPoints = new Array();
var shipCurveYPoints = new Array();
var shipCurveVel = new Array();
var shipCurveText = new Array();
var pathText = "";

var randSeed = new Date().getTime();
var mazeSeed = randSeed;
var processNextFrame = false;

//var moveId = setInterval(processFrame, (1000/fps));

document.onkeydown = processKeyDown;
document.onkeyup = processKeyUp;

// the maze only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*9301+49297) % 233280;
    return randSeed/(233280.0);
}

function startup(evt) {
    backgroundDocument = evt.target.ownerDocument;
    // motion tracking from https://sensor-js.xyz/demo.html
    if (DeviceMotionEvent &&
	typeof(DeviceMotionEvent.requestPermission) === "function") {
	DeviceMotionEvent.requestPermission();
    }
    makeMaze(Math.floor(Math.random() * 10000));
    startAnimation();
}

function setForm() {
    var inputForm = backgroundDocument.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = mazeSeed;
        var speedValue = -1;
        for(var i = 0; i < inputForm.mazespeed.length; i++){
            if(inputForm.mazespeed[i].checked){
                speedValue = i;
            }
        }
        if(speedValue == -1){
            inputForm.mazespeed[2].checked = true;
            speedValue = 2;
        }
        if(speedValue == 0){
            frameDelay = 45;
        }
        if(speedValue == 1){
            frameDelay = 20;
        }
        if(speedValue == 2){
            frameDelay = 12;
        }
        if(speedValue == 3){
            frameDelay = 8;
        }
    }
}

function redrawBlocks(){
    var blockpath = backgroundDocument.getElementById("blockpath");
    for(var i = 0; i < numBlocks; i++){
        bXP = blockXPos[i];
        bYP = blockYPos[i];
        blockSize = blockHeight[i];
        blockText[i] =
            "M" + bXP + " " + bYP + " " +
            "L" + (bXP+bW) + " " + bYP + " " +
            "L" + (bXP+bW) + " " + (bYP+blockSize) + " " +
            "L" + bXP + " " + (bYP+blockSize) + "Z";
    }
    blockpath.setAttribute("d", blockText.join(" "));
}

function resetBlocks(){
    for(var i = 0; i < numBlocks; i++){
        if(blockYPos[i] < 0) {
	    blockYPos[i] = -blockYPos[i];
	}
    }
    redrawBlocks();
}

function makeMaze(seed){
    randSeed = seed;
    mazeSeed = seed;
    setForm();
    var seedtext = backgroundDocument.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var cave = backgroundDocument.getElementById("cavesetup");
    cave.setAttribute("width", svgWidth);
    cave.setAttribute("height", svgHeight);
    randSeed = seed;
    mazeSeed = seed;
    var cavepath = backgroundDocument.getElementById("cavepath");
    var blockpath = backgroundDocument.getElementById("blockpath");
    // set up obstacles
    caveXPoints[0] = 0;
    caveYTop[0] = 0;
    caveYBot[0] = cH;
    numPoints = 1;
    var xPos = 0;
    while(xPos < cW){
        var dist = (nextRandom() * (cH / 4)) + cH / 4;
        var caveDiff = cH - ((cH - bH - sH) *
                             (Math.sqrt(xPos + dist) / Math.sqrt(cW)) );
        if((xPos + dist) < cW) {
            caveXPoints[numPoints] = xPos + dist;
        } else {
            caveXPoints[numPoints] = cW;
            caveDiff = (bH+sH);
        }
        caveYTop[numPoints] = nextRandom() * cH;
        caveYBot[numPoints] = caveYTop[numPoints] + caveDiff;
        if(caveYBot[numPoints] > cH){
            caveYTop[numPoints] = cH - caveDiff;
            caveYBot[numPoints] = caveYTop[numPoints] + caveDiff;
        }
        numPoints = numPoints + 1;
        xPos = xPos + dist;
    }
    xPos = 100; // prevent early crashes
    numBlocks = 0;
    var nextChange = 1;
    while(xPos < cW){
        var dist = nextRandom() * (cH / 4) + (cH / 4);
        var blockSize = bH + ((bM - bH) * (1 - Math.sqrt(xPos + dist) / Math.sqrt(cW)));
        var bXP = Math.round((xPos + dist) * 10) / 10;
        while(bXP > (caveXPoints[nextChange] + bW)){
            nextChange = nextChange + 1;
        }
        var blockXDiff = bXP - caveXPoints[nextChange - 1];
        var caveXDiff = caveXPoints[nextChange] - caveXPoints[nextChange - 1];
        var caveYDTop = caveYTop[nextChange] - caveYTop[nextChange - 1];
        var caveYDBot = caveYBot[nextChange] - caveYBot[nextChange - 1];
        var caveTop = (blockXDiff / caveXDiff) * caveYDTop +
            caveYTop[nextChange - 1];
        var caveBot = (blockXDiff / caveXDiff) * caveYDBot +
            caveYBot[nextChange - 1];
        var bYP = Math.round((nextRandom() * (caveTop - caveBot) + caveBot - (blockSize / 2)) * 10) / 10;
        if(bYP < 0){
            bYP = 0;
        }
        if(bYP > cH){
            bYP = cH;
        }
        if((bXP) < cW) {
            blockXPos[numBlocks] = bXP;
            blockYPos[numBlocks] = bYP;
            if(bYP + blockSize > cH){
                blockSize = cH - bYP;
            }
            blockHeight[numBlocks] = blockSize;
            blockText[numBlocks] =
                "M" + bXP + " " + bYP + " " +
                "L" + (bXP+bW) + " " + bYP + " " +
                "L" + (bXP+bW) + " " + (bYP+blockSize) + " " +
                "L" + bXP + " " + (bYP+blockSize) + "Z";
            numBlocks = numBlocks + 1;
        }
        xPos = xPos + dist;
    }
    blockpath.setAttribute("d", blockText.join(" "));
    // phantom block past the edge of the screen [not drawn]
    blockXPos[numBlocks] = cW + bW * 2;
    blockYPos[numBlocks] = cH / 2;
    blockHeight[numBlocks] = bH;
    fwdPath = new Array();
    revPath = new Array();
    for(var i=0; i < numPoints; i = i + 1){
        fwdPath[i] =
            "L" + caveXPoints[i] +
            " " + caveYTop[i];
        revPath[i] =
            "L" + caveXPoints[(numPoints - 1) - i] +
            " " + caveYBot[(numPoints - 1) - i];
    }
    pathDesc = "M0 0 " + fwdPath.join(" ") +
        " " + revPath.join(" ") + "L0 " + cH + " Z";
    cavepath.setAttribute("d", pathDesc);
    started = true;
    reset();
}

function updateBullet(){
    bullet = backgroundDocument.getElementById("bulletpos");
    if(bulX > 0){
        bullet.setAttribute("cx", bulX);
        bullet.setAttribute("cy", bulY);
    } else {
        bullet.setAttribute("cx", -100);
        bullet.setAttribute("cy", -100);
    }
}

function clearBullet(){
    bulX = -100;
    bulY = -100;
    bulVX = -1;
    bulVY = 0;
    updateBullet();
}

function handleOrientation(event) {
    if(motionRotation.length < 3){
	motionRotation = [0,0,0];
    }
    if(event.alpha != null){
	motionRotation[0] = event.alpha.toFixed(2);
    }
    if(event.beta != null){
	motionRotation[1] = event.beta.toFixed(2);
    }
    if(event.gamma != null){
	motionRotation[2] = event.gamma.toFixed(2);
    }
}

function startAnimation(){
    processNextFrame = true;
    requestAnimationFrame(processFrame);
    window.addEventListener("deviceorientation", handleOrientation);
}

function stopAnimation(){
    processNextFrame = false;
    window.removeEventListener("deviceorientation", handleOrientation);
}

function reset(){
    if(started){
	usingMouse = false;
        leftEdge = 0;
        shipX = 0;
        shipY = 200;
        shipVX = 1;
        shipVY = 0;
	oldPX = 200;
	oldPY = 200;
	shipPX = 200;
	shipPY = 200;
	clearBullet();
	timesActivated = 0;
        shipAccel = shipAccelFactor;
        nextBlock = 0;
        nextPoint = 1;
        cave = backgroundDocument.getElementById("cavesetup");
        oldVB = cave.setAttribute("viewBox","0 0 400 500");
        ship = backgroundDocument.getElementById("shippath");
        ship.setAttribute("d", d="M0 200");
        stopAnimation();
	startAnimation();
        shipCurveXPoints = new Array();
        shipCurveYPoints = new Array();
        shipCurveVel = new Array();
        shipCurveText = new Array();
        shipCurveXPoints[0] = 0;
        shipCurveYPoints[0] = cH / 2;
        shipCurveVel[0] = 0;
        shipCurveText[0] = "M 0 " + (cH / 2);
        shipCurvePos = 1;
        edgeCurvePos = 0;
        pathText = "M 0 " + cH / 2;
        var ep = backgroundDocument.getElementById("explodepath");
        ep.setAttribute("d", "");
        explodeCount = 0;
        oldTime = new Date().getTime();
	resetBlocks();
    }
    finished = false;
}

function updatePointer(){
    pointerDot = backgroundDocument.getElementById("pointerdot");
    if(shipPX > 0){
	pointerDot.setAttribute("cx", shipPX);
	pointerDot.setAttribute("cy", shipPY);
    } else {
	pointerDot.setAttribute("cx", -100);
	pointerDot.setAttribute("cy", -100);
    }
    if(motionRotation.length >= 3){
	angX = motionRotation[2] / 30;
	angY = motionRotation[1] / 30;
	pointerX = (angX * 250 + 250) % 500;
	pointerY = (angY * 250 + 250) % 500;
	orientationNode = backgroundDocument.getElementById("orientationMarker");
	orientationNode.setAttribute("visibility", "visible");
	orientationNode.setAttribute("transform", "translate(" + pointerX + leftEdge + "," + pointerY + ")");
	usingMouse = true;
	shipPX = pointerX + leftEdge;
	shipPY = pointerY;
    }
}

function movePointer(e){
    usingMouse = true;
    shipPX = e.offsetX + leftEdge;
    shipPY = e.offsetY;
    updatePointer();
}

function clearPointer(shiftPoss){
    if(shiftPoss){
	shipPX = -100;
	shipPY = shipY;
    }
    updatePointer();
}

function accelerateShip(e){
    if(finished){
        if((explodeCount > 30) || (shipX > 4998)){
            reset();
        }
    } else {
        shipAccel = 0 - shipAccelFactor;
        //alterPath(shipX, shipY, shipVY);
    }
    usingMouse = false;
    clearPointer(true);
}

function decelerateShip(e){
    if(finished){
        if((explodeCount > 30) || (shipX > 4998)){
            reset();
        }
    } else {
        shipAccel = shipAccelFactor;
        //alterPath(shipX, shipY, shipVY);
    }
    usingMouse = false;
    clearPointer(true);
}

function activateWeapon(){
    if(finished){
        if((explodeCount > 30) || (shipX > 4998)){
	    explodeCount = 70;
            reset();
	    clearPointer(true);
        }
    } else {
	if(getScore() >= 1000){
	    timesActivated += 1;
	    bulX = shipX;
	    bulY = shipY;
	    bulVX = shipVX * 4 / (45 / frameDelay);
	    bulVY = shipVY * 4 / (45 / frameDelay);
	}
    }
}

function processKeyDown(e){
    if(e.keyCode == 13){
	activateWeapon();
    } else {
	accelerateShip();
    }
}

function processKeyUp(e){
    if(e.keyCode == 13){
    } else {
	decelerateShip();
    }
}

function alterPath(xE, yE, vE){
    xS = shipCurveXPoints[shipCurvePos - 1];
    yS = shipCurveYPoints[shipCurvePos - 1];
    vS = shipCurveVel[shipCurvePos - 1];
    if(xE - xS > 0){
        shipCurveXPoints[shipCurvePos] = xE;
        shipCurveYPoints[shipCurvePos] = yE;
        shipCurveVel[shipCurvePos] = vE;
        // mid quadratic curve point is line intersection of tangent at start and tangent at end
        // y1 = vS (x - xS) + yS [line 1]
        // y2 = vE (x - xE) + yE [line 2]
        // solve for x: vS * (x - xS) + yS = vE * (x - xE) + yE
        // => vS * x - vS * xS + yS = vE * x - vE * xE + yE
        // => vS * x - vE * x = vS * xS - vE * xE - yS + yE
        // => x * (vS - vE) = vS * xS - vE * xE - (yS - yE)
        // => x = (vS * xS - vE * xE - (yS - yE)) / (vS - vE)
        var xM;
        if(vS - vE == 0){
            xM = (xE - xS) / 2;
        } else {
            xM = (vS * xS - vE * xE - (yS - yE)) / (vS - vE);
        }
        var yM = vS * (xM - xS) + yS;
        shipCurveText[shipCurvePos] = "Q" + xM + " " + yM + " " +
            shipCurveXPoints[shipCurvePos] + " " + shipCurveYPoints[shipCurvePos];
        while((leftEdge) > shipCurveXPoints[edgeCurvePos]){
            edgeCurvePos = edgeCurvePos + 1;
        }
        edgeCurvePos = Math.max(0,edgeCurvePos-1);
        shipCurvePos = shipCurvePos + 1;
        // pathText = "M" +
        //     shipCurveXPoints[edgeCurvePos] + " " +
        //     shipCurveYPoints[edgeCurvePos] + " " +
        //     shipCurveText.slice(edgeCurvePos).join(" ");
    }
}

function collide(posX, posY){
    var nextPoint = 0;
    var nextBlock = 0;
    while(posX > caveXPoints[nextPoint]){
        nextPoint = nextPoint + 1;
    }
    while(posX > (blockXPos[nextBlock] + bW)){
        nextBlock = nextBlock + 1;
    }
    if((posY > cH) || (posY < 0)){
        posY = Math.min(Math.max(posY, 0), cH);
        return(true);
    }
    var posXDiff = posX - caveXPoints[nextPoint - 1];
    var posYDiff = posY - caveYTop[nextPoint - 1];
    var caveXDiff = caveXPoints[nextPoint] - caveXPoints[nextPoint - 1];
    var caveYDiff = caveYTop[nextPoint] - caveYTop[nextPoint - 1];
    if(posY < caveYTop[nextPoint - 1] +
       (posXDiff / caveXDiff) * caveYDiff){
        return(true);
    }
    posYDiff = posY - caveYBot[nextPoint - 1];
    caveYDiff = caveYBot[nextPoint] - caveYBot[nextPoint - 1];
    if(posY > caveYBot[nextPoint - 1] +
       (posXDiff / caveXDiff) * caveYDiff){
        return(true);
    }
    if(!((posX < blockXPos[nextBlock]) ||
         (posX > blockXPos[nextBlock] + bW) ||
         (posY < blockYPos[nextBlock]) ||
         (posY > blockYPos[nextBlock] + blockHeight[nextBlock]))){
        return(true);
    }
    return(false);
}

function removeDestroyedColumn(posX, posY){
    var nextPoint = 0;
    var nextBlock = 0;
    while(posX > caveXPoints[nextPoint]){
        nextPoint = nextPoint + 1;
    }
    while(posX > (blockXPos[nextBlock] + bW)){
        nextBlock = nextBlock + 1;
    }
    if((posY > cH) || (posY < 0)){
        posY = Math.min(Math.max(posY, 0), cH);
        return(false);
    }
    var posXDiff = posX - caveXPoints[nextPoint - 1];
    var posYDiff = posY - caveYTop[nextPoint - 1];
    var caveXDiff = caveXPoints[nextPoint] - caveXPoints[nextPoint - 1];
    var caveYDiff = caveYTop[nextPoint] - caveYTop[nextPoint - 1];
    if(posY < caveYTop[nextPoint - 1] +
       (posXDiff / caveXDiff) * caveYDiff){
        return(false);
    }
    posYDiff = posY - caveYBot[nextPoint - 1];
    caveYDiff = caveYBot[nextPoint] - caveYBot[nextPoint - 1];
    if(posY > caveYBot[nextPoint - 1] +
       (posXDiff / caveXDiff) * caveYDiff){
        return(false);
    }
    if(!((posX < blockXPos[nextBlock]) ||
         (posX > blockXPos[nextBlock] + bW) ||
         (posY < blockYPos[nextBlock]) ||
         (posY > blockYPos[nextBlock] + blockHeight[nextBlock]))){
	blockYPos[nextBlock] = -blockYPos[nextBlock];
	redrawBlocks();
        return(true);
    }
    return(false);
}

function processFrame(){
    if(started){
        var newTime = new Date().getTime();
        var elapsed = newTime - oldTime;
        oldTime = newTime;
        var ti = elapsed / frameDelay; // time increment
        // big latencies shouldn't cause disaster
        if(ti > 5){
            ti = 2;
        }
        cave = backgroundDocument.getElementById("cavesetup");
        oldVB = cave.getAttribute("viewBox").split(" ");
	if(usingMouse){
	    if((shipPX <= shipX) && (shipX < 4998)){
		shipVX = ti;
		shipVY += Math.sign(shipPY - shipY) *
		    (shipAccelFactor * ti * (45 / frameDelay));
	    } else {
		vAng = Math.atan2(shipPY - shipY, shipPX - shipX);
		if(false){
		    shipVX = ti;
		    shipVY += Math.sign(shipPY - shipY) *
			(shipAccelFactor * ti * (45 / frameDelay));
		} else {
		    shipVX = ti;
		    newVY = (Math.sin(vAng) * ti) * (1/Math.abs(Math.cos(vAng)));
		    if(Math.abs(newVY - shipVY) <=
		       (shipAccelFactor * ti * (45 / frameDelay))){
			shipVY = newVY;
		    } else {
			shipVY += Math.sign(newVY - shipVY) *
			    (shipAccelFactor * ti * (45 / frameDelay));
		    }
		}
		
	    }
	} else {
	    shipVX = ti;
	    shipVY = shipVY + (shipAccel * (45 / frameDelay));
            //shipX = shipX + shipVX;
            //shipY = shipY + (ti * shipVelocity + 0.5 * shipAccel * shipAccel);
	}
        shipX = shipX + shipVX;
        shipY = shipY + shipVY;
	if((bulX > 0) && (shipVX > 0)){
	    bulX += ti * bulVX;
	    bulY += ti * bulVY;
	}
	oldPX = shipPX - leftEdge;
	oldPY = shipPY;
        if(shipX > leadSize){
            leftEdge = shipX - leadSize;
        } else {
            leftEdge = 0;
        }
	if(shipX < 0){
	    shipX = 0;
	}
        if(leftEdge > (cW - svgWidth)){
            leftEdge = (cW - svgWidth);
        }
        oldVB[0] = leftEdge;
        cave.setAttribute("viewBox", oldVB.join(" "));
        // var scorelabel = backgroundDocument.getElementById("scorelabel");
        // scorelabel.setAttribute("x",Number(oldVB[0]) + 190);
        var score = backgroundDocument.getElementById("scoretext");
        score.setAttribute("x",Number(oldVB[0]) + 120);
        score.firstChild.nodeValue = "Score: " + getScore();
	if(collide(shipX, shipY)){
	    lose();
	}
	if((bulX > 0) && collide(bulX, bulY)){
	    removeDestroyedColumn(bulX, bulY);
	    clearBullet();
	}
        if(shipX >= cW){
            shipX = cW;
            win();
        }
        ship = backgroundDocument.getElementById("shippath");
        pathText = pathText + " L" +
            Math.round(shipX*10)/10 + " " +  Math.round(shipY * 10) / 10;
        ship.setAttribute("d", pathText);
        ship = backgroundDocument.getElementById("shippos");
        ship.setAttribute("cx", shipX);
        ship.setAttribute("cy", shipY);
	//if(!collide(shipPX + ti * shipVX, shipPY)){
	if((shipX > leadSize) && (leftEdge < (cW - svgWidth)) && (usingMouse)){
	    shipPX = shipPX + shipVX;
	}
	updateBullet();
	updatePointer();
    }
    if(processNextFrame){
	requestAnimationFrame(processFrame);
    }
}

function getScore(){
    return(Math.floor(shipX)  - (timesActivated * 1000));
}

function win(){
    finished = true;
    var score = backgroundDocument.getElementById("scoretext");
    score.firstChild.nodeValue = "Won! " + getScore();
    alterPath(shipX, shipY, shipVY);
    stopAnimation();
}

function lose(){
    finished = true;
    alterPath(shipX, shipY, shipVY);
//    var ghost = backgroundDocument.getElementById("ghostpath");
//    ghost.setAttribute("d", ghost.getAttribute("d") + " " +
//                       shipCurveText.join(" "));
    stopAnimation();
    clearBullet();
    processNextFrame = false;
    requestAnimationFrame(explodeShip);
}

function saveSVG(){
    if(!finished){
        // if someone saves before crashing
        //alterPath(shipX, shipY, shipVY);
    }
    var saveForm = backgroundDocument.getElementById("saveform");
    var playername = saveForm.nameinput.value;
    
    var ship = backgroundDocument.getElementById("shippath");
    ship.setAttribute("d", shipCurveText.join(" "));
    var cave = backgroundDocument.getElementById("cave");
    var oldViewBox = cave.getAttribute("viewBox");
    var s = new XMLSerializer();
    var svgString = "<svg width=\"5000\" viewBox=\"0 0 5000 500\">" +
        s.serializeToString(cave) + "</svg>";
    localStorage.setItem("SVGdata", svgString);
    //window.open("savecave.svg");
    //window.open("data:image/svg+xml," + encodeURIComponent(svgString));
    // ## submit to script for "high scores"
    var myForm = document.createElement("form");
    myForm.method="post";
    myForm.action = "cgi-bin/svgcave_submitter.py";
    myForm.target = "_blank";
    myInput = document.createElement("input");
    myInput.setAttribute("name", "cave") ;
    myInput.setAttribute("value", encodeURIComponent(svgString));
    myForm.appendChild(myInput) ;
    myInput = document.createElement("input");
    myInput.setAttribute("name", "playername") ;
    myInput.setAttribute("value", encodeURIComponent(playername));
    myForm.appendChild(myInput) ;
    myInput = document.createElement("input");
    myInput.setAttribute("name", "framedelay") ;
    myInput.setAttribute("value", frameDelay);
    myForm.appendChild(myInput) ;
    document.body.appendChild(myForm);
    myForm.submit();
    document.body.removeChild(myForm);
    // alert(svgString);
}

function explodeShip(){
    if(explodeCount > 60){
	stopAnimation();
    } else if(!processNextFrame){
	if(explodeCount % 3 == 0){
            var ep = backgroundDocument.getElementById("explodepath");
            var epString = "M" + shipX + " " + shipY;
            if(explodeCount > 0){
		epString = ep.getAttribute("d");
            }
            epString = epString + "L" +
		(shipX + nextRandom() * cH - (cH / 2)) + " " +
		(shipY + nextRandom() * cH - (cH / 2)) + " L" +
		shipX + " " + shipY;
            ep.setAttribute("d", epString);
	}
        explodeCount = explodeCount + 1;
	requestAnimationFrame(explodeShip);
    }
}

function getEmail(){
  var count = 2; /* number of email addresses */
  var b = "";
  var a = Math.floor(nextRandom() * count);
  if (a==0) {b= "svgcave"+"%40gringer%2E"+"org"};
  if (a==1) {b= "hacking"+"%40gringer%2E"+"org"};
  b = "mailto:" + b;
  return b;
}

