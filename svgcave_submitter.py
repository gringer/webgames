#!/usr/bin/env python

import cgi # for cgi forms
import cgitb # for cgi trace-back
import urllib # for decoding URL encoding
import os # for file existence checks
import re # for regular expression work
import sys # for output without linebreaks

cgitb.enable() # make errors visible on web pages

form = cgi.FieldStorage()   # FieldStorage object to
                            # hold the form data

frameDelay = form.getfirst('framedelay','12')
frameDesc = "jogging"
if(frameDelay == "45"):
    frameDesc = "walking"
if(frameDelay == "12"):
    frameDesc = "running"
if(frameDelay == "8"):
    frameDesc = "sprinting"

playerName = form.getfirst('playername','Anonymous')

caveData = urllib.unquote(form.getfirst('cave',''))
caveData = re.sub('id=".*?"','',caveData)
caveData = re.sub('Score: ([0-9]+)</text>',
                  'Score: \\1</text><text text-anchor="left" ' +
                  'font-size="40px" y="440" x="20">[%s, %s]</text>' % (playerName, frameDesc),
                  caveData)

print('Content-type: text/html\n')
print '''<!DOCTYPE html>
<html>
 <head>
  <title>Saved SVGCaves</title>
 </head>
 <body>
  <h1>Saved SVGCaves</h1>'''

oldCaves = list()
added = 0
if(len(caveData) > 100):
    oldCaves.append("".join(caveData.split('\n')) + '\n')
    added += 1
if(os.path.exists('../webdata/savedcaves.svg')):
    f = open('../webdata/savedcaves.svg', 'r')
    for line in f:
        if(line.startswith('<svg')):
            if(added < 5):
                oldCaves.append(line)
                added += 1
    f.close()

f = open('../webdata/savedcaves.svg', 'w')
f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n')
for cave in oldCaves:
    f.write(cave)
    sys.stdout.write(cave)
f.close()
print(' </body>\n</html>')
