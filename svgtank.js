
// SVGTank game -- like Tank Wars, but with SVG and javascript
// author: David Eccles (gringer) <programming@gringer.org>

var backgroundDocument;
var started = false;
var projectileFired = false;
var leftEdge = 0;
var projectileX = 0;
var projectileY = 450;
var motionRotation = [0];
var oldPX = 200;
var oldPY = 200;
var projectilePX = 200;
var projectilePY = 200;
var projectileVX = 1;
var projectileVY = 0;
var bulX = -100;
var bulY = -100;
var bulVX = -1;
var bulVY = 0;
var usingMouse = false;
var timesActivated = 0;
var projectileAccelFactor = 0.08;
var projectileAccel = projectileAccelFactor;
var oldTime = new Date().getTime();
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second
var leadSize = 50; // traversed path to show at beginning of screen

var numTanks = 4;

const tankStartNames = ["Alice", "Janice", "Elena", "Jon", "Jessie",
			"Kevin", "Neeva", "Saleh", "Rebecca", "Laura",
			"Mauro", "Alexis", "Avril", "Rafia", "Artem",
			"Eugene", "Samantha", "Hannes", "Carolee",
			"Yann", "Emma", "Charlotte", "Mojca", "Zahra"];

const tankPosX = new Array();
const tankPosY = new Array();
const tankAngle = new Array();
const tankPower = new Array();
const tankHealth = new Array();
const tankName = new Array();
const tankSelectedWeapon = new Array();
const tankToggleBits = new Array();

var initialBeta = 0;

var finished = false;
var explodeCount = 0;

var cW = 809; // Tank Width
var cH = 500; // Tank Height
var sH = 6; // projectile Height

var svgWidth = 809;
var svgHeight = 600;

var canvasPixels;
var doneDropInitialisation = false;
const lastBooms = new Array();
var colsToDrop = new Array();
var boomDropDirt = false;
var dirtDropProbability = 0.1;
const weaponNames = ["Incinerator", "MkIIIncinerator", "20kNuke", "5MNuke",
		     "BallOfDirt", "MkIIDirt", "PressuredDirt", "HighPressuredDirt",
		     "ExplosiveDirt", "MIRV", "Bullet", "Grenade",
		     "Laser", "SonicBlaster", "CRI4D", "CRI8D", "CRI20D" ,"CRI20ND"];
const toggleNames = ["Herbs", "Vicks"];
const piAdj = Math.PI / 180;
var tankOrder = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var tankColourHue = [0.7886179, 0.6437659, 0.5943563, 0.5080214, 0.4205934, 0.2716390, 0.1930783, 0.1145833, 0.07033639, 0.04147105];
var dropTankAnimateId = null;
var dropTankTankId = 0;
var lastDropFrameTime = 0;
var currentTankId = 0;

var shiftPressed = false;
var altPressed = false;
var controlPressed = false;
var leftPressed = false;
var rightPressed = false;
var upPressed = false;
var downPressed = false;
var canFireProjectile = false;

var numPoints = 0;
var tankXPoints = new Array();
var tankYTop = new Array();
var tankYBot = new Array();
var nextPoint = 1;

var edgeCurvePos = 0;
var projectileCurvePos = 0;
var projectileCurveXPoints = new Array();
var projectileCurveYPoints = new Array();
var projectileCurveVel = new Array();
var projectileCurveText = new Array();
var pathText = "";

var randSeed = new Date().getTime();
var mazeSeed = randSeed;

document.onkeydown = processKeyDown;
document.onkeyup = processKeyUp;

function shuffle(array) {
    let currentIndex = array.length,  randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

	// Pick a remaining element.
	randomIndex = Math.floor(Math.random() * currentIndex);
	currentIndex--;

	// And swap it with the current element.
	[array[currentIndex], array[randomIndex]] = [
	    array[randomIndex], array[currentIndex]];
    }

    return array;
}

// https://stackoverflow.com/a/17243070/3389895
function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
	s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
    case 0: r = v, g = t, b = p; break;
    case 1: r = q, g = v, b = p; break;
    case 2: r = p, g = v, b = t; break;
    case 3: r = p, g = q, b = v; break;
    case 4: r = t, g = p, b = v; break;
    case 5: r = v, g = p, b = q; break;
    }
    return ("rgb(" + Math.round(r * 255) +
	    "," + Math.round(g * 255) +
	    "," + Math.round(b * 255) + ")")
}

function getBezierY(xs, xm, xe, ys, ym, ye){
    xs = Math.floor(xs + 0.5);
    xm = Math.floor(xm + 0.5);
    xe = Math.floor(xe + 0.5);    
    var xScale = (xe - xs);
    var p = (xm - xs) / xScale;
    var pd = 2 * p - 1;
    const yVals = new Array();
    const tVals = new Array();
    //console.log(["Bezier points:", xs, xm, xe, ys, ym, ye, p]);
    if(p == 0.5){ // this is outside the loop to improve efficiency
	for(let i = 0; i <= xScale; i++){
	    var t = i / xScale;
	    var y0 =   ys * (1 - t) + ym * t;
	    var y1 =   ym * (1 - t) + ye * t;
	    yVals[i] = y0 * (1 - t) + y1 * t;
	}
    } else {
	for(let i = 0; i <= xScale; i++){
	    var t = i / xScale;
	    dEqn = Math.sqrt(p**2 - 2*p*t + t);
	    p1 = (p - dEqn) / pd;
	    p2 = (p + dEqn) / pd;
	    t = ((p1 >= 0) && (p1 <= 1)) ? p1 : p2;
	    tVals[i] = t;
	    var y0 =   ys * (1 - t) + ym * t;
	    var y1 =   ym * (1 - t) + ye * t;
	    yVals[i] = y0 * (1 - t) + y1 * t;
	}
	//console.log(tVals);
    }
    // patch up ends
    yVals[0] = ys;
    yVals[xScale] = ye;
    return(yVals);
}

// the maze only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*9301+49297) % 233280;
    return randSeed/(233280.0);
}

function startup(evt) {
    backgroundDocument = evt.target.ownerDocument;
    // motion tracking from https://sensor-js.xyz/demo.html
    if (DeviceMotionEvent &&
	typeof(DeviceMotionEvent.requestPermission) === "function") {
	DeviceMotionEvent.requestPermission();
    }
    makeMaze(Math.floor(Math.random() * 10000));
}

function setForm() {
    var inputForm = backgroundDocument.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = mazeSeed;
        var speedValue = -1;
	for(var i = 0; i < inputForm.mazespeed.length; i++){
	    if(inputForm.mazespeed[i].checked){
		speedValue = i;
	    }
	}
	if(speedValue == -1){
            inputForm.mazespeed[2].checked = true;
            speedValue = 2;
        }
        if(speedValue == 0){
            frameDelay = 45;
        }
        if(speedValue == 1){
            frameDelay = 20;
        }
        if(speedValue == 2){
            frameDelay = 12;
        }
        if(speedValue == 3){
            frameDelay = 8;
        }
    }
}

function makeMaze(seed){
    randSeed = seed;
    mazeSeed = seed;
    setForm();
    backgroundDocument.activeElement.blur();
    var seedtext = backgroundDocument.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var projectilepath = backgroundDocument.getElementById("projectilepath");
    projectilepath.setAttribute("d", "");
    var tank = backgroundDocument.getElementById("tanksetup");
    tank.setAttribute("width", svgWidth);
    tank.setAttribute("height", svgHeight);
    randSeed = seed;
    mazeSeed = seed;
    var tankpath = backgroundDocument.getElementById("tankpath");
    started = false;
    // set up obstacles
    tankXPoints[0] = 0;
    tankYTop[0] = 5;
    tankYBot[0] = nextRandom() * (cH/2) + cH/2;
    numPoints = 1;
    var xPos = 0;
    // set up landscape
    // Landscape is created as quadratic cuves, with the control points
    // set randomly, and the vertices as the midpoints between two
    // adjacent control points
    var dist = 0;
    var cv = backgroundDocument.getElementById('tankcanvas').getContext('2d');
    canvasPixels = cv.createImageData(svgWidth, svgHeight);
    while(xPos <= (cW + dist)){
        dist = Math.round(((nextRandom() * (cH / 8)) + cH / 12) / 2) * 2;
        tankXPoints[numPoints] = xPos + dist;
        tankYTop[numPoints] = 5;
        tankYBot[numPoints] = Math.round(nextRandom() * (cH * 0.75) +
					 cH * 0.25);
        numPoints = numPoints + 1;
        xPos = xPos + dist;
    }
    // console.log("last point at " + xPos + " [width: " + cW + "]");
    fwdPath = new Array();
    revPath = new Array();
    lastRevX = tankXPoints[0];
    lastRevY = tankYBot[0];
    const bezierPointsX = new Array();
    const bezierPointsY = new Array();
    bezierPointsX.push(0); bezierPointsY.push(lastRevY);
    for(var i=0; i < numPoints; i = i + 1){
	curRevX = tankXPoints[i];
	curRevY = tankYBot[i];
	bezierPointsX.push((lastRevX + curRevX)/2);
	bezierPointsY.push((lastRevY + curRevY)/2);
	bezierPointsX.push(curRevX);
	bezierPointsY.push(curRevY);
	lastRevX = curRevX;
	lastRevY = curRevY;
    }
    // put bezier points onto canvas
    for(var i=1; i < (numPoints*2); i = i + 2){
	xs = bezierPointsX[i];
	xm = bezierPointsX[i+1];
	xe = bezierPointsX[i+2];
	ys = bezierPointsY[i];
	ym = bezierPointsY[i+1];
	ye = bezierPointsY[i+2];
	const yPoints = getBezierY(xs, xm, xe, ys, ym, ye);
	xScale = xe - xs;
	cv.strokeStyle = (i % 4 == 1) ? "#006700" : "#670000";
	for(var yi = 0; yi < yPoints.length; yi = yi + 1){
	    posX = Math.floor(xs+yi);
	    if(posX < cW){
		posY = Math.floor(yPoints[yi]);
		var topFrac = 1 - (yPoints[yi] - posY);
		for(var yyi = posY; yyi < cH; yyi = yyi + 1){
		    canvasPixels.data[(yyi * cW + (posX)) * 4 + 1] = 100;
		    canvasPixels.data[(yyi * cW + (posX)) * 4 + 3] =
			(yyi == posY) ? Math.floor(255 * topFrac) : 255;
		}
	    }
	}
    }
    //console.log(canvasPixels.width);
    //console.log(canvasPixels.height);
    //console.log(canvasPixels.data.length);
    cv.putImageData(canvasPixels, 0, 0);
    // Add tanks
    numTanks = backgroundDocument.getElementById("numtanksinput").value;
    shuffle(tankOrder);
    shuffle(tankStartNames);
    var tankEdgeGap = 50;
    var tankSep = (cW - (tankEdgeGap * 2)) / (numTanks - 1);
    // Reset tank positions
    for(var i = 0; i < 10; i = i + 1){
	var actualTankID = tankOrder[i];
	tankPosX[i] = -100;
	tankPosY[i] = -100;
	tankAngle[i] = 0;
	tankPower[i] = 200;
	tankHealth[i] = 100;
	tankSelectedWeapon[i] = 0;
	tankToggleBits[i] = 0;
	tankName[i] = tankStartNames[i];
	var tankGroup = backgroundDocument.getElementById("tankGroup" + ((actualTankID == 10) ? "" : "0") + actualTankID);
	tankGroup.setAttribute("transform", "translate(-100,-100)");
    }
    for(var i = 0; i < numTanks; i = i + 1){
	var actualTankID = tankOrder[i];
	var newPosX = Math.floor(tankEdgeGap + i * tankSep);
	var newPosY = -10;
	tankPosX[i] = newPosX;
	tankPosY[i] = newPosY;
	tankAngle[i] = (i == 0) ? 15 : ((i == numTanks - 1) ? -15 : 0);
	var tankTurret = backgroundDocument.getElementById("tankTurret" + ((actualTankID == 10) ? "" : "0") + actualTankID);
	tankTurret.setAttribute("transform", "rotate(" + tankAngle[i] + ")");
	while((newPosY < cH) && ( (newPosY < 0) || (canvasPixels.data[((newPosY * cW) + newPosX) * 4 + 1] < 255) ) ){
	    newPosY = newPosY + 1;
	}
	//console.log("tank " + actualTankID + " ("+newPosX+","+newPosY+"): " + canvasPixels.data[((newPosY * cW) + newPosX) * 4 + 3]);
    }
    prepareDropTanks();
}

function dropTanks(frameTime){
    var elapsedStart = (lastDropFrameTime == 0) ? 0 : (frameTime - lastDropFrameTime);
    var elapsed = elapsedStart;
    var animTankId = dropTankTankId;
    var travelDist = 0;
    lastDropFrameTime = frameTime;
    if(dropTankTankId < numTanks){
	var actualTankId = tankOrder[dropTankTankId];
	var newPosX = tankPosX[dropTankTankId];
	var newPosY = tankPosY[dropTankTankId] + 2;
	while((newPosY < (cH - 1)) &&
	      ( (newPosY < 0) || (canvasPixels.data[((newPosY * cW) + newPosX) * 4 + 3] < 255) ) ){
	    newPosY = newPosY + 1;
	    travelDist = travelDist + 1;
	    tankPosY[dropTankTankId] = newPosY - 2;
	    elapsed = elapsed - 3;
	    if(elapsed < 1){
		lastDropFrameTime = lastDropFrameTime - elapsed;
		break;
	    }
	}
	if(elapsed > 1){
	    lastDropFrameTime = 0;
	    dropTankTankId = dropTankTankId + 1;
	}
	if(travelDist > 0){
	    if(started){
		// add fall damage
		tankHealth[dropTankTankId] = Math.max(0, Math.ceil(tankHealth[dropTankTankId] - (travelDist / 2)));
	    }
	    // wipe away the dirt in the drop zone
	    var cv = backgroundDocument.getElementById('tankcanvas').getContext('2d');
	    cv.clearRect(newPosX - 12.5, newPosY - travelDist - 1, 25, travelDist);
	    canvasPixels = cv.getImageData(0, 0, cW, cH);
	    updateTurretDisplay(dropTankTankId);
	} else {
	}
	requestAnimationFrame(dropTanks);
    } else {
	changePlayer(!started);
	started = true;
	var deadCount = 0;
	for(var i = 0; i < numTanks; i = i + 1){
	    if(tankHealth[i] <= 0){
		deadCount = deadCount + 1;
	    }
	}
	if(deadCount >= (numTanks - 1)){
	    started = false;
	    makeMaze(mazeSeed);
	}
    }
}

function prepareDropTanks(){
    dropTankTankId = 0;
    lastDropFrameTime = 0;
    dropTankAnimateId = requestAnimationFrame(dropTanks);
}

function updateWeapon(tankToUpdate=-1){
    tankPlateSVG = backgroundDocument.getElementById("svgWeaponPlateEmbed").getSVGDocument();
    if(tankPlateSVG === null){
	return;
    }
    if(tankToUpdate == -1){
	tankToUpdate = currentTankId;
    }
    var currentWeaponId = tankSelectedWeapon[tankToUpdate];
    var toggleBits = tankToggleBits[tankToUpdate];
    currentWeapon = weaponNames[currentWeaponId];
    weaponNames.forEach(function(wName){
	weaponGroup = tankPlateSVG.getElementById("gWeapon" + wName);
	if(weaponGroup){
	    weaponGroup.setAttribute("opacity", wName == currentWeapon ? "1" : "0.25");
	    weaponRect = tankPlateSVG.getElementById("rWeapon" + wName);
	    weaponRect.setAttribute("stroke-width", wName == currentWeapon ? "2" : "1");
	}
    });
    for(let ti = 0; ti < 2; ti = ti + 1){
	tName = toggleNames[ti];
	tActive = ( (toggleBits & (1 << ti)) != 0 );
	toggleGroup = tankPlateSVG.getElementById("gToggle" + tName);
	if(toggleGroup){
	    toggleGroup.setAttribute("opacity", tActive ? "1" : "0.25");
	    toggleRect = tankPlateSVG.getElementById("rToggle" + tName);
	    toggleRect.setAttribute("stroke-width", tActive ? "2" : "1");
	}
    }
}

function updateMIRV(){
    MIRV = backgroundDocument.getElementById("MIRVpos");
    if(bulX > 0){
        MIRV.setAttribute("cx", bulX);
        MIRV.setAttribute("cy", bulY);
    } else {
        MIRV.setAttribute("cx", -100);
        MIRV.setAttribute("cy", -100);
    }
}

function clearMIRV(){
    bulX = -100;
    bulY = -100;
    bulVX = -1;
    bulVY = 0;
    updateMIRV();
}

function handleOrientation(event) {
    if(motionRotation.length < 3){
	motionRotation = [0,0,0];
    }
    if(event.alpha != null){
	motionRotation[0] = event.alpha.toFixed(2);
    }
    if(event.beta != null){
	motionRotation[1] = event.beta.toFixed(2);
    }
    if(event.gamma != null){
	motionRotation[2] = event.gamma.toFixed(2);
    }
}

function fireProjectile(){
    projectileFired = true;
    usingMouse = false;
    leftEdge = 0;
    var dX = -Math.cos((tankAngle[currentTankId] + 90) * piAdj);
    var dY = -Math.sin((tankAngle[currentTankId] + 90) * piAdj);
    var weaponMomentum = Math.max(0.25, Math.sqrt(getWeaponBoomRadius() / 25));
    projectileVX = dX * (tankPower[currentTankId] * 0.05) / weaponMomentum;
    projectileVY = dY * (tankPower[currentTankId] * 0.05) / weaponMomentum;
    projectileX = tankPosX[currentTankId] + dX * 25;
    projectileY = tankPosY[currentTankId] + dY * 25;
    // console.log("Current Tank ID: " + currentTankId);
    // console.log("Current Projectile Pos: (" + projectileX + ", " + projectileY + ")");
    oldPX = cW/2; // location of mouse pointer indicator
    oldPY = cH/2;
    projectilePX = cW/2;
    projectilePY = cH/2;
    clearMIRV();
    timesActivated = 0;
    projectileAccel = projectileAccelFactor;
    nextPoint = 1;
    tank = backgroundDocument.getElementById("tanksetup");
    oldVB = tank.setAttribute("viewBox","0 0 809 600");
    projectile = backgroundDocument.getElementById("projectilepath");
    projectile.setAttribute("d", d="M" + projectileX + "," + projectileY);
    projectileCurveXPoints = new Array();
    projectileCurveYPoints = new Array();
    projectileCurveVel = new Array();
    projectileCurveText = new Array();
    projectileCurveXPoints[0] = projectileX;
    projectileCurveYPoints[0] = projectileY;
    projectileCurveVel[0] = 0;
    projectileCurveText[0] = "M " + projectileX + "," + projectileY;
    projectileCurvePos = 1;
    edgeCurvePos = 0;
    pathText = "M " + projectileX + "," + projectileY;
    var ep = backgroundDocument.getElementById("explodepath");
    ep.setAttribute("d", "");
    var ep = backgroundDocument.getElementById("explodecircle");
    ep.setAttribute("cx", "-100");
    ep.setAttribute("cy", "-100");
    ep.setAttribute("r", "0");
    var epa = backgroundDocument.getElementById("explodeAnimation");
    epa.setAttribute("attributeName", "r");
    epa.setAttribute("values", "0");
    boomDropDirt = false;
    doneDropInitialisation = false;
    explodeCount = 0;
    oldTime = new Date().getTime();
    updateWeapon();
    requestAnimationFrame(moveProjectile);
}

function updatePointer(){
    if(!backgroundDocument){
	return;
    }
    pointerDot = backgroundDocument.getElementById("pointerdot");
    if(projectilePX > 0){
	pointerDot.setAttribute("cx", projectilePX);
	pointerDot.setAttribute("cy", projectilePY);
    } else {
	pointerDot.setAttribute("cx", -100);
	pointerDot.setAttribute("cy", -100);
    }
    if(motionRotation.length >= 3){
	angX = motionRotation[2] / 30;
	angY = motionRotation[1] / 30;
	pointerX = (angX * 250 + 250) % 500;
	pointerY = (angY * 250 + 250) % 500;
	orientationNode = backgroundDocument.getElementById("orientationMarker");
	orientationNode.setAttribute("visibility", "visible");
	orientationNode.setAttribute("transform", "translate(" + pointerX + leftEdge + "," + pointerY + ")");
	usingMouse = true;
	projectilePX = pointerX + leftEdge;
	projectilePY = pointerY;
    }
}

function movePointer(e){
    usingMouse = true;
    projectilePX = e.offsetX + leftEdge;
    projectilePY = e.offsetY;
    updatePointer();
}

function clearPointer(shiftPoss){
    if(shiftPoss){
	projectilePX = -100;
	projectilePY = projectileY;
    }
    updatePointer();
}

function changePlayer(chooseRandom = false){
    var deadCount = 0;
    for(var i = 0; i < numTanks; i = i + 1){
	if(tankHealth[i] <= 0){
	    deadCount = deadCount + 1;
	}
    }
    if(deadCount >= (numTanks - 1)){
	return;
    }
    if(chooseRandom){
	currentTankId = Math.floor(nextRandom() * numTanks);
    } else {
	currentTankId = (currentTankId + 1) % numTanks;
	while(tankHealth[currentTankId] <= 0){
	    currentTankId = (currentTankId + 1) % numTanks;
	}
    }
    canFireProjectile = true;
    updateTurretDisplay();
}

function updateTurretDisplay(tankToUpdate=-1){
    var deadCount = 0;
    for(var i = 0; i < numTanks; i = i + 1){
	if(tankHealth[i] <= 0){
	    deadCount = deadCount + 1;
	}
	var actualTankID = tankOrder[i];
	var tankGroup = backgroundDocument.getElementById("tankGroup" + ((actualTankID == 10) ? "" : "0") + actualTankID);
	tankGroup.setAttribute("transform", "translate(" + tankPosX[i] + "," + tankPosY[i] + ")");
	tankGroup.setAttribute("stroke", HSVtoRGB(tankColourHue[actualTankID - 1], 1, tankHealth[i] / 100));
	tankGroup.setAttribute("fill", HSVtoRGB(tankColourHue[actualTankID - 1], 1, tankHealth[i] / 100));
	var tankTurret = backgroundDocument.getElementById("tankTurret" + ((actualTankID == 10) ? "" : "0") + actualTankID);
	tankTurret.setAttribute("transform", "rotate(" + tankAngle[i] + ")");
	// wipe turret location from canvas
	var cv = backgroundDocument.getElementById('tankcanvas').getContext('2d');
	var dX = -Math.cos((tankAngle[i] + 90) * piAdj);
	var dY = -Math.sin((tankAngle[i] + 90) * piAdj);
	cv.save();
	cv.translate(tankPosX[i], tankPosY[i]);
	cv.rotate((tankAngle[i] + 180) * piAdj);
	cv.clearRect(-2, 0, 4, 22);
	cv.restore();
    }
    if(tankToUpdate == -1){
	tankToUpdate = currentTankId;
    }
    if(tankHealth[tankToUpdate] > 0){
	tankPower[tankToUpdate] = Math.max(0, Math.min(tankPower[tankToUpdate], 10 * tankHealth[tankToUpdate]));
	var nameText = backgroundDocument.getElementById("nametext");
	var angleText = backgroundDocument.getElementById("angletext");
	var powerText = backgroundDocument.getElementById("powertext");
	var healthText = backgroundDocument.getElementById("healthtext");
	var directionSymbol = backgroundDocument.getElementById("directionsymbol");
	nameText.firstChild.nodeValue = tankName[tankToUpdate];
	nameText.setAttribute("fill", HSVtoRGB(tankColourHue[tankOrder[tankToUpdate] - 1], 1, 1 ));
	angleText.firstChild.nodeValue = "Angle: " + (90 - Math.abs(tankAngle[tankToUpdate]));
	powerText.firstChild.nodeValue = "Power: " + tankPower[tankToUpdate];
	healthText.firstChild.nodeValue = "Health: " + tankHealth[tankToUpdate];
	directionSymbol.setAttribute("href", (tankAngle[tankToUpdate] < 0) ? "#gDirLeft" : "#gDirRight");
	updateWeapon(tankToUpdate);
    }
    canvasPixels = cv.getImageData(0, 0, cW, cH);
}

function adjustTurret(){
    if(!canFireProjectile){
	return;
    }
    if(leftPressed){
	tankAngle[currentTankId] = tankAngle[currentTankId] - 1;
	if(tankAngle[currentTankId] < -90){
	    tankAngle[currentTankId] = 90;
	}
    }
    if(rightPressed){
	tankAngle[currentTankId] = tankAngle[currentTankId] + 1;
	if(tankAngle[currentTankId] > 90){
	    tankAngle[currentTankId] = -90;
	}
    }
    if(upPressed){
	tankPower[currentTankId] = tankPower[currentTankId] + (shiftPressed ? 100 : 1);
	if(tankPower[currentTankId] >= 1000){
	    tankPower[currentTankId] = 1000;
	    upPressed = false;
	}
    }
    if(downPressed){
	tankPower[currentTankId] = tankPower[currentTankId] - (shiftPressed ? 100 : 1);
	if(tankPower[currentTankId] <= 0){
	    tankPower[currentTankId] = 0;
	    downPressed = false;
	}
    }
    updateTurretDisplay();
    if(!altPressed && (leftPressed || rightPressed || upPressed || downPressed)){
	requestAnimationFrame(adjustTurret);
    }
}

function processMouseClick(e){
    //if(canFireProjectile){
	//canFireProjectile = false;
	//fireProjectile();
    //}
    return true;
}

function processKeyDown(e){
    if(e.keyCode == 16){
	shiftPressed = true;
    } 
    if(e.keyCode == 17){
	controlPressed = true;
    }
    if(e.keyCode == 18){
	altPressed = true;
    }
    if(((e.keyCode == 37) || (e.keyCode == 65)) && !leftPressed){
	leftPressed = true;
	requestAnimationFrame(adjustTurret);
    }
    if(((e.keyCode == 39) || (e.keyCode == 68)) && !rightPressed){
	rightPressed = true;
	requestAnimationFrame(adjustTurret);
    }
    if(((e.keyCode == 38) || (e.keyCode == 87)) && !upPressed){
	upPressed = true;
	requestAnimationFrame(adjustTurret);
    }
    if(((e.keyCode == 40) || (e.keyCode == 83)) && !downPressed){
	downPressed = true;
	requestAnimationFrame(adjustTurret);
    }
    if (e.keyCode == 72){
	if(!projectileFired){
	    tankToggleBits[currentTankId] = tankToggleBits[currentTankId] ^ 1;
	    updateWeapon();
	}
    } else if (e.keyCode == 86){
	if(!projectileFired){
	    tankToggleBits[currentTankId] = tankToggleBits[currentTankId] ^ 2;
	    updateWeapon();
	}
    } else if (e.keyCode == 188){
	if(!projectileFired){
	    tankSelectedWeapon[currentTankId] =
		(tankSelectedWeapon[currentTankId] + weaponNames.length - 1) % weaponNames.length;
	    updateWeapon();
	}
    } else if (e.keyCode == 190){
	if(!projectileFired){
	    tankSelectedWeapon[currentTankId] =
		(tankSelectedWeapon[currentTankId] + 1) % weaponNames.length;
	    updateWeapon();
	}
    } else if ((e.keyCode == 13) || (e.keyCode == 32)){
	if(canFireProjectile){
	    canFireProjectile = false;
	    fireProjectile();
	}
    }
}

function processKeyUp(e){
    if(e.keyCode == 16){
	shiftPressed = false;
    }
    if(e.keyCode == 16){
	controlPressed = false;
    }
    if(e.keyCode == 18){
	altPressed = false;
    }
    if(((e.keyCode == 37) || (e.keyCode == 65))){
	leftPressed = false;
    }
    if(((e.keyCode == 39) || (e.keyCode == 68))){
	rightPressed = false;
    }
    if(((e.keyCode == 38) || (e.keyCode == 87))){
	upPressed = false;
    }
    if(((e.keyCode == 40) || (e.keyCode == 83))){
	downPressed = false;
    }
}

function alterPath(xE, yE, vE){
    xS = projectileCurveXPoints[projectileCurvePos - 1];
    yS = projectileCurveYPoints[projectileCurvePos - 1];
    vS = projectileCurveVel[projectileCurvePos - 1];
    if(xE - xS > 0){
        projectileCurveXPoints[projectileCurvePos] = xE;
        projectileCurveYPoints[projectileCurvePos] = yE;
        projectileCurveVel[projectileCurvePos] = vE;
        // mid quadratic curve point is line intersection of tangent at start and tangent at end
        // y1 = vS (x - xS) + yS [line 1]
        // y2 = vE (x - xE) + yE [line 2]
        // solve for x: vS * (x - xS) + yS = vE * (x - xE) + yE
        // => vS * x - vS * xS + yS = vE * x - vE * xE + yE
        // => vS * x - vE * x = vS * xS - vE * xE - yS + yE
        // => x * (vS - vE) = vS * xS - vE * xE - (yS - yE)
        // => x = (vS * xS - vE * xE - (yS - yE)) / (vS - vE)
        var xM;
        if(vS - vE == 0){
            xM = (xE - xS) / 2;
        } else {
            xM = (vS * xS - vE * xE - (yS - yE)) / (vS - vE);
        }
        var yM = vS * (xM - xS) + yS;
        projectileCurveText[projectileCurvePos] = "Q" + xM + " " + yM + " " +
            projectileCurveXPoints[projectileCurvePos] + " " + projectileCurveYPoints[projectileCurvePos];
        while((leftEdge) > projectileCurveXPoints[edgeCurvePos]){
            edgeCurvePos = edgeCurvePos + 1;
        }
        edgeCurvePos = Math.max(0,edgeCurvePos-1);
        projectileCurvePos = projectileCurvePos + 1;
    }
}

function collide(posX, posY){
    // assume a 5x5 hit box centred on posX, posY
    posX = Math.min(cW-3, Math.max(2, Math.round(posX)));
    posY = Math.min(cH-3, Math.max(2, Math.round(posY)));
    hitTotal = 0;
    for (let px = posX - 2; px <= posX+2; px = px + 1) {
	for (let py = posY - 2; py <= posY+2; py = py + 1) {
	    hitTotal = hitTotal + canvasPixels.data[((py) * cW + (px)) * 4 + 3];
	}
    }
    hitTotal = hitTotal - ( // subtract half the diagonals
	canvasPixels.data[((posY-2) * cW + (posX-2)) * 4 + 3] +
	    canvasPixels.data[((posY-2) * cW + (posX+2)) * 4 + 3] +
	    canvasPixels.data[((posY+2) * cW + (posX-2)) * 4 + 3] +
	    canvasPixels.data[((posY+2) * cW + (posX+2)) * 4 + 3]) / 2;
    for(let i = 0; i < numTanks; i = i + 1){
	tankX = tankPosX[i];
	tankY = tankPosY[i];
	tankDist = Math.sqrt((tankX - posX)  ** 2 + (tankY - posY) ** 2);
	if((tankDist < 12.5) && (tankY >= posY)){
	    hitTotal = hitTotal + 255 * 100;
	    console.log("Hit a tank!");
	}
	// Alternatively... check line intersection
	// https://mathworld.wolfram.com/Circle-LineIntersection.html
    }
    return(hitTotal / 255); // approximate number of dirt dots hit
}

function moveProjectile(){
    var newTime = new Date().getTime();
    var elapsed = newTime - oldTime;
    oldTime = newTime;
    var ti = elapsed / frameDelay; // time increment
    // big latencies shouldn't cause disaster
    if(ti > 5){
        ti = 2;
    }
    tank = backgroundDocument.getElementById("tanksetup");
    oldVB = tank.getAttribute("viewBox").split(" ");
    projectileVY = projectileVY + (projectileAccel * (45 / frameDelay));
    adjVX = projectileVX * ti;
    adjVY = projectileVY * ti;
    projectileX = projectileX + adjVX;
    projectileY = projectileY + adjVY;
    if((bulX > 0) && (projectileVX > 0)){
	bulX += ti * bulVX;
	bulY += ti * bulVY;
    }
    oldPX = projectilePX - leftEdge;
    oldPY = projectilePY;
    if(projectileX > leadSize){
        leftEdge = projectileX - leadSize;
    } else {
        leftEdge = 0;
    }
    if(projectileX < 0){
	projectileX = 0;
    }
    if(leftEdge > (cW - svgWidth)){
        leftEdge = (cW - svgWidth);
    }
    oldVB[0] = leftEdge;
    //tank.setAttribute("viewBox", oldVB.join(" "));
    // var scorelabel = backgroundDocument.getElementById("scorelabel");
    // scorelabel.setAttribute("x",Number(oldVB[0]) + 190);
    var projectileShouldContinue = true;
    var speed = Math.sqrt(projectileVX ** 2 + projectileVY ** 2);
    if(projectileX >= cW){
	projectileShouldContinue = false;
	projectileFired = false;
        // projectileX = cW;
	prepareDropTanks();
    } else if(projectileX <= 0){
	projectileShouldContinue = false;
	projectileFired = false;
        // projectileX = 0;
	prepareDropTanks();
    } else if(projectileY >= cH){
	// projectileY = cH - 1;
	projectileShouldContinue = false;
	startExplode();
    } else {
	var dirtHit = collide(projectileX, projectileY);
	var weaponMomentum = getWeaponBoomRadius() / 25;
	if(speed - (dirtHit / (weaponMomentum + 1)) <= 0){
	    console.log("speed at explosion: " + speed);
	    projectileShouldContinue = false;
	    startExplode();
	} else if (dirtHit > 0) {
	    console.log("speed at slow down: " + speed);
	    newSpeedFac = (speed - (dirtHit / (weaponMomentum + 1))) / speed;
	    projectileVX = newSpeedFac * projectileVX;
	    projectileVY = newSpeedFac * projectileVY;
	}
    }
    projectile = backgroundDocument.getElementById("projectilepath");
    pathText = pathText + " L" +
        Math.round(projectileX*10)/10 + " " +  Math.round(projectileY * 10) / 10;
    projectile.setAttribute("d", pathText);
    projectile = backgroundDocument.getElementById("projectilepos");
    projectile.setAttribute("cx", projectileX);
    projectile.setAttribute("cy", projectileY);
    updateMIRV();
    updatePointer();
    if(projectileShouldContinue){
	requestAnimationFrame(moveProjectile);
    }
}

function getScore(){
    return(Math.floor(projectileX)  - (timesActivated * 1000));
}

function startExplode(){
    projectileExploding = true;
    alterPath(projectileX, projectileY, projectileVY);
    clearMIRV();
    explodeProjectile();
}

function saveSVG(){
    if(!finished){
        // if someone saves before crashing
        //alterPath(projectileX, projectileY, projectileVY);
    }
    var saveForm = backgroundDocument.getElementById("saveform");
    var playername = saveForm.nameinput.value;
    
    var projectile = backgroundDocument.getElementById("projectilepath");
    var tank = backgroundDocument.getElementById("tank");
    var oldViewBox = tank.getAttribute("viewBox");
    var s = new XMLSerializer();
    var svgString = '<?xml version="1.0" standalone="no"?>\r\n' +
	"<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"809\" viewBox=\"0 0 809 600\">" +
        s.serializeToString(tank) + "</svg>";
    var svgBlob = new Blob([svgString], {type:"image/svg+xml;charset=utf-8"});
    var svgUrl = URL.createObjectURL(svgBlob);
    var downloadLink = backgroundDocument.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = "savetank.svg";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}

function dropDirt(evt){
    // console.log("Calling drop dirt");
    if(!doneDropInitialisation){
	colsToDrop.length = 0;
	if((tankSelectedWeapon[currentTankId] == 13) || (nextRandom() < dirtDropProbability)){
	    for(let ix = 0; ix < cW; ix = ix + 1){
		var maxDirt = 0;
		for(let iy = 0; iy < cH; iy = iy + 1){
		    var cp = canvasPixels.data[(iy * cW + ix) * 4 + 3];
		    maxDirt = (cp > maxDirt) ? cp : maxDirt;
		    if(cp < maxDirt){
			colsToDrop.push(ix);
			break;
		    }
		}
	    }
	}
	doneDropInitialisation = true;
    }
    if(colsToDrop.length > 0){
	//console.log("Dropping " + colsToDrop.length + " columns");
	//console.log(colsToDrop);
	for(let ixi = 0; ixi < colsToDrop.length; ixi = ixi + 1){
	    ix = colsToDrop[ixi];
	    var dirtSum = 0;
	    for(let iy = (cH-1); iy >= 0; iy = iy - 1){
		dirtSum = dirtSum + canvasPixels.data[(iy * cW + ix) * 4 + 3];
	    }
	    //console.log("Dirt sum for column " + ix + ": " + dirtSum);
	    for(let iy = (cH-1); iy >= 0; iy = iy - 1){
		canvasPixels.data[(iy * cW + ix) * 4 + 1] = (dirtSum > 0) ? 100 : 0;
		canvasPixels.data[(iy * cW + ix) * 4 + 3] = (dirtSum >= 255) ? 255 : dirtSum;
		dirtSum = (dirtSum > 255) ? dirtSum - 255 : 0;
	    }
	}
	var cv = backgroundDocument.getElementById('tankcanvas').getContext('2d');
	cv.putImageData(canvasPixels, 0, 0);
    }
    boomDropDirt = false;
    prepareDropTanks();
}

function explodeTanks(boomX, boomY, boomRad){
    for(var i = 0; i < numTanks; i = i + 1){
	tankX = tankPosX[i];
	tankY = tankPosY[i];
	tankDist = Math.sqrt((tankX - boomX)  ** 2 + (tankY - boomY) ** 2);
	if(tankDist < boomRad){
	    tankHealth[i] = Math.max(0, tankHealth[i] - 100);
	} else if((tankDist - boomRad) < 25){
	    var healthSub = (100 * boomRad / 160) * ((tankDist - boomRad) / 25);
	    tankHealth[i] = Math.max(0, Math.floor(tankHealth[i] - healthSub));
	}
    }
    updateTurretDisplay();
}

function doneBoom(evt){
    if(boomDropDirt){
	dropDirt(evt);
    } else if(lastBooms.length > 0){
	projectileFired = false;
	var boomRad = lastBooms.pop();
	var boomY = lastBooms.pop();
	var boomX = lastBooms.pop();
        var ep = backgroundDocument.getElementById("explodecircle");
        ep.setAttribute("opacity", "0");
        var epa = backgroundDocument.getElementById("explodeAnimation");
	epa.setAttribute("attributeName", "opacity");
	epa.setAttribute("values", "1;0");
	epa.setAttribute("dur", "1s");
	epa.setAttribute("repeatCount", "1");
	doneDropInitialisation = false;
	boomDropDirt = true;
	epa.beginElement();
	var cv = backgroundDocument.getElementById('tankcanvas').getContext('2d');
	cv.save();
	var weaponType = "explode";
	var currentWeaponId = tankSelectedWeapon[currentTankId];
	if((currentWeaponId >= 4) && (currentWeaponId <= 8)){
	    weaponType = "dirt";
	} else if(currentWeaponId == 13){
	    weaponType = "sonic";
	    cv.strokeStyle = "#00000000";
	}
	if(weaponType == "explode"){
	    explodeTanks(boomX, boomY, boomRad);
	    cv.fillStyle = "#00000000";
	    cv.strokeStyle = "#00000000";
	    cv.beginPath();
	    cv.arc(boomX, boomY, boomRad, 0, 2 * Math.PI, true);
	    cv.fill();
	    cv.clip();
	    cv.clearRect(0,0,cW,cH);
	} else if(weaponType == "dirt"){
	    cv.fillStyle = "#006400ff";
	    cv.strokeStyle = "#006400ff";
	    if(currentWeaponId == 8){ // explosive dirt
		dy = boomY;
		dx = dy * 0.5;
		canvasPixels = cv.getImageData(0, 0, cW, cH);
		// cost for explosive dirt is approximately the same as a ball of dirt
		// so use the same amount of dirt for explosive dirt
		// i.e. radius = 30; approx 2800 pixels
		for(let di = 0; di < 3000; di++){
		    var deltaY = (Math.min(200, boomY) * nextRandom());
		    var deltaX = (nextRandom() - 0.5) * deltaY;
		    var draw = true;
		    let posX = Math.floor(boomX + deltaX);
		    let posY = Math.floor(boomY - deltaY);
		    if(posX < 0){
			continue;
		    }
		    if(posX > cW){
			continue;
		    }
		    //console.log("Putting dirt dot at " + (boomX+dx) + "," + (boomY + dy));
		    canvasPixels.data[((posY) * cW + (posX)) * 4 + 1] = 100;
		    canvasPixels.data[((posY) * cW + (posX)) * 4 + 3] = 255;
		}
		cv.putImageData(canvasPixels, 0, 0);
	    } else {
		cv.beginPath();
		cv.arc(boomX, boomY, boomRad, 0, 2 * Math.PI, true);
		cv.fill();
	    }
	}
	cv.restore();
	// add bottom line
	cv.strokeStyle = "#006400ff";
	cv.lineWidth = 2;
	cv.beginPath();
	cv.moveTo(0,cH);
	cv.lineTo(cW,cH);
	cv.stroke();
	canvasPixels = cv.getImageData(0, 0, cW, cH);
    } else {
	requestAnimationFrame(dropTanks);
    }
}

function getWeaponBoomRadius(){
    // used for calculating momentum and explosion size
    var weaponBoomRadius = 25;
    var currentWeaponId = tankSelectedWeapon[currentTankId];
    if((currentWeaponId == 0) || (currentWeaponId == 4)){
	weaponBoomRadius = 25;
    } else if ((currentWeaponId == 1) || (currentWeaponId == 5) || (currentWeaponId == 9)){
	weaponBoomRadius = 45;
    } else if ((currentWeaponId == 2) || (currentWeaponId == 6)){
	weaponBoomRadius = 80;
    } else if ((currentWeaponId == 3) || (currentWeaponId == 7)){
	weaponBoomRadius = 160;
    } else if (currentWeaponId == 10){
	weaponBoomRadius = 2;
    } else if (currentWeaponId == 11){
	weaponBoomRadius = 8;
    }
    return(weaponBoomRadius);
}

function explodeProjectile(){
    var boomRadius = getWeaponBoomRadius();
    var ep = backgroundDocument.getElementById("explodecircle");
    var weaponType = "explode";
    ep.setAttribute("stroke", "#000000ff");
    var currentWeaponId = tankSelectedWeapon[currentTankId];
    if((currentWeaponId >= 4) && (currentWeaponId <= 8)){
	weaponType = "dirt";
	if(currentWeaponId == 8){
	    boomRadius = 0;
	    ep.setAttribute("stroke", "#00000000");
	} else {
	    boomRadius = boomRadius - 1;
	}
    }
    if(currentWeaponId == 13){
	weaponType = "sonic";
	boomRadius = 903;
	ep.setAttribute("stroke", "#00000000");
    }
    var startRad = boomRadius < 4 ? boomRadius / 4 : 4;
    var endRad = boomRadius;
    var animDur = boomRadius <= 4 ? 0.1 : Math.sqrt(boomRadius) / 50;
    if((weaponType == "explode") || (weaponType == "dirt") || (weaponType == "sonic")){
	ep.setAttribute("fill", "url(#" + weaponType + "RadialGradient)");
    }
    if(currentWeaponId == 8){
	animDur=0.2;
    }
    ep.setAttribute("cx", projectileX);
    ep.setAttribute("cy", projectileY);
    ep.setAttribute("r", endRad);
    ep.setAttribute("opacity", "1");
    boomDropDirt = false;
    lastBooms.push(projectileX);
    lastBooms.push(projectileY);
    lastBooms.push(endRad);
    var epa = backgroundDocument.getElementById("explodeAnimation");
    epa.setAttribute("attributeName", "r");
    epa.setAttribute("values", startRad + ";" + endRad);
    epa.setAttribute("dur", animDur + "s");
    epa.setAttribute("repeatCount", "1");
    epa.beginElement();
}

function getEmail(){
  var count = 2; /* number of email addresses */
  var b = "";
  var a = Math.floor(nextRandom() * count);
  if (a==0) {b= "svgtank"+"%40gringer%2E"+"org"};
  if (a==1) {b= "hacking"+"%40gringer%2E"+"org"};
  b = "mailto:" + b;
  return b;
}

