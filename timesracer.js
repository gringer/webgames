// Going Viral game -- viral isolation game, with SVG and javascript
// author: David Eccles (gringer) <bioinformatics@gringene.org>

// TODO:
//  * consider using window.localStorage for high score tables
//    - best times for each size
//    - mean times + number of plays for current table
//  * track / report mouse travel distance

var bDoc;
var svgNS = "http://www.w3.org/2000/svg";
var started = false;
var leftEdge = 0;
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second

var started = false;
var finished = false;

// game / screen settings
var svgWidth = 500;
var svgHeight = 500;
var gameWidth = 400;
var gameHeight = 400;
var mouseX = -1;
var mouseY = -1;

// number game parameters
var numX = 5;
var numY = 5;
var textSize = 10;
var boxWidth = gameWidth / numX;
var boxHeight = gameHeight / numY;
var numLocs = new Array();
var xNums = new Array();
var yNums = new Array();

// game settings
var startTime = -1;
var elapsedTime = -1;
var stopTime = -1;
var stoppedTime = 0;
var alertedWon = false;
var lastClickPos = -1;

var randSeed = new Date().getTime();
var gameSeed = randSeed;
var lastLog = 0;

var gameTimer = setInterval(processFrame, (1000/fps));

var useStorage = false;

if (typeof(Storage) !== "undefined") {
    useStorage = true;
}

// the game only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*9301+49297) % 233280;
    return randSeed/(233280.0);
}

function startup(evt) {
    bDoc = evt.target.ownerDocument;
    makeGame(Math.floor(Math.random() * 10000), 5);
}

function makeGame(seed, size){
    // reset timer (if not stopped)
    clearInterval(gameTimer);
    // reset win state
    started = false;
    alertedWon = false;
    lastClickPos = -1;
    startTime = -1;
    stopTime = -1;
    stoppedTime = 0;
    // set up form variables
    randSeed = seed;
    gameSeed = seed;
    numX = size;
    numY = size;
    setForm();
    boxWidth = gameWidth / numX;
    boxHeight = gameHeight / numY;
    textSize = boxHeight / 3;
    if(textSize > 20){
	textSize = 20;
    }
    var seedtext = bDoc.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var game = bDoc.getElementById("gamesetup");
    game.setAttribute("width", svgWidth + "px");
    game.setAttribute("height", svgHeight + "px");
    randSeed = seed;
    gameSeed = seed;
    // reset game variables
    numLocs = new Array();
    xNums = new Array();
    yNums = new Array();
    // create random shuffle of numbers 1..12
    shuffleX = new Array();
    shuffleY = new Array();
    for(var i=0; i < 12; i++){
	shuffleX[i] = [i+1, nextRandom()];
	shuffleY[i] = [i+1, nextRandom()];
    }
    // sort array based on random variable
    shuffleX.sort(function(a,b){return(a[1]-b[1])});
    shuffleY.sort(function(a,b){return(a[1]-b[1])});
    // set up numbers / array by sorting based on shuffle order
    for(var i=0; i < Math.max(numX, numY); i++){
	// add numbers to array up to size limit
	if(i < numX){
	    xNums[i] = (shuffleX[i])[0];
	}
	if(i < numY){
	    yNums[i] = (shuffleY[i])[0];
	}
    }
    // create table
    for(var ix=0; ix < numX; ix++){
	for(var iy=0; iy < numY; iy++){
	    numLocs[(iy * numX) + ix] = xNums[ix] * yNums[iy];
	}
    }
    // scramble board by swapping each position with a random [other] one
    for(pos = 0; pos < (numX * numY); pos++){
	randPos = Math.floor(nextRandom() * (numX * numY - 1));
	if(randPos >= pos){
	    randPos += 1;
	}
	tmpPos = numLocs[pos];
	numLocs[pos] = numLocs[randPos];
	numLocs[randPos] = tmpPos;
    }
    // if the game is already solved, randomly swap the first position with
    // another one
    if(checkCorrect()){
	pos = Math.floor(nextRandom() * (numX * numY));
	randPos = Math.floor(nextRandom() * (numX * numY - 1));
	if(randPos >= pos){
	    randPos += 1;
	}
	tmpPos = numLocs[pos];
	numLocs[pos] = numLocs[randPos];
	numLocs[randPos] = tmpPos;
    }
    // draw board / numbers
    drawNumbers();
    // start timer
    startTime = new Date();
    elapsedTime = 0;
    gameTimer = setInterval(processFrame, (1000/fps));
    updateTimerText();
    // update score
    started = true;
    checkCorrect();
}

function drawNumbers(){
    var outerpath = bDoc.getElementById("grid_boundary");
    var innerpath = bDoc.getElementById("inner_grid");
    var numberGroup = bDoc.getElementById("g_numbers");
    var outertext = "";
    var innertext = "";
    var foundPoints = [];
    var collided = [];
    var addedCollision = [];
    var pointLoc = [];
    // create border
    outertext = "m 50,50 h " + gameWidth + " v " + gameHeight + " h -" +
	gameWidth + " v -" + gameHeight;
    // clear existing numbers (if they exist)
    var outernums = bDoc.getElementById("nums_boundary");
    if(outernums){
	outernums.remove();
    }
    var gridnums = bDoc.getElementById("nums_grid");
    if(gridnums){
	gridnums.remove();
    }
    // create grid and outer numbers
    var outerNumGroup =
	bDoc.createElementNS(svgNS, 'g');
    outerNumGroup.setAttribute("id", "nums_boundary");
    numberGroup.appendChild(outerNumGroup);
    var innerNumGroup =
	bDoc.createElementNS(svgNS, 'g');
    innerNumGroup.setAttribute("id", "nums_grid");
    numberGroup.appendChild(innerNumGroup);
    for(var ix=0; ix <= numX; ix++){
	innertext += "M " + (50 + ix * boxWidth) +
	    ",5 v " + (gameHeight + 45) + "\n";
	if(ix < numX){
	    var newText = bDoc.createElementNS(svgNS, "text");
	    newText.setAttribute("id", "oX_"+ix);
	    newText.setAttribute("x", 50 + (ix+0.5) * boxWidth);
	    newText.setAttribute("y", 27.5 + textSize/2);
	    newText.setAttribute("font-size", textSize*1.5 + "px");
	    newText.setAttribute("fill", "black");
	    newText.setAttribute("text-anchor", "middle");
	    var textNode = document.createTextNode(xNums[ix]);
	    newText.appendChild(textNode);
	    outerNumGroup.appendChild(newText);
	}
    }
    for(var iy=0; iy <= numY; iy++){
	innertext += "M 5," + (50 + iy * boxHeight) +
	    " h " + (gameWidth + 45) + "\n";
	if(iy < numY){
	    var newText = bDoc.createElementNS(svgNS, "text");
	    newText.setAttribute("id", "oY_"+iy);
	    newText.setAttribute("x", 27.5);
	    newText.setAttribute("y", 50 + (iy+0.5) * boxWidth + textSize/2);
	    newText.setAttribute("font-size", textSize*1.5 + "px");
	    newText.setAttribute("fill", "black");
	    newText.setAttribute("text-anchor", "middle");
	    var textNode = document.createTextNode(yNums[iy]);
	    newText.appendChild(textNode);
	    outerNumGroup.appendChild(newText);
	}
    }
    for(var ix=0; ix < numX; ix++){
	for(var iy=0; iy < numY; iy++){
	    var midX = 50 + (ix+0.5) * boxWidth;
	    var midY = 50 + (iy+0.5) * boxHeight;
	    var pos = (iy * numX) + ix;
	    var newText = bDoc.createElementNS(svgNS, "text");
	    newText.setAttribute("id", "i_"+pos);
	    newText.setAttribute("x", midX);
	    newText.setAttribute("y", midY + textSize/2);
	    if(numLocs[(iy * numX) + ix] == (xNums[ix] * yNums[iy])){
		newText.setAttribute("font-size", textSize*1.25 + "px");
		newText.setAttribute("fill", "lightgreen");
	    } else {
		newText.setAttribute("font-size", textSize + "px");
		newText.setAttribute("fill", "steelblue");
	    }
	    newText.setAttribute("text-anchor", "middle");
	    var textNode = document.createTextNode(numLocs[pos]);
	    newText.appendChild(textNode);
	    innerNumGroup.appendChild(newText);
	    // create numLocs[(iy * numX) + ix] at (startX, startY);
	}
    }
    outerpath.setAttribute("d", outertext);
    innerpath.setAttribute("d", innertext);
}

function setForm() {
    var inputForm = bDoc.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = gameSeed;
	if((numX < 2) || (numX > 12)){
	    numX = 5;
	    numY = 5;
	}
	inputForm.sizeinput.value = numX;
    }
}

function getSizeTimes(){
    lsName = "SS:" + numX + "x" +  numY;
    if(!localStorage.getItem(lsName)){
	return("");
    }
    timeList = localStorage.getItem(lsName).split(";");
    for(var i = 0; i < timeList.length; i++){
	timeList[i] = Number(timeList[i]);
    }
    if(timeList.length > 5){
	timeList.sort(function(a, b){return(0.5 - Math.random())});
	while(timeList.length > 5){
	    timeList.pop();
	}
	tlText = "";
	for(var i = 0; i < timeList.length; i++){
	    if(tlText == ""){
		tlText = timeList[i];
	    } else {
		tlText += ";" + timeList[i];
	    }
	}
	localStorage.setItem(lsName, tlText);
    }
    timeList.sort(function(a, b){return(a - b)});
    timeText = "";
    for(var i = 0; i < timeList.length; i++){
	timeText += timeList[i] + "\n";
    }
    return(timeText);
}

function getTimes(){
    lsName = "HS:" + gameSeed + ":" + numX + ":" +  numY;
    if(!localStorage.getItem(lsName)){
	return("");
    }
    timeList = localStorage.getItem(lsName).split(";");
    for(var i = 0; i < timeList.length; i++){
	timeList[i] = Number(timeList[i]);
    }
    if(timeList.length > 5){
	timeList.sort(function(a, b){return(0.5 - Math.random())});
	while(timeList.length > 5){
	    timeList.pop();
	}
	tlText = "";
	for(var i = 0; i < timeList.length; i++){
	    if(tlText == ""){
		tlText = timeList[i];
	    } else {
		tlText += ";" + timeList[i];
	    }
	}
	localStorage.setItem(lsName, tlText);
    }
    timeList.sort(function(a, b){return(a - b)});
    timeText = "";
    for(var i = 0; i < timeList.length; i++){
	timeText += timeList[i] + "\n";
    }
    return(timeText);
}

function processFrame(){
    elapsedTime = ((new Date()) - startTime) / 1000 - stoppedTime;
    updateTimerText();
}

function updateTimerText(){
    var timeText = bDoc.getElementById("timelabel");
    timeText.firstChild.nodeValue = "" +
	(Math.floor(elapsedTime * 100)/100) + " s";
}

function movePointer(e){
    pointerVisible = true;
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

function updateColour(posX, posY){
    cPos = (posY * numX) + posX;
    posText = bDoc.getElementById("i_" + cPos);
    if(numLocs[cPos] ==
       (xNums[posX] * yNums[posY])){
	posText.setAttribute("font-size", textSize*1.25 + "px");
	posText.setAttribute("fill", "lightgreen");
    } else {
	posText.setAttribute("font-size", textSize + "px");
	posText.setAttribute("fill", "steelblue");
    }
}

function updateColourP(cPos){
    posX = Math.floor(cPos % numX);
    posY = Math.floor(cPos / numX);
    updateColour(posX, posY);
}

function chooseNumber(e){
    if(stopTime != -1){
	return;
    }
    gridX = Math.floor((mouseX - 50) / boxWidth);
    gridY = Math.floor((mouseY - 50) / boxHeight);
    if(gridX >= 0 && gridX <= numX &&
       gridY >= 0 && gridY <= numY){
	var clickPos = gridY * numX + gridX;
	var clickedText =
	    bDoc.getElementById("i_" + clickPos);
	clickedText.setAttribute("fill", "orangered");
	if(lastClickPos != -1){
	    var lastClickedText =
		bDoc.getElementById("i_" + lastClickPos);
	    // update text
	    lastClickedText.firstChild.nodeValue = numLocs[clickPos];
	    clickedText.firstChild.nodeValue = numLocs[lastClickPos];
	    // swap the two numbers
	    tmpPos = numLocs[clickPos];
	    numLocs[clickPos] = numLocs[lastClickPos];
	    numLocs[lastClickPos] = tmpPos;
	    // update colours
	    updateColourP(clickPos);
	    updateColourP(lastClickPos);
	    // reset click flags
	    lastClickPos = -1;
	    checkCorrect();
	} else {
	    lastClickPos = clickPos;
	}
    }
}

// work out how many cells are correct, and update score
function checkCorrect(){
    correctScore = 0;
    workScore = 0;
    for(var ix=0; ix < numX; ix++){
	for(var iy=0; iy < numY; iy++){
	    if(numLocs[(iy * numX) + ix] == (xNums[ix] * yNums[iy])){
		correctScore++;
	    } else {
		workScore++;
	    }
	}
    }
    if(!started){
	return(workScore == 0);
    }
    var correctText = bDoc.getElementById("correctlabel");
    correctText.firstChild.nodeValue = "Correct: " + correctScore;
    var workText = bDoc.getElementById("worklabel");
    workText.firstChild.nodeValue = "Needs Work: " + workScore;
    if((workScore == 0) && (!alertedWon)){
	alertedWon = true;
	if(useStorage){
	    lsName = "HS:" + gameSeed + ":" + numX + ":" +  numY;
	    if(localStorage.getItem(lsName)){
		localStorage.setItem(lsName,
				     localStorage.getItem(lsName) + ";" +
				     Math.floor(elapsedTime * 100) / 100);
	    } else {
		localStorage.setItem(lsName,
				     Math.floor(elapsedTime * 100) / 100);
	    }
	    lsName = "SS:" + numX + "x" +  numY;
	    if(localStorage.getItem(lsName)){
		localStorage.setItem(lsName,
				     localStorage.getItem(lsName) + ";" +
				     Math.floor(elapsedTime * 100) / 100);
	    } else {
		localStorage.setItem(lsName,
				     Math.floor(elapsedTime * 100) / 100);
	    }
	}
	timeMins = Math.floor(elapsedTime / 60);
	timeSecs = elapsedTime % 60;
	timeSecs = Math.floor(timeSecs * 100) / 100;
	var timeText = "Completed in ";
	if(timeSecs == 0){
	    timeText += "exactly ";
	}
	if(timeMins >= 1){
	    timeText += timeMins + " minute";
	    if(timeMins != 1){
		timeText += "s";
	    }
	}
	if(timeSecs > 0) {
	    if(timeMins >= 1){
		timeText += ", ";
	    }
	    timeText += timeSecs + " second";
	    if(timeSecs != 1){
		timeText += "s";
	    }
	}
	timeText += "!\n";
	gt = getTimes();
	if(gt != ""){
	    timeText += "\nHere are some previous times " +
		"for table #" + gameSeed +
		"; " + numX + "x" + numY + " (in seconds):\n" + gt;
	}
	st = getSizeTimes();
	if(st != ""){
	    timeText += "\nHere are some previous times " +
		"for all " + numX + "x" + numY +
		" tables (in seconds):\n" + st;
	}
	timeText += "\n\nClick 'Ok' to start a new random game.";
	stopTimer();
	if(confirm(timeText)){
	    makeGame(Math.floor(Math.random() * 10000),
		     Math.floor(Math.random() * 3) + 4);
	}
	return(true);
    }
    return(false);
}

function clearPointer(){
}

function stopTimer(){
    clearInterval(gameTimer);
    stopTime = new Date();
}

function startTimer(){
    if(stopTime != -1){
	var currentTime = new Date();
	stoppedTime += (currentTime - stopTime) / 1000;
	stopTime = -1;
    }
    clearInterval(gameTimer);
    gameTimer = setInterval(processFrame, (1000/fps));
}
