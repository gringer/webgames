// Witness Bingo game
// author: David Eccles (gringer) <bioinformatics@gringene.org>

witnessLabels = [['FREE:','Brain hurting'],
		 ['#1: "I know the rules!...', 'that\'s impossible!"'],
		 ['#2: Activates', 'yellow box'],
		 ['#3: "I was looking', 'right at it!"'],
		 ['#4: "One, two, three..."'],
		 ['#5: Figures out rules', 'before', 'being taught them'],
		 ['#6: Takes the', 'long way round'],
		 ['#7: Solves a puzzle', 'by getting distracted'],
		 ['#8: Misses a puzzle', 'by getting distracted'],
		 ['#9: "I don\'t know', 'where I am!"'],
		 ['#10: Makes', 'some sparkles'],
		 ['#11: Over 5 minutes', 'on an easy puzzle'],
		 ['#12: Instantly solves', 'a difficult puzzle'],
		 ['#13: Forgets to', 'follow the wires'],
		 ['#14: Chases the', 'wrong wire'],
		 ['#15: Cancels just', 'before solving'],
		 ['#16: Solves a puzzle','when not','in front of it'],
		 ['#17: "The game can', 'read my mind!"'],
		 ['#18: "Wait, what?"'],
		 ['#19: "Just one more!"'],
		 ['#20: "I don\'t mind this...', 'oh, ****"'],
		 ['#21: "This is easy...', 'oh, ****"'],
		 ['#22: "I\'ve tried that"','[they haven\'t]'],
		 ['#23: Draws the','same path', 'three times'],
		 ['#24: "This doesn\'t work"','[it does]'],
		 ['#25: "This *has* to work"','[it doesn\'t]'],
		 ['#26: Draws solution','on paper'],
		 ['#27: Solves using','a screenshot'],
		 ['#28: "I didn\'t realise', 'I could do that"'],
		 ['#29: "They\'re the', 'same colour?!"'],
		 ['#30: "They\'re', 'different colours?!"'],
		 ['#31: Solves the', 'wrong puzzle'],
		 ['#32: Forgets to', 'close drawing app'],
		 ['#33: Watcher', 'accidentally', 'spoils solution'],
		 ['#34: Watcher', 'intentionally', 'spoils solution'],
		 ['#35: Forgets', 'the correct rules'],
		 ['#36: Remembers', 'the wrong rules'],
		 ['#37: Uses Internet', 'instead of', 'stream watchers'],
		 ['#38: Runs to', 'puzzle panel', 'to solve it'],
		 ['#38: Resolves a', 'puzzle', 'for research'],
		 ['#39: More interested', 'in rules than', 'solution'],
		 ['#40: "Can this thing', 'go any faster?"'],
		 ['#41: "Can this thing', 'go any slower?"'],
		 ['#42: Unfurls', 'a box'],
		 ['#43: Listens to', 'a log']
		];

var bDoc;
var svgNS = "http://www.w3.org/2000/svg";
var started = false;
var leftEdge = 0;
var frameDelay = 20; // milliseconds between one score increment
var fps = 50; // attempt this many updates per second

var started = false;
var finished = false;

// game / screen settings
var svgWidth = 1000;
var svgHeight = 500;
var gameWidth = 900;
var gameHeight = 400;
var mouseX = -1;
var mouseY = -1;

// number game parameters
var numX = 5;
var numY = 5;
var textSize = 5;
var boxWidth = gameWidth / numX;
var boxHeight = gameHeight / numY;
var shuffleSlots = new Array();
var completedSlots = new Array();

// game settings
var startTime = -1;
var elapsedTime = -1;
var stopTime = -1;
var stoppedTime = 0;
var alertedWon = false;
var lastClickPos = -1;

var randSeed = new Date().getTime();
var gameSeed = randSeed;
var typeLookup = {mul: "x", add: "+", sub: "-"};
var lastLog = 0;

var gameTimer = setInterval(processFrame, (1000/fps));

var useStorage = false;

if (typeof(Storage) !== "undefined") {
    useStorage = true;
}

// the game only needs to look random, but seeding is nice
function nextRandom(){
    randSeed = (randSeed*9301+49297) % 233280;
    return randSeed/(233280.0);
}

function startup(evt) {
    bDoc = evt.target.ownerDocument;
    makeGame(Math.floor(Math.random() * 10000), 5,
	     ["mul","add","sub"][Math.floor(Math.random() * 3)]);
}

function makeGame(seed, size, gType){
    //console.log("seed: " + seed + "; size: " +
    //            size + "; type: " + gType + "\n");
    // reset timer (if not stopped)
    clearInterval(gameTimer);
    // reset win state
    started = false;
    alertedWon = false;
    lastClickPos = -1;
    startTime = -1;
    stopTime = -1;
    stoppedTime = 0;
    // set up form variables
    randSeed = seed;
    gameSeed = seed;
    numX = size;
    numY = size;
    setForm();
    boxWidth = gameWidth / numX;
    boxHeight = gameHeight / numY;
    textSize = boxHeight / 6;
    if(textSize > 20){
	textSize = 20;
    }
    var seedtext = bDoc.getElementById("seedlabel");
    seedtext.firstChild.nodeValue = "Seed: " + seed;
    var game = bDoc.getElementById("gamesetup");
    game.setAttribute("width", svgWidth + "px");
    game.setAttribute("height", svgHeight + "px");
    randSeed = seed;
    gameSeed = seed;
    // reset game variables
    numLocs = new Array();
    xNums = new Array();
    yNums = new Array();
    // create random shuffle of Bingo board
    shuffleSlots = new Array();
    for(var i=0; i < witnessLabels.length; i++){
	shuffleSlots[i] = witnessLabels[i];
	completedSlots[i] = false;
    }
    //console.log(shuffleSlots);
    for(var i=1; i < witnessLabels.length; i++){
	shufflePos = Math.floor(nextRandom() * (witnessLabels.length-1)) + 1;
	tmp = shuffleSlots[i];
	shuffleSlots[i] = shuffleSlots[shufflePos];
	shuffleSlots[shufflePos] = tmp;
    }
    // place the first slot in the middle[ish]
    midPos = Math.floor((((numY-1)/2) * numX) + ((numX-1) / 2));
    tmp = shuffleSlots[midPos];
    shuffleSlots[midPos] = shuffleSlots[0];
    shuffleSlots[0] = tmp;
    completedSlots[midPos] = true;
    //console.log(shuffleSlots);
    // draw board / numbers
    drawBoard();
    // start timer
    startTime = new Date();
    elapsedTime = 0;
    gameTimer = setInterval(processFrame, (1000/fps));
    updateTimerText();
    // update score
    started = true;
    checkCorrect();
}

function drawBoard(){
    var outerpath = bDoc.getElementById("grid_boundary");
    var innerpath = bDoc.getElementById("inner_grid");
    var numberGroup = bDoc.getElementById("g_numbers");
    var outertext = "";
    var innertext = "";
    var foundPoints = [];
    var collided = [];
    var addedCollision = [];
    var pointLoc = [];
    // create border
    outertext = "m 50,50 h " + gameWidth + " v " + gameHeight + " h -" +
	gameWidth + " v -" + gameHeight;
    // clear existing numbers (if they exist)
    var outernums = bDoc.getElementById("nums_boundary");
    if(outernums){
	outernums.remove();
    }
    var gridnums = bDoc.getElementById("nums_grid");
    if(gridnums){
	gridnums.remove();
    }
    // create grid and outer numbers
    var outerNumGroup =
	bDoc.createElementNS(svgNS, 'g');
    outerNumGroup.setAttribute("id", "nums_boundary");
    numberGroup.appendChild(outerNumGroup);
    var innerNumGroup =
	bDoc.createElementNS(svgNS, 'g');
    innerNumGroup.setAttribute("id", "nums_grid");
    numberGroup.appendChild(innerNumGroup);
    // add X lines
    for(var ix=0; ix <= numX; ix++){
	innertext += "M " + (50 + ix * boxWidth) +
	    ",50 v " + (gameHeight) + "\n";
    }
    // add Y lines
    for(var iy=0; iy <= numY; iy++){
	innertext += "M 50," + (50 + iy * boxHeight) +
	    " h " + (gameWidth) + "\n";
    }
    // Add in text
    for(var ix=0; ix < numX; ix++){
	for(var iy=0; iy < numY; iy++){
	    var midX = 50 + (ix+0.5) * boxWidth;
	    var midY = 50 + (iy+0.5) * boxHeight;
	    var pos = (iy * numX) + ix;
	    if(shuffleSlots[pos].length == 1){
		var newText = bDoc.createElementNS(svgNS, "text");
		newText.setAttribute("id", "i_"+pos+"_0");
		newText.setAttribute("x", midX);
		newText.setAttribute("y", midY + textSize/2);
		if(completedSlots[pos]){
		    newText.setAttribute("font-size", textSize*1.25 + "px");
		    newText.setAttribute("fill", "lightgreen");
		} else {
		    newText.setAttribute("font-size", textSize + "px");
		    newText.setAttribute("fill", "steelblue");
		}
		newText.setAttribute("text-anchor", "middle");
		var textNode = document.createTextNode(shuffleSlots[pos][0]);
		newText.appendChild(textNode);
		innerNumGroup.appendChild(newText);
	    } else {
		//var textNode = document.createTextNode('');
		yStart = midY + textSize - (textSize * shuffleSlots[pos].length / 2);
		for(var it = 0; it < shuffleSlots[pos].length; it++){
		    tsubs = shuffleSlots[pos][it];
		    var newText = bDoc.createElementNS(svgNS, "text");
		    newText.setAttribute("id", "i_"+pos+"_"+it);
		    newText.setAttribute("x", midX);
		    newText.setAttribute("y", yStart + textSize * it);
		    if(completedSlots[pos]){
			newText.setAttribute("font-size", textSize*1.25 + "px");
			newText.setAttribute("fill", "lightgreen");
		    } else {
			newText.setAttribute("font-size", textSize + "px");
			newText.setAttribute("fill", "steelblue");
		    }
		    newText.setAttribute("text-anchor", "middle");
		    var textNode = document.createTextNode(tsubs);
		    newText.appendChild(textNode);
		    innerNumGroup.appendChild(newText);
		}
	    }
	}
    }
    outerpath.setAttribute("d", outertext);
    innerpath.setAttribute("d", innertext);
}

function setForm() {
    var inputForm = bDoc.getElementById("seedform");
    if((typeof(inputForm) == "undefined") || (inputForm == null)){
        setTimeout(setForm, 100);
    } else {
        inputForm.seedinput.value = gameSeed;
	if((numX < 2) || (numX > 12)){
	    numX = 5;
	    numY = 5;
	}
	inputForm.sizeinput.value = numX;
    }
}

function getSizeTimes(){
    lsName = "SS:" + numX + "x" +  numY;
    if(!localStorage.getItem(lsName)){
	return("");
    }
    timeList = localStorage.getItem(lsName).split(";");
    for(var i = 0; i < timeList.length; i++){
	timeList[i] = Number(timeList[i]);
    }
    if(timeList.length > 5){
	timeList.sort(function(a, b){return(0.5 - Math.random())});
	while(timeList.length > 5){
	    timeList.pop();
	}
	tlText = "";
	for(var i = 0; i < timeList.length; i++){
	    if(tlText == ""){
		tlText = timeList[i];
	    } else {
		tlText += ";" + timeList[i];
	    }
	}
	localStorage.setItem(lsName, tlText);
    }
    timeList.sort(function(a, b){return(a - b)});
    timeText = "";
    for(var i = 0; i < timeList.length; i++){
	timeText += timeList[i] + "\n";
    }
    return(timeText);
}

function getTimes(){
    lsName = "HS:" + gameSeed +
	":" + numX + ":" +  numY;
    if(!localStorage.getItem(lsName)){
	return("");
    }
    timeList = localStorage.getItem(lsName).split(";");
    for(var i = 0; i < timeList.length; i++){
	timeList[i] = Number(timeList[i]);
    }
    if(timeList.length > 5){
	timeList.sort(function(a, b){return(0.5 - Math.random())});
	while(timeList.length > 5){
	    timeList.pop();
	}
	tlText = "";
	for(var i = 0; i < timeList.length; i++){
	    if(tlText == ""){
		tlText = timeList[i];
	    } else {
		tlText += ";" + timeList[i];
	    }
	}
	localStorage.setItem(lsName, tlText);
    }
    timeList.sort(function(a, b){return(a - b)});
    timeText = "";
    for(var i = 0; i < timeList.length; i++){
	timeText += timeList[i] + "\n";
    }
    return(timeText);
}

function processFrame(){
    elapsedTime = ((new Date()) - startTime) / 1000 - stoppedTime;
    updateTimerText();
}

function updateTimerText(){
    var timeText = bDoc.getElementById("timelabel");
    timeText.firstChild.nodeValue = "" +
	(Math.floor(elapsedTime * 100)/100) + " s";
}

function showList(){
    alertText = "Full bingo list:";
    for(var i=0; i < (numX * numY); i++){
	alertText += " \n";
	for(var j=0; j < shuffleSlots[i].length; j++){
	    alertText += " " + shuffleSlots[i][j];
	}
    }
    alert(alertText);
}

function movePointer(e){
    pointerVisible = true;
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

function updateColour(posX, posY){
    cPos = (posY * numX) + posX;
    for(var i=0; i < shuffleSlots[cPos].length; i++){
	posText = bDoc.getElementById("i_" + cPos + "_" + i);
	if(completedSlots[cPos]){
	    posText.setAttribute("font-size", textSize*1.25 + "px");
	    posText.setAttribute("fill", "lightgreen");
	} else {
	    posText.setAttribute("font-size", textSize + "px");
	    posText.setAttribute("fill", "steelblue");
	}
    }
}

function updateColourP(cPos){
    posX = Math.floor(cPos % numX);
    posY = Math.floor(cPos / numX);
    updateColour(posX, posY);
}

function chooseNumber(e){
    if(stopTime != -1){
	return;
    }
    gridX = Math.floor((mouseX - 50) / boxWidth);
    gridY = Math.floor((mouseY - 50) / boxHeight);
    if(gridX >= 0 && gridX <= numX &&
       gridY >= 0 && gridY <= numY){
	var clickPos = gridY * numX + gridX;
	//checkCorrect();
	completedSlots[clickPos] = !completedSlots[clickPos];
	updateColour(gridX, gridY);
	checkCorrect();
    }
}

// work out how many cells are correct, and update score
function checkCorrect(){
    correctScore = 0;
    workScore = 0;
    if(!started){
	return(workScore == 0);
    }
    for(var ix=0; ix < numX; ix++){
	for(var iy=0; iy < numY; iy++){
	    var midX = 50 + (ix+0.5) * boxWidth;
	    var midY = 50 + (iy+0.5) * boxHeight;
	    var pos = (iy * numX) + ix;
	    if(completedSlots[pos]){
		correctScore += 1;
	    }
	    else {
		workScore += 1;
	    }
	}
    }
    var correctText = bDoc.getElementById("correctlabel");
    correctText.firstChild.nodeValue = "Found: " + correctScore;
    var workText = bDoc.getElementById("worklabel");
    workText.firstChild.nodeValue = "Remaining: " + workScore;
    if((workScore == 0) && (!alertedWon)){
	alertedWon = true;
	timeMins = Math.floor(elapsedTime / 60);
	timeSecs = elapsedTime % 60;
	timeSecs = Math.floor(timeSecs * 100) / 100;
	var timeText = "Completed in ";
	if(timeSecs == 0){
	    timeText += "exactly ";
	}
	if(timeMins >= 1){
	    timeText += timeMins + " minute";
	    if(timeMins != 1){
		timeText += "s";
	    }
	}
	if(timeSecs > 0) {
	    if(timeMins >= 1){
		timeText += ", ";
	    }
	    timeText += timeSecs + " second";
	    if(timeSecs != 1){
		timeText += "s";
	    }
	}
	timeText += "!\n";
	timeText += "\n\nClick 'Ok' to start a new random game.";
	stopTimer();
	if(confirm(timeText)){
	    makeGame(Math.floor(Math.random() * 10000),
		     5,
		     ["mul","add","sub"][Math.floor(Math.random() * 3)]);
	}
	return(true);
    }
    return(false);
}

function clearPointer(){
}

function stopTimer(){
    clearInterval(gameTimer);
    stopTime = new Date();
}

function startTimer(){
    if(stopTime != -1){
	var currentTime = new Date();
	stoppedTime += (currentTime - stopTime) / 1000;
	stopTime = -1;
    }
    clearInterval(gameTimer);
    gameTimer = setInterval(processFrame, (1000/fps));
}
